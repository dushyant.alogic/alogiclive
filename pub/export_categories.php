<?php
use Magento\Framework\App\Bootstrap;
//require __DIR__ . '/app/bootstrap.php';
require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

//get category factory
$categoryCollectionFactory = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
$categoryCollection = $categoryCollectionFactory->create();
$categoryCollection->addAttributeToSelect('*');

$categoryArray = array();
foreach ($categoryCollection as $category) {
    $split = explode("/",$category->getPath());
    $sorter = "";
    foreach($split as $element)
        $sorter.= $element + 10000;
    $categoryArray[] = array($category->getPath(), $category->getLevel(), $category->getName(), $sorter);
}

usort($categoryArray, "usortTest");

$handle = fopen("category_export.txt", "w");

foreach($categoryArray as $category)
{
    $path = $category[0];
    $level = $category[1];
    $name = $category[2];

    $tree = "";
    if($level > 0){
        for($i = 0; $i < $level - 1; ++$i)
            $tree.= " ";
    }

    //echo $tree.$name."\n";
    fwrite($handle,$tree.$name."\n");
}

echo "found ".$categoryCollection->getSize()." categories\n";
fwrite($handle,"found ".$categoryCollection->getSize()." categories");
fclose($handle);

function usortTest($a, $b) {
    return strcmp($a[3],$b[3]);
}
