<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Magento\Framework\App\Bootstrap;
require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');
$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');

$collection = $productCollection->create()->addAttributeToSelect('*')->load();
foreach ($collection as $product){
   try {
           echo 'Deleted : '.$product->getSku()."<br>";
           $product->delete();

       } catch (Exception $e) {
           echo 'Unable to delete product : '.$product->getName()."<br>";
           echo $e->getMessage()."<br>";
    }
}
