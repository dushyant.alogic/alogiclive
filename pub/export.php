<?php
require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$_objectManager = $bootstrap->getObjectManager();
$state = $_objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');
$registry = $_objectManager->get('Magento\Framework\Registry');
$registry->register('isSecureArea', true);

//Store id of exported products, This is useful when we have multiple stores.
$store_id = 1;

$fp = fopen("export.csv", "w+");
$collection = $_objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory')->create()->addStoreFilter($store_id)->addAttributeToFilter('type_id', 'configurable')->addAttributeToSelect(array('name', 'sku', 'associated_skus
', 'metallic_colour'));
fputcsv($fp, ['type', 'sku', 'conf_skus', 'conf_colour']);
foreach ($collection as $product) {
	$data = array();
	$data[] = $product->getTypeId();
	$data[] = $product->getSku();
	//$data[] = $product->getAssociatedSkus();
	// fputcsv($fp, $data);

	$child_products = $product->getTypeInstance()->getUsedProducts($product, null);
	if (count($child_products) > 0) {
		$resultstr = array();
		$colors = array();
		foreach ($child_products as $child) {
			//echo "<pre>";
			//var_dump($child->getData());
			//die;

			//$data = array();
			// $data[] = $child->getTypeId();
			//$data[] = $child->getName();
			//$data[] = $child->getSku();
			$resultstr[] = $child->getSku();
			$colors[] = $child->getMetallicColour();

			//$data[] = $child->getAssociatedSkus();
			//$data[] = $child->getAssociatedSkus();
			// fputcsv($fp, $data);
		}
		$data[] = implode(',', $resultstr);
		$data[] = implode(',', $colors);
	}
	fputcsv($fp, $data);
}
fclose($fp);
?>


