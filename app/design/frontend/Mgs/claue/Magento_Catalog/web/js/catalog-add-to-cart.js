

/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/modal/alert',
    'jquery/ui',    
], function ($, $t,alert) {
    "use strict";

    $.widget('mage.catalogAddToCart', {

        options: {
            processStart: null,
            processStop: null,
            bindSubmit: true,
            minicartSelector: '[data-block="minicart"]',
            messagesSelector: '[data-placeholder="messages"]',
            productStatusSelector: '.stock.available',
            addToCartButtonSelector: '.action.tocart',
            addToCartButtonDisabledClass: 'disabled',
            addToCartButtonTextWhileAdding: '',
            addToCartButtonTextAdded: '',
            addToCartButtonTextDefault: ''
        },

        _create: function () {
            if (this.options.bindSubmit) {
                this._bindSubmit();
            }
        },

        _bindSubmit: function () {
            var self = this;
            this.element.on('submit', function (e) {
                e.preventDefault();
                self.submitForm($(this));
            });
        },

        isLoaderEnabled: function () {
            return this.options.processStart && this.options.processStop;
        },

        /**
         * Handler for the form 'submit' event
         *
         * @param {Object} form
         */
        submitForm: function (form) {
            var addToCartButton, self = this;

            if (form.has('input[type="file"]').length && form.find('input[type="file"]').val() !== '') {
                self.element.off('submit');
                // disable 'Add to Cart' button
                addToCartButton = $(form).find(this.options.addToCartButtonSelector);
                addToCartButton.prop('disabled', true);
                addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
                form.submit();
            } else {
                self.ajaxSubmit(form);
            }
        },
        ajaxCross: function () {
            var self = this;
            $.ajax({
                url: BASE_URL + '/accounthome/crosssell/index',
                async: false,
                type: 'post',

                beforeSend: function () {
                    $('#ajax_loader').show();
                    console.log("before send");
                },
                success: function (res) {
                    console.log("success", res);
                    if ($('#crosssellmini').length) {
                        $('#crosssellmini').html(res);
                        $('button.tocart').click(function (event) {
                            event.preventDefault();
                            var tag = $(this).parents('form:first');

                            var data = tag.serializeArray();
                            self.initAjaxNewAddToCart(tag, 'catalog-add-to-cart-' + $.now(), tag.attr('action'), data);


                        });
                        jQuery('#ajax_loader').hide();
                    } else if (window.parent.jQuery('#crosssellmini').length) {
                        window.parent.jQuery('#crosssellmini').html(res);
                        window.parent.jQuery('button.tocart').click(function (event) {
                            event.preventDefault();
                            var tag = $(this).parents('form:first');
                            
                            var data = tag.serializeArray();
                            self.initAjaxNewAddToCart(tag, 'catalog-add-to-cart-' + $.now(), tag.attr('action'), data);


                        });
                        jQuery('#ajax_loader').hide();
                    }


                }
            });


        },
        initAjaxNewAddToCart: function (tag, actionId, url, data) {

            data.push({
                name: 'action_url',
                value: tag.attr('action')
            });

            var $addToCart = tag.find('.tocart').text();

            var self = this;
            data.push({
                name: 'ajax',
                value: 1
            });

            $.ajax({
                url: url,
                data: $.param(data),
                type: 'post',
                dataType: 'json',
                beforeSend: function (xhr, options) {
                    /*if (tag.find('.tocart').length) {
                        tag.find('.tocart').addClass('disabled');
                        tag.find('.tocart').text('Adding...');
                        tag.find('.tocart').attr('title', 'Adding...');
                    } else {
                        tag.addClass('disabled');
                        tag.text('Adding...');
                        tag.attr('title', 'Adding...');
                    }*/
                },
                success: function (response, status) {
                    if (status == 'success') {
                        var $source = '';
                        if (response.backUrl) {
                            console.log('Back url');
                            data.push({
                                name: 'action_url',
                                value: response.backUrl
                            });
                            self.initAjaxNewAddToCart(tag, 'catalog-add-to-cart-' + $.now(), response.backUrl, data);
                        } else {
                            console.log('else Back url');
                            console.log('Response', response);
                            console.log("cart length", jQuery('.action.showcart').length);
                            if (jQuery('.action.showcart').length) {
                                //jQuery('.action.showcart').trigger('click');
                            }
                            if (jQuery('.action.showcart').length) {
                                if (!jQuery('.action.showcart').closest('.minicart-wrapper').hasClass('active')) {
                                    jQuery('.action.showcart').trigger('click');
                                }

                            }

                            if (tag.find('.tocart').length) {
                                tag.find('.tocart').removeClass('disabled');
                                tag.find('.tocart').text($addToCart);
                                tag.find('.tocart').attr('title', $addToCart);
                                if (tag.closest('.product-item-info').length) {
                                    $source = tag.closest('.product-item-info');
                                    var width = $source.outerWidth();
                                    var height = $source.outerHeight();
                                } else {
                                    $source = tag.find('.tocart');
                                    var width = 300;
                                    var height = 300;
                                }

                            } else {
                                tag.removeClass('disabled');
                                tag.text($addToCart);
                                tag.attr('title', $addToCart);
                                $source = tag.closest('.product-item-info');
                                var width = $source.outerWidth();
                                var height = $source.outerHeight();
                            }
                        }
                    }
                },
                error: function () {
                    $('#mgs-ajax-loading').hide();
                    window.location.href = ajaxCartConfig.redirectCartUrl;
                }
            });
        },

        ajaxSubmit: function (form) {
            var self = this;
            $(self.options.minicartSelector).trigger('contentLoading');
            self.disableAddToCartButton(form);

            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                dataArray: form.serializeArray(),
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    if (self.isLoaderEnabled()) {
                        $('body').trigger(self.options.processStart);
                    }
                    $('body').append("<div id='fancybox-loading'><div></div></div>");
                    $('#ajax_loader').show();

                    if (self.isLoaderEnabled()) {
                        //    $('body').trigger(self.options.processStart);
                    }
                    console.log("beforeSend");
                },
                success: function (res) {
                    console.log("cart result",res);
                    //if(jQuery('body').hasClass('mgs_quickview-catalog_product-view')){
                    //window.parent.jQuery.magnificPopup.close();
                    //jQuery('.action.showcart').trigger('click');
                    //jQuery('.action.showcart', window.parent.document).trigger('click');
                    /*if(this.dataArray.length <= 1){ 
                        if(window.parent.jQuery('.action.showcart').length){
                            window.parent.jQuery('.action.showcart').trigger('click');
                         }
                    }*/
                    self.ajaxCross();
                    console.log("Length", this.dataArray.length);
                    // jQuery('.action.showcart').trigger('click');
                    if (this.dataArray.length != 1) {
                        if (window.parent.jQuery('.action.showcart').length) {
                            console.log('In Parent');
                            window.parent.jQuery('.action.showcart').trigger('click');
                            window.parent.jQuery.magnificPopup.close();
                        }
                        if (jQuery('.action.showcart').length) {
                            if (!jQuery('.action.showcart').closest('.minicart-wrapper').hasClass('active')) {
                                jQuery('.action.showcart').trigger('click');
                            }

                        }
                    }
                    //jQuery('.action.showcart').trigger('click');

                    if (jQuery('.action.showcart').length) {
                        //jQuery('.action.showcart').trigger('click');
                    }
                    /*  if(this.dataArray.length > 1){
                        
                        if(window.parent.jQuery('.action.showcart').length){
                              window.parent.jQuery('.action.showcart').trigger('click');
                           }
                      }else{
                          if(jQuery('.action.showcart').length){
                          jQuery('.action.showcart').trigger('click');
                        }  
                      }*/

                    console.log("dataArray", this.dataArray);
                    //}
                    jQuery('#ajax_loader').hide();
                    jQuery('body #fancybox-loading').remove();
                    if (res.error) {
                        if (res.html) {
                            require(['jquery', 'rokanthemes/fancybox'], function ($) {
                                jQuery.fancybox({
                                    content: res.html,
                                    helpers: {
                                        overlay: {
                                            locked: false
                                        }
                                    }
                                });
                            });
                        }
                    }
                    if (res.html && res.error) {
                        require(['jquery', 'rokanthemes/fancybox'], function ($) {
                            jQuery.fancybox({
                                content: res.html,
                                helpers: {
                                    overlay: {
                                        locked: false
                                    }
                                }
                            });
                        });
                    }
                    if (self.isLoaderEnabled()) {
                        $('body').trigger(self.options.processStop);
                    }

                    if (res.backUrl) {
                        window.location = res.backUrl;
                        return;
                    }
                    if (res.messages) {
                        $(self.options.messagesSelector).html(res.messages);
                    }
                    if (res.minicart) {
                        $(self.options.minicartSelector).replaceWith(res.minicart);
                        $(self.options.minicartSelector).trigger('contentUpdated');
                    }
                    if (res.product && res.product.statusText) {
                        $(self.options.productStatusSelector)
                            .removeClass('available')
                            .addClass('unavailable')
                            .find('span')
                            .html(res.product.statusText);
                    }
                    self.enableAddToCartButton(form);
                },
                error: function (xhr, status, errorThrown) {
                    alert({
                            title: 'Attention!',
                            content: 'Please select the required option(s)',
                            actions: {
                                always: function(){}
                            }
                        });

                            
                            jQuery('#ajax_loader').hide();
                            jQuery('body #fancybox-loading').remove();                            
                            self.enableAddToCartButton(form);
                }
            });
        },

        disableAddToCartButton: function (form) {
            var addToCartButtonTextWhileAdding = this.options.addToCartButtonTextWhileAdding || $t('Adding...');
            var addToCartButton = $(form).find(this.options.addToCartButtonSelector);
            addToCartButton.addClass(this.options.addToCartButtonDisabledClass);
            addToCartButton.find('.fa').addClass('fa-spin');
            addToCartButton.find('span:not(.fa)').text(addToCartButtonTextWhileAdding);
            addToCartButton.attr('title', addToCartButtonTextWhileAdding);
        },

        enableAddToCartButton: function (form) {
            var addToCartButtonTextAdded = this.options.addToCartButtonTextAdded || $t('Added');
            var self = this,
                addToCartButton = $(form).find(this.options.addToCartButtonSelector);

            addToCartButton.find('span:not(.fa)').text(addToCartButtonTextAdded);
            addToCartButton.find('.fa').removeClass('fa-spin');
            addToCartButton.find('.fa').addClass('fa-added');
            addToCartButton.attr('title', addToCartButtonTextAdded);

            setTimeout(function () {
                var addToCartButtonTextDefault = self.options.addToCartButtonTextDefault || $t('Add to Cart');
                addToCartButton.removeClass(self.options.addToCartButtonDisabledClass);
                addToCartButton.find('span:not(.fa)').text(addToCartButtonTextDefault);
                addToCartButton.find('.fa').removeClass('fa-added');
                addToCartButton.attr('title', addToCartButtonTextDefault);
            }, 1000);
        }
    });
    
    return $.mage.catalogAddToCart;
});

