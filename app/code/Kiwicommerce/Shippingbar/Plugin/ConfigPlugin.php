<?php

namespace Kiwicommerce\Shippingbar\Plugin;
use Magento\Framework\App\RequestInterface;

class ConfigPlugin {
	private $request;

	private $configWriter;
	public function __construct(RequestInterface $request) {
		$this->request = $request;
	}
	public function aroundSave(
		\Magento\Config\Model\Config $subject,
		\Closure $proceed
	) {
		$meetParams = $this->request->getParam('groups');
/*
$meetParams['shippingsection']['fields']['shipping_bar']['value'];
$writer = new \Laminas\Log\Writer\Stream(BP . '/var/log/templog1.log');
$logger = new \Laminas\Log\Logger();
$logger->addWriter($writer);
$logger->info(print_r($meetParams, true));*/

		// your custom logic
		return $proceed();
	}
}