<?php

namespace Lof\Rma\Model\ResourceModel\Rma;

class ExtendedCollection extends Collection
{
    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _getSelectedColumns()
    {
        if ($this->_period_type) {
            switch ($this->_period_type) {
                case "year":
                    $this->periodFormat = 'YEAR(main_table.' . $this->getDateColumnFilter() . ')';
                    break;
                case "quarter":
                    $this->periodFormat = 'CONCAT(QUARTER(main_table.' . $this->getDateColumnFilter() . '),"/",YEAR(main_table.' . $this->getDateColumnFilter() . '))';
                    break;
                case "week":
                    $this->periodFormat = 'CONCAT(YEAR(main_table.' . $this->getDateColumnFilter() . '),"", WEEK(main_table.' . $this->getDateColumnFilter() . '))';
                    break;
                case "day":
                    $this->periodFormat = 'DATE(main_table.' . $this->getDateColumnFilter() . ')';
                    break;
                case "hour":
                    $this->periodFormat = "DATE_FORMAT(main_table." . $this->getDateColumnFilter() . ", '%H:00')";
                    break;
                case "weekday":
                    $this->periodFormat = 'WEEKDAY(main_table.' . $this->getDateColumnFilter() . ')';
                    break;
                case "month":
                default:
                    $this->periodFormat = 'CONCAT(MONTH(main_table.' . $this->getDateColumnFilter() . '),"/",YEAR(main_table.' . $this->getDateColumnFilter() . '))';
                    break;
            }
        }

        $this->selectedColumns = [
            'total_rma_cnt' => 'COUNT(*)',
            'time' => $this->periodFormat,
            'increment_id' => '(main_table.increment_id)',
            'total_request' => 'sum(rma_item.qty_requested)',
            'reason' => '(rma_reason.name)',
            'product_ids' => 'GROUP_CONCAT(DISTINCT rma_item.product_id SEPARATOR \',\')',
            'sku' => 'GROUP_CONCAT(DISTINCT catalog_product.sku SEPARATOR \',\')',
            'order_customer_email' => '(customer.email)',
            'last_reply_name' => '(main_table.last_reply_name)',
            'order_increment_id' => '(order.increment_id)',
            'status_name' => '(status.name)',
        ];

        $statusCollection = $this->statusCollectionFactory->create()->addActiveFilter();

        foreach ($statusCollection as $status) {
            $this->selectedColumns["{$status->getId()}_cnt"] = "SUM(if (main_table.status_id = {$status->getId()}, 1, 0))";
        }

        $this->getSelect()->reset(\Zend_Db_Select::COLUMNS)->columns($this->selectedColumns)->group('main_table.rma_id');
        // sql theo filter date
        if ($this->_to_date_filter && $this->_from_date_filter) {
            $dateStart = $this->_localeDate->convertConfigTimeToUtc($this->_from_date_filter, 'Y-m-d 00:00:00');
            $endStart = $this->_localeDate->convertConfigTimeToUtc($this->_to_date_filter, 'Y-m-d 23:59:59');
            $dateRange = array('from' => $dateStart, 'to' => $endStart, 'datetime' => true);

            $this->addFieldToFilter('main_table.' . $this->getDateColumnFilter(), $dateRange);
        }

        return $this;
    }

    protected function initFields()
    {
        /* @noinspection PhpUnusedLocalVariableInspection */
        $select = $this->getSelect();
        $select->joinLeft(
            ['order' => $this->getTable('sales_order')],
            'main_table.order_id = order.entity_id',
            ['order_increment_id' => 'order.increment_id']
        );
        $select->joinLeft(
            ['customer' => $this->getTable('customer_entity')],
            'main_table.customer_id = customer.entity_id',
            ['customer_firstname' => 'customer.firstname',
                'customer_lastname' => 'customer.lastname',
                'order_customer_email' => 'customer.email'
            ]
        );

        $select->joinLeft(
            ['status' => $this->getTable('lof_rma_status')],
            'main_table.status_id = status.status_id',
            ['status_name' => 'status.name']
        );

        $select->joinLeft(
            ['rma_item' => $this->getTable('lof_rma_item')],
            'main_table.rma_id = rma_item.rma_id'
        );

        $select->joinLeft(
            ['catalog_product' => $this->getTable('catalog_product_entity')],
            'rma_item.product_id = catalog_product.entity_id'
        );

        $select->joinLeft(
            ['rma_reason' => $this->getTable('lof_rma_reason')],
            'rma_item.reason_id = rma_reason.reason_id'
        );
    }
}
