<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */



namespace Lof\Rma\Api\Data;

use Lof\Rma\Api;

/**
 * @method Api\Data\RmaSearchResultsInterface getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
 */
interface RmaInterface extends DataInterface
{
    const KEY_INCREMENT_ID       = 'increment_id';
    const KEY_GUEST_ID           = 'guest_id';
    const KEY_FIRSTNAME          = 'firstname';
    const KEY_LASTNAME           = 'lastname';
    const KEY_EMAIL              = 'email';
    const KEY_CUSTOMER_ID        = 'customer_id';
    const KEY_CUSTOMER_EMAIL     = 'customer_email';
    const KEY_ORDER_ID           = 'order_id';
    const KEY_STATUS_ID          = 'status_id';
    const KEY_STORE_ID           = 'store_id';
    const KEY_TRACKING_CODE      = 'tracking_code';
    const KEY_IS_RESOLVED        = 'is_resolved';
    const KEY_CREATED_AT         = 'created_at';
    const KEY_UPDATED_AT         = 'updated_at';
    const KEY_IS_GIFT            = 'is_gift';
    const KEY_IS_ADMIN_READ      = 'is_admin_read';
    const KEY_USER_ID            = 'user_id';
    const KEY_LAST_REPLY_NAME    = 'last_reply_name';
    const KEY_TICKET_ID          = 'ticket_id';
    const KEY_EXCHANGE_ORDER_IDS = 'order_ids';
    const KEY_CREDIT_MEMO_IDS    = 'credit_memo_ids';
    const KEY_RETURN_ADDRESS     = 'return_address';
    const KEY_PARENT_RMA_ID      = 'parent_rma_id';
    const KEY_BUNDLE_ORDER_IDS   = 'bundle_order_ids';
    const KEY_CHILD_RMA_IDS      = 'child_rma_ids';
    const MESSAGE_CODE = 'RMA-';

    /**
     * @return string
     */
    public function getCustomerEmail();

    /**
     * @param string $customerEmail
     * @return $this
     */
    public function setCustomerEmail($customerEmail);
    
    /**
     * @return string
     */
    public function getIncrementId();

    /**
     * @param string $incrementId
     * @return $this
     */
    public function setIncrementId($incrementId);

    /**
     * @return string
     */
    public function getFirstname();

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname);

    /**
     * @return string
     */
    public function getLastname();

    /**
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname);

    
    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email);

    
    /**
     * @return int
     */
    public function getParentRmaId();

    /**
     * @param int $rma_id
     * @return $this
     */
    public function setParentRmaId($rma_id);

    /**
     * @return int
     */
    public function getCustomerId();

    /**
     * @param int $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * @return int
     */
    public function getOrderId();

    /**
     * @param int $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * @return int
     */
    public function getStatusId();

    /**
     * @param int $statusId
     * @return $this
     */
    public function setStatusId($statusId);

    /**
     * @return int
     */
    public function getStoreId();

    /**
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId);

    /**
     * @return string
     */
    public function getTrackingCode();

    /**
     * @param string $trackingCode
     * @return $this
     */
    public function setTrackingCode($trackingCode);

    /**
     * @return boolean|int
     */
    public function getIsResolved();

    /**
     * @param boolean $isResolved
     * @return $this
     */
    public function setIsResolved($isResolved);


    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $date
     * @return $this
     */
    public function setCreatedAt($date);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param string $date
     * @return $this
     */
    public function setUpdatedAt($date);

    /**
     * @return bool|null
     */
    public function getIsGift();

    /**
     * @param bool $isGift
     * @return $this
     */
    public function setIsGift($isGift);

    /**
     * @return bool|null
     */
    public function getIsAdminRead();

    /**
     * @param bool $isAdminRead
     * @return $this
     */
    public function setIsAdminRead($isAdminRead);

    /**
     * @return int
     */
    public function getUserId();

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId);

    /**
     * @return string
     */
    public function getLastReplyName();

    /**
     * @param string $lastReplyName
     * @return $this
     */
    public function setLastReplyName($lastReplyName);

    /**
     * @return string
     */
    public function getReturnAddress();

    /**
     * @param string $address
     * @return $this
     */
    public function setReturnAddress($address);

    /**
     * @return string
     */
    public function getCode();

    /**
     * @return string
     */
    public function getBundleOrderIds();

    /**
     * @param string $bundleOrderIds
     * @return $this
     */
    public function setBundleOrderIds($bundleOrderIds);

    /**
     * @return mixed
     */
    public function getChildIds();

    /**
     * @param string $childRmaIds
     * @return $this
     */
    public function setChildIds($childRmaIds);
}