<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */
namespace Lof\Rma\Block\Adminhtml;

use Magento\Framework\View\Element\Template;

/**
 * Class FacebookSupport
 * @package Lof\FaceSupportLive\Block\Chatbox
 */
class ReportRma extends Template implements \Magento\Widget\Block\BlockInterface
{
    protected $_rmaCollectionFactory;
    protected $_statusCollection;
    /**
     * FacebookSupport constructor.
     * @param Template\Context $context
     * @param \Lof\Rma\Model\ResourceModel\Rma\Collection $rmaCollectionFactory
     * @param \Lof\Rma\Model\ResourceModel\Status\Collection $statusCollection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Lof\Rma\Model\ResourceModel\Rma\Collection $rmaCollectionFactory,
        \Lof\Rma\Model\ResourceModel\Status\Collection $statusCollection,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $my_template = "Lof_Rma::report/rma/grid/chart.phtml";
        if($this->hasData("template") && $this->getData("template")) {
            $my_template = $this->getData("template");
        } elseif(isset($data['template']) && $data['template']){
            $my_template = $data['template'];
        }
        if($my_template) {
            $this->setTemplate($my_template);
        }

        $this->_rmaCollectionFactory = $rmaCollectionFactory;
        $this->_statusCollection = $statusCollection;
       
    }

    public function getStoreManager(){
        return $this->_storeManager;
    }
    public function getRmaCollection(){
        return $this->_rmaCollectionFactory;
    }
    public function getStatusCollection(){
        return $this->_statusCollection;
    }
    public function getActiveStatus(){
        if(!isset($this->_activeStatusCollection)){
            $this->_activeStatusCollection = $this->getStatusCollection()->addActiveFilter();
        }
        return $this->_activeStatusCollection;
    }

    public function filterRmaCollection($type, $field = 'created_at', $startdate, $endate){
        if(!isset($this->_rmaItemCollection)){
            $resourceCollection =  $this->getRmaCollection()
                                ->setPeriodType($type)
                                ->setDateColumnFilter($field)
                                ->addDateFromFilter($startdate)
                                ->addDateToFilter( $endate)
                                ->_getSelectedColumns();
            $this->_rmaItemCollection = $resourceCollection;
        }
        return  $this->_rmaItemCollection;
    }
}
   
     

   

    

