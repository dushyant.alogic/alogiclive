<?php

namespace Lof\Rma\Block\Adminhtml;

use Magento\Framework\View\Element\Template;

/**
 * Class ReportDetail
 * @package Lof\Rma\Block\Adminhtml
 */
class ReportDetail extends Template implements \Magento\Widget\Block\BlockInterface
{
    /**
     * @var \Lof\Rma\Model\ResourceModel\Rma\Collection
     */
    protected $_rmaCollectionFactory;
    /**
     * @var \Lof\Rma\Model\ResourceModel\Status\Collection
     */
    protected $_statusCollection;

    /**
     * ReportDetail constructor.
     * @param Template\Context $context
     * @param \Lof\Rma\Model\ResourceModel\Rma\Collection $rmaCollectionFactory
     * @param \Lof\Rma\Model\ResourceModel\Status\Collection $statusCollection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Lof\Rma\Model\ResourceModel\Rma\Collection $rmaCollectionFactory,
        \Lof\Rma\Model\ResourceModel\Status\Collection $statusCollection,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $my_template = "Lof_Rma::report/rma/detail/grid/chart.phtml";
        if ($this->hasData("template") && $this->getData("template")) {
            $my_template = $this->getData("template");
        } elseif (isset($data['template']) && $data['template']) {
            $my_template = $data['template'];
        }

        if ($my_template) {
            $this->setTemplate($my_template);
        }

        $this->_rmaCollectionFactory = $rmaCollectionFactory;
        $this->_statusCollection = $statusCollection;
    }

    /**
     * @return \Magento\Store\Model\StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->_storeManager;
    }

    /**
     * @return \Lof\Rma\Model\ResourceModel\Rma\Collection
     */
    public function getRmaCollection()
    {
        return $this->_rmaCollectionFactory;
    }

    /**
     * @return \Lof\Rma\Model\ResourceModel\Status\Collection
     */
    public function getStatusCollection()
    {
        return $this->_statusCollection;
    }

    /**
     * @param $type
     * @param string $field
     * @param $startdate
     * @param $endate
     * @return mixed
     */
    public function filterRmaCollection($type, $field = 'created_at', $startdate, $endate)
    {
        if (!isset($this->_rmaItemCollection)) {
            $resourceCollection = $this->getRmaCollection()
                ->setPeriodType($type)
                ->setDateColumnFilter($field)
                ->addDateFromFilter($startdate)
                ->addDateToFilter($endate)
                ->_getSelectedColumns();
            $this->_rmaItemCollection = $resourceCollection;
        }
        return $this->_rmaItemCollection;
    }
}







