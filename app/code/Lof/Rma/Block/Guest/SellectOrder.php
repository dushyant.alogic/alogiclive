<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Rma\Block\Guest;

class SellectOrder extends \Magento\Framework\View\Element\Html\Link
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Session\SessionManagerInterface $sessionObj,
        \Magento\Customer\Model\Session $customerSession, 
        \Magento\Framework\Api\SearchCriteriaBuilder       $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrderBuilder            $sortOrderBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface                   $orderRepository,
        \Lof\Rma\Helper\Data    $rmaHelper,
        \Lof\Rma\Helper\Help                                $Helper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        array $data = []
    ) {
        $this->sortOrderBuilder      = $sortOrderBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderRepository       = $orderRepository;
        $this->rmaHelper             = $rmaHelper;
        $this->helper                = $Helper;
        $this->customerSession        = $customerSession;
        $this->context                = $context;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->sessionObj = $sessionObj;
        parent::__construct($context, $data);
    }

    public function filterOrderByProductName($product_name, $allow_order_ids = []){
        if($allow_order_ids && (strlen($product_name) >=3)){
            $collection = $this->_orderCollectionFactory->create();
            $collection->addAttributeToSelect('entity_id')
                        ->addFieldToFilter('entity_id', ['in' => $allow_order_ids]);

            $collection->getSelect()->join(
                        ['order_item' => $collection->getTable('sales_order_item')],
                        'order_item.order_id = main_table.entity_id',
                        ['product_name' => 'order_item.name'])
                        ->group('main_table.entity_id');
            
            $collection->addFieldToFilter('order_item.name', ['like' => ('%'.$product_name.'%')]);
            
            if($collection->count()){
                $filter_order_ids = [];
                foreach($collection as $_order){
                    $filter_order_ids[] = $_order->getId();
                }
                return $filter_order_ids;
            }
        }
        return [];
    }

    public function getSessionOrder(){
        $this->sessionObj->start();
        return $this->sessionObj->getGuestOrderId();
    }

    public function getSessionEmail(){
        $this->sessionObj->start();
        return $this->sessionObj->getGuestEmail();
    }

    public function getOrderList() {
        if(!isset($this->_orderList)){
            $customer_email = $this->getSessionEmail();
            $itemsperpage = 30;
            $currentPage = (isset($_GET['p']) && $_GET['p'])?(int)$_GET['p']:1;

            $order_id = $this->getRequest()->getParam('id');
            $product_name = $this->getRequest()->getParam('product');
            $order_date_from = $this->getRequest()->getParam('date_from');
            $order_date_to = $this->getRequest()->getParam('date_to');
            $sort = $this->getRequest()->getParam('sort');
            $sort_by_field = 'entity_id';
            $sort_direction = \Magento\Framework\Api\SortOrder::SORT_DESC;
            if($sort){
                $sort_array = explode(":",$sort);
                if(isset($sort_array[0]) && $sort_array[0]){
                    $sort_by_field = $sort_array[0];
                }
                if(isset($sort_array[1]) && $sort_array[1]){
                    $sort_direction = $sort_array[1];
                }
            }
            $allow_order_ids = $this->rmaHelper->getAllowOrderId();
            if($product_name && $allow_order_ids && (strlen($product_name) >=3)){
                $allow_order_ids = $this->filterOrderByProductName($product_name, $allow_order_ids);
            }
            if($allow_order_ids){
                $searchCriteria = $this->searchCriteriaBuilder
                    ->addFilter('customer_email',$customer_email)
                    ->addFilter('entity_id', $allow_order_ids, 'in');
                if($order_id){
                    $searchCriteria->addFilter('increment_id', $order_id);
                }
                if($order_date_from){
                    $order_date_from = date("Y-m-d H:i:s", strtotime($order_date_from));
                    $searchCriteria->addFilter('created_at', $order_date_from, 'gt');
                }
                if($order_date_to){
                    $order_date_to = date("Y-m-d H:i:s", strtotime($order_date_to));
                    $searchCriteria->addFilter('created_at', $order_date_to, 'lt');
                }

                $searchCriteria = $searchCriteria->setPageSize($itemsperpage)
                    ->setCurrentPage($currentPage)
                    ->addSortOrder($this->sortOrderBuilder->setField($sort_by_field)
                                                        ->setDirection($sort_direction)
                                                        ->create()
                    );

                $orders = $this->orderRepository->getList($searchCriteria->create());
                $toolbar = $this->getToolbarBlock();
                // set collection to toolbar and apply sort
                if($toolbar){
                    $toolbar->setData('_current_limit',$itemsperpage)->setCollection($orders);
                    $this->setChild('toolbar', $toolbar);
                }
                $this->_orderList = $orders->getItems();
            }else {
                $this->_orderList = [];
            }
        }
        return $this->_orderList;
    }
    /**
     * @param int $orderId
     * @return string
     */
    public function getOrderUrl($orderId)
    {
        return  $this->context->getUrlBuilder()->getUrl('sales/order/view', ['order_id' => $orderId]);
    }
    /**
     * @return string
     */
    public function getCreateRmaUrl($order_id)
    {
        return $this->context->getUrlBuilder()->getUrl('returns/guest/new/',['order_id' => $order_id]);
    }

    public function getIsUseBothType(){
        return $this->helper->getIsUseBothType();
    }

    /**
     * @return int
     */
    public function getReturnPeriod()
    {
        return $this->helper->getConfig($store = null,'rma/policy/return_period');
    }
    public function getIsShowBundle(){
        return $this->helper->isShowBundleRmaFrontend();
    }
     /**
     * @return boolean
     */
     public function IsItemsQtyAvailable($order){
        $items = $order->getAllItems();
        foreach ($items as  $item) {
            if($item->getData('base_row_total') <=0||$item->getData('product_type') == 'bundle')  continue; 
                if($this->rmaHelper->getItemQuantityAvaiable($item)>0){
                    return true;
                }
            }  
        return false;    
    }
   
    /**
     * Prepare layout for change buyer
     *
     * @return Object
     */
    public function _prepareLayout() {
        if($this->getIsShowBundle()){
            $this->pageConfig->getTitle ()->set(__('Sellect One or Multi Orders'));
        } else {
            $this->pageConfig->getTitle ()->set(__('Sellect Order'));
        }
        return parent::_prepareLayout ();
    }

    public function _toHtml(){
        $template = $this->getTemplate();
        if($this->getIsShowBundle()){
            $template = "Lof_Rma::guest/select_multi_orders.phtml";
            $this->setTemplate($template);
        }
        return parent::_toHtml();
    }

    public function getToolbarBlock()
    {
        $block = $this->getLayout()->getBlock('rma_toolbar');
        if ($block) {
            return $block;
        }
    }
}