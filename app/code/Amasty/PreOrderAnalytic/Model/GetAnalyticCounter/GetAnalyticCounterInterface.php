<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_PreOrderAnalytic
 */


declare(strict_types=1);

namespace Amasty\PreOrderAnalytic\Model\GetAnalyticCounter;

interface GetAnalyticCounterInterface
{
    public function execute(array $params): int;
}
