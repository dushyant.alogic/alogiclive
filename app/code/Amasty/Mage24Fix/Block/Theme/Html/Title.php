<?php

declare (strict_types = 1);

namespace Amasty\Mage24Fix\Block\Theme\Html;

use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
use Magento\Catalog\Model\Category;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Cms\Model\Page;

/**
 * Class Title
 *
 * Fix fatal with plugin to title block only for 2.4.0 version - https://github.com/magento/magento2/issues/28981
 */
class Title extends Template {
	/**
	 * Config path to 'Translate Title' header settings
	 */
	const XML_PATH_HEADER_TRANSLATE_TITLE = 'design/header/translate_title';

	/**
	 * @var ScopeConfigInterface
	 */
	private $scopeConfig;

	/**
	 * Own page title to display on the page
	 *
	 * @var string
	 */
	protected $pageTitle;

	public function __construct(
		Template\Context $context,
		Category $categoryModel,
		StoreManagerInterface $storemanager,
		Page $page,
		ProductRepositoryInterfaceFactory $productRepositoryFactory,
		Registry $registry,
		ScopeConfigInterface $scopeConfig,
		Http $request,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->_categoryModel = $categoryModel;
		$this->_storeManager = $storemanager;
		$this->_productRepositoryFactory = $productRepositoryFactory;
		$this->_page = $page;
		$this->_registry = $registry;

		$this->_request = $request;
		$this->scopeConfig = $scopeConfig;
	}
	public function getBannerImageUrl() {
		$url = false;
		$store = $this->_storeManager->getStore();
		if ($this->_request->getFullActionName() == 'presskit_index_index' 
			|| $this->_request->getFullActionName() == 'browsecatalogs_index_index'
			|| $this->_request->getFullActionName() == 'blog_index_index'
			|| $this->_request->getFullActionName() == 'pressrelease_index_index'
			|| $this->_request->getFullActionName() == 'pressrelease_index_view'	) {
			return $url = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) . 'wysiwyg/Banner_image_V1_-_FINAL.jpg';
		}
		if ($this->_request->getFullActionName() == 'shippingtracking_index_index') {
			return $url = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) . 'cms/hero/image/product-support-banner.jpg';
		}
		if ($this->_request->getFullActionName() == 'downloadcatalogue_index_index' || $this->_request->getFullActionName() == 'downloadcatalogue_index_view') {
			return $url = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) . 'front-feature-wall-mock-up-3.jpg';
		}
		if ($this->_request->getFullActionName() == 'products_index_index') {
			$main_category = $this->_categoryModel->load(428);
			$image = $main_category->getCateLandingImg();

			if ($image) {
				if (is_string($image)) {
					$url = $this->_storeManager->getStore()->getBaseUrl(
						\Magento\Framework\UrlInterface::URL_TYPE_WEB
					) . $image;

					//$url = $image;
				} else {
					throw new \Magento\Framework\Exception\LocalizedException(
						__('Something went wrong while getting the image url.')
					);
				}
			}
			return $url;
		}
		if ($this->_request->getFullActionName() == 'accounthome_index_index') {
			return $url = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) . 'orderimage.png';
		}

		if ($this->_request->getFullActionName() == 'checkout_cart_index' || $this->_request->getFullActionName() == 'checkout_index_index' || $this->_request->getFullActionName() == 'onepagecheckout_index_index') {
			return $url = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) . 'shopping-cart-banner.jpg';
		}
		if ($this->_request->getFullActionName() == 'catalog_category_view') {
			$_category = $this->_registry->registry('current_category');
			$image = $_category->getCateLandingImg();

			if ($image) {
				if (is_string($image)) {
					$url = $this->_storeManager->getStore()->getBaseUrl(
						\Magento\Framework\UrlInterface::URL_TYPE_WEB
					) . $image;

					//$url = $image;
				} else {
					throw new \Magento\Framework\Exception\LocalizedException(
						__('Something went wrong while getting the image url.')
					);
				}
			}

		} elseif ($this->_request->getFullActionName() == 'catalog_product_view') {
			$params = $this->_request->getParams();

			if (isset($params['id'])) {
				$product = $this->_productRepositoryFactory->create()->getById($params['id']);

				if ($product->getProductBanner()) {
					$url = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getProductBanner();
				} else {
					$categoryIds = $product->getCategoryIds();
					if (count($categoryIds)) {
						foreach ($categoryIds as $catId) {
							$_category = $this->_categoryModel->load($catId);
							if ($_category->getCateLandingImg()) {
								$image = $_category->getCateLandingImg();

								if ($image) {
									if (is_string($image)) {
										$url = $this->_storeManager->getStore()->getBaseUrl(
											\Magento\Framework\UrlInterface::URL_TYPE_WEB
										) . $image;

										//$url = $image;
									} else {
										throw new \Magento\Framework\Exception\LocalizedException(
											__('Something went wrong while getting the image url.')
										);
									}
								}

							}

						}

					}
				}

			}
		} else {
			$cmspages = array('cms_index_index', 'cms_page_view');
			if (in_array($this->_request->getFullActionName(), $cmspages)) {
				//CMS page
				$page = $this->_page->getData();

				if (isset($page['cms_hero_image'])) {
					$url = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'cms/hero/image/' . $page['cms_hero_image'];
				}
			}
		}

		return $url;
	}
	/**
	 * Provide own page title or pick it from Head Block
	 *
	 * @return string
	 */
	public function getPageTitle() {
		if (!empty($this->pageTitle)) {
			return $this->pageTitle;
		}

		$pageTitle = $this->pageConfig->getTitle()->getShort();

		return $this->shouldTranslateTitle() ? __($pageTitle) : $pageTitle;
	}

	/**
	 * Provide own page content heading
	 *
	 * @return string
	 */
	public function getPageHeading() {
		$pageTitle = !empty($this->pageTitle) ? $this->pageTitle : $this->pageConfig->getTitle()->getShortHeading();

		return $this->shouldTranslateTitle() ? __($pageTitle) : $pageTitle;
	}

	/**
	 * Set own page title
	 *
	 * @param string $pageTitle
	 * @return void
	 */
	public function setPageTitle($pageTitle) {
		$this->pageTitle = $pageTitle;
	}

	/**
	 * Check if page title should be translated
	 *
	 * @return bool
	 */
	private function shouldTranslateTitle(): bool {
		return $this->scopeConfig->isSetFlag(
			self::XML_PATH_HEADER_TRANSLATE_TITLE,
			ScopeInterface::SCOPE_STORE
		);
	}
}
