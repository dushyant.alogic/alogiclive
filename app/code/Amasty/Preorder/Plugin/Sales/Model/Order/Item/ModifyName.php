<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Plugin\Sales\Model\Order\Item;

use Amasty\Preorder\Model\Order\Item\GetPreorderInformation;
use Magento\Sales\Model\Order\Item;

class ModifyName
{
    /**
     * @var GetPreorderInformation
     */
    private $getPreorderInformation;

    public function __construct(GetPreorderInformation $getPreorderInformation)
    {
        $this->getPreorderInformation = $getPreorderInformation;
    }

    public function afterGetName(Item $subject, string $result): string
    {
        $preorderInformation = $this->getPreorderInformation->execute($subject);
        if ($preorderInformation->isPreorder()) {
            $result .=  ' ' . $preorderInformation->getNote();
        }

        return $result;
    }
}
