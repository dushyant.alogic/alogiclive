<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Plugin\ProductList;

use Amasty\Preorder\Model\ConfigProvider;
use Amasty\Preorder\Model\Product\Processor;
use Amasty\Xsearch\Block\Search\Product as XsearchProduct;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

class GetPreorderHtmlForList
{
    /**
     * @var Processor
     */
    private $processor;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(
        Processor $processor,
        ConfigProvider $configProvider
    ) {
        $this->processor = $processor;
        $this->configProvider = $configProvider;
    }

    /**
     * @param Template $subject
     * @param ProductInterface[] $productItems
     * @return string
     * @throws LocalizedException
     */
    public function get(Template $subject, array $productItems): string
    {
        if (!$this->configProvider->isEnabled() || $this->isSkipXsearchPopup($subject)) {
            return '';
        }

        $this->processor->execute($productItems);

        return $subject->getLayout()
            ->getBlock('preorder.list')
            ->setData('items', $productItems)
            ->toHtml();
    }

    /**
     * Xsearch popup processed via
     * @see \Amasty\Preorder\Plugin\Xsearch\Block\Search\Product\ChangeCartLabel
     *
     * @param Template $subject
     * @return bool
     */
    private function isSkipXsearchPopup(Template $subject): bool
    {
        return $subject instanceof XsearchProduct;
    }
}
