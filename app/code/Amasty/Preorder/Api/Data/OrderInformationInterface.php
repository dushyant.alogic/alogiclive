<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


namespace Amasty\Preorder\Api\Data;

interface OrderInformationInterface
{
    const MAIN_TABLE = 'amasty_preorder_order_preorder';
    const ID = 'id';
    const ORDER_ID = 'order_id';
    const PREORDER_FLAG = 'is_preorder';
    const WARNING = 'warning';

    /**
     * @return bool
     */
    public function isPreorder(): bool;

    /**
     * @param bool $isPreorder
     * @return OrderInformationInterface
     */
    public function setIsPreorder(bool $isPreorder): OrderInformationInterface;

    /**
     * @return string|null
     */
    public function getWarning(): ?string;

    /**
     * @param string $warning
     * @return OrderInformationInterface
     */
    public function setWarning(string $warning): OrderInformationInterface;
}
