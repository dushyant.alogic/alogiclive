<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\Indexer\Product;

use Magento\Framework\Indexer\AbstractProcessor;

class PreorderProcessor extends AbstractProcessor
{
    const INDEXER_ID = 'amasty_preorder_product_preorder';
}
