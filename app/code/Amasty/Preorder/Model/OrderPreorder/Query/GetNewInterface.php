<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\OrderPreorder\Query;

use Amasty\Preorder\Api\Data\OrderInformationInterface;

interface GetNewInterface
{
    public function execute(array $data = []): OrderInformationInterface;
}
