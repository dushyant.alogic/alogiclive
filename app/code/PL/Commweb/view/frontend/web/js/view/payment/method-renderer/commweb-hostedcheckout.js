/**
 * Created by Linh on 6/8/2016.
 */
define(
    [
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'PL_Commweb/js/action/set-hostedcheckout-method',
        'Magento_Checkout/js/model/payment/additional-validators'
    ],
     function ($, Component, setPaymentMethodAction, additionalValidators){
        'use strict';
       // var paymentMethod = ko.observable(null);

        return Component.extend({
            defaults: {
                template: 'PL_Commweb/payment/commweb-hosted'
            },
            getCode: function() {
                return 'commweb_hostedcheckout';
            },

            isActive: function() {
                return true;
            },
			
            continueToCommWeb: function () {
                if (this.validate() && additionalValidators.validate()) {
                    //update payment method information if additional data was changed
                    this.selectPaymentMethod();
                    setPaymentMethodAction();
                    return false;
                }
            }
            
        });
    }
);