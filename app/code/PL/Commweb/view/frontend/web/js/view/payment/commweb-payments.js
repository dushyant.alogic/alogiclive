define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            
            {
                type: 'commweb_direct',
                component: 'PL_Commweb/js/view/payment/method-renderer/commweb-direct'
            },
            {
                type: 'commweb_hostedcheckout',
                component: 'PL_Commweb/js/view/payment/method-renderer/commweb-hostedcheckout'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);