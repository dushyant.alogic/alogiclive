<?php

namespace Newsroom\Presskit\Block;

use Magento\Framework\View\Element\Template;
use Newsroom\Presskit\Model\ResourceModel\Image\Collection;
use Newsroom\Presskit\Model\ResourceModel\Image\CollectionFactory;

class Images extends \Magento\Framework\View\Element\Template {
	/**
	 * @var string
	 */
	protected $_template = 'Newsroom_Presskit::images.phtml';

	/**
	 * @var int
	 */
	private $reviewId;

	/**
	 * @var \Magento\Framework\Json\EncoderInterface
	 */
	private $jsonEncoder;

	/**
	 * @var CollectionFactory
	 */
	private $collectionFactory;


	


	
	protected $_backurl;
	public function __construct(
		Template\Context $context,
		CollectionFactory $collectionFactory,		
		\Magento\Framework\Json\EncoderInterface $jsonEncoder,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->jsonEncoder = $jsonEncoder;
		$this->collectionFactory = $collectionFactory;		
	}
	public function _prepareLayout() {
		$this->pageConfig->getTitle()->set(__('Presskit'));

		return parent::_prepareLayout();
	}
	/**
	 * @return int
	 */
	public function getBackUrl() {
		return $this->_backurl;
	}
	public function setBackUrl($backurl) {
		$this->_backurl = $backurl;
		return $this;
	}
	/**
	 * @return Collection
	 */
	public function getCollection() {

		if ($presskit_id = $this->getRequest()->getParam('presskit_id')) {
			$this->setPresskitId($presskit_id);
			if ($this->getRequest()->getParam('parent_id')) {
				$parent_id = $this->getRequest()->getParam('parent_id');
				$imagespage_url = $this->getUrl('presskit/index/view', ['presskit_id' => $this->getPresskitId(), 'parent_id' => $this->getRequest()->getParam('parent_id')]);
				$this->setBackUrl($imagespage_url);
			}

		}
		/** @var Collection $collection */
		$collection = $this->collectionFactory->create()
			->addFieldToSelect('*')
			->addFieldToFilter('presskit_id', $this->getPresskitId());

		return $collection;
	}


/*	public function getFullImagePath($item) {
		return $this->imageHelper->getFullPath($item->getPath());
	}*/


/*	public function getResizedImagePath($item) {
		return $this->imageHelper->resize($item->getPath(), $this->configHelper->getReviewImageWidth() * 2);
	}
*/

/*	public function getMaxHeight() {
		return $this->configHelper->getReviewImageWidth();
	}*/

	/**
	 * @return int
	 */
	public function getPresskitId() {
		return $this->presskitId;
	}

	/**
	 * @param $reviewId
	 *
	 * @return $this
	 */
	public function setPresskitId($presskitId) {
		$this->presskitId = $presskitId;
		return $this;
	}
}
