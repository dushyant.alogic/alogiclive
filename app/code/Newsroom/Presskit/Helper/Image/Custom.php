<?php
namespace Newsroom\Presskit\Helper\Image;

class Custom extends \Magento\Framework\Data\Form\Element\Image {
	protected function _getDeleteCheckbox() {
		return '';
	}
	public function getElementHtml() {
		$html = parent::getElementHtml();
		$html = str_replace('type="file"', 'type="file" multiple accept="image/*" ', $html);
		$html .= '<style>a[previewlinkid] {display: none;}</style>';
		return $html;
	}

}
