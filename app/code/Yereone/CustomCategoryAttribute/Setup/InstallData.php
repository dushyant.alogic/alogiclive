<?php

namespace Yereone\CustomCategoryAttribute\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface {
	/**
	 * @var EavSetupFactory
	 */
	private $eavSetupFactory;

	/**
	 * @param EavSetupFactory $eavSetupFactory
	 */
	public function __construct(EavSetupFactory $eavSetupFactory) {
		$this->eavSetupFactory = $eavSetupFactory;
	}

	/**
	 * Installs data for a module
	 *
	 * @param ModuleDataSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * @return void
	 */
	public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
		$setup->startSetup();

		$eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'is_essential',
			[

				'type' => 'int',
				'label' => 'Is Essential',
				'input' => 'select',
				'required' => false,
				'sort_order' => 10,
				'default' => '0',
				'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
				'group' => 'Is Landing Page',
			]

		);
		$eavSetup->addAttribute(
			\Magento\Catalog\Model\Category::ENTITY,
			'cate_essential_img',
			[

				'type' => 'varchar',
				'label' => 'Thumbnail Image',
				'input' => 'image',
				'backend' => 'Magento\Catalog\Model\Category\Attribute\Backend\Image',
				'required' => false,
				'sort_order' => 20,
				'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
				'group' => 'Essential Image',
			]

		);
		$setup->endSetup();
	}
}
