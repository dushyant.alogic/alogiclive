<?php

namespace StripeIntegration\Payments\Test\Integration\Mock\Events;

class ChargeRefunded extends AbstractEvent
{
    public function getChargeRefunded($totalAmount, $refundedAmount, $order)
    {
        $paymentIntentId = $order->getPayment()->getLastTransId();
        $orderIncrementId = $order->getIncrementId();

        return $this->object('charge.refunded', '{
      "id": "ch_1JEq0f2WmagXEVq494RkxyQs",
      "object": "charge",
      "amount": '.$totalAmount.',
      "amount_captured": '.$totalAmount.',
      "amount_refunded": '.$refundedAmount.',
      "application": null,
      "application_fee": null,
      "application_fee_amount": null,
      "balance_transaction": "txn_1JEq0f2WmagXEVq4e9jOIMa4",
      "billing_details": {
        "address": {
          "city": "Culver City",
          "country": "US",
          "line1": "1234 Doesnt Exst, Suite 123",
          "line2": null,
          "postal_code": "12345-6789",
          "state": "MI"
        },
        "email": "janedoe@example.com",
        "name": "Jane Doe",
        "phone": null
      },
      "calculated_statement_descriptor": "LUMA STORE EUROPE",
      "captured": true,
      "created": 1626676285,
      "currency": "usd",
      "customer": "cus_JsbDgvUdlXKX4D",
      "description": "Order #'.$orderIncrementId.' by Jane Doe",
      "destination": null,
      "dispute": null,
      "disputed": false,
      "failure_code": null,
      "failure_message": null,
      "fraud_details": {
      },
      "invoice": null,
      "livemode": false,
      "metadata": {
        "Module": "Magento2 v{{VERSION}}",
        "Order #": "'.$orderIncrementId.'",
        "Guest": "Yes"
      },
      "on_behalf_of": null,
      "order": null,
      "outcome": {
        "network_status": "approved_by_network",
        "reason": null,
        "risk_level": "normal",
        "risk_score": 47,
        "seller_message": "Payment complete.",
        "type": "authorized"
      },
      "paid": true,
      "payment_intent": "'.$paymentIntentId.'",
      "payment_method": "pm_1JEq0a2WmagXEVq4YglJH5ji",
      "payment_method_details": {
        "card": {
          "brand": "visa",
          "checks": {
            "address_line1_check": "pass",
            "address_postal_code_check": "pass",
            "cvc_check": "pass"
          },
          "country": "US",
          "exp_month": 12,
          "exp_year": 2022,
          "fingerprint": "rZMQtM121pFiy3SJ",
          "funding": "credit",
          "installments": null,
          "last4": "4242",
          "moto": null,
          "network": "visa",
          "three_d_secure": null,
          "wallet": null
        },
        "type": "card"
      },
      "receipt_email": "janedoe@example.com",
      "receipt_number": null,
      "receipt_url": "https://pay.stripe.com/receipts/acct_1031aK2WmagXEVq4/ch_1JEq0f2WmagXEVq494RkxyQs/rcpt_JsbDoYZTForjcCmla8AXX96yVAkKSkF",
      "refunded": false,
      "refunds": {
        "object": "list",
        "data": [
          {
            "id": "re_1JEq4t2WmagXEVq4sn50s2pa",
            "object": "refund",
            "amount": '.$refundedAmount.',
            "balance_transaction": "txn_1JEq4t2WmagXEVq44z4Tz7Nu",
            "charge": "ch_1JEq0f2WmagXEVq494RkxyQs",
            "created": 1626676547,
            "currency": "usd",
            "metadata": {
            },
            "payment_intent": "'.$paymentIntentId.'",
            "reason": "requested_by_customer",
            "receipt_number": null,
            "source_transfer_reversal": null,
            "status": "succeeded",
            "transfer_reversal": null
          }
        ],
        "has_more": false,
        "total_count": 1,
        "url": "/v1/charges/ch_1JEq0f2WmagXEVq494RkxyQs/refunds"
      },
      "review": null,
      "shipping": {
        "address": {
          "city": "Culver City",
          "country": "US",
          "line1": "1234 Doesnt Exst, Suite 123",
          "line2": null,
          "postal_code": "12345-6789",
          "state": "MI"
        },
        "carrier": null,
        "name": "Jane Doe",
        "phone": "",
        "tracking_number": null
      },
      "source": null,
      "source_transfer": null,
      "statement_descriptor": null,
      "statement_descriptor_suffix": null,
      "status": "succeeded",
      "transfer_data": null,
      "transfer_group": null
    }');
    }

    public function getChargeRefundedTwice($totalAmount, $firstRefundAmount, $secondRefundAmount, $order)
    {

        $paymentIntentId = $order->getPayment()->getLastTransId();
        $orderIncrementId = $order->getIncrementId();

        return $this->object('charge.refunded', '{
      "id": "ch_1JEq0f2WmagXEVq494RkxyQs",
      "object": "charge",
      "amount": '.$totalAmount.',
      "amount_captured": '.$totalAmount.',
      "amount_refunded": '.($firstRefundAmount + $secondRefundAmount).',
      "application": null,
      "application_fee": null,
      "application_fee_amount": null,
      "balance_transaction": "txn_1JEq0f2WmagXEVq4e9jOIMa4",
      "billing_details": {
        "address": {
          "city": "Culver City",
          "country": "US",
          "line1": "1234 Doesnt Exst, Suite 123",
          "line2": null,
          "postal_code": "12345-6789",
          "state": "MI"
        },
        "email": "janedoe@example.com",
        "name": "Jane Doe",
        "phone": null
      },
      "calculated_statement_descriptor": "LUMA STORE EUROPE",
      "captured": true,
      "created": 1626676285,
      "currency": "usd",
      "customer": "cus_JsbDgvUdlXKX4D",
      "description": "Order #'.$orderIncrementId.' by Jane Doe",
      "destination": null,
      "dispute": null,
      "disputed": false,
      "failure_code": null,
      "failure_message": null,
      "fraud_details": {
      },
      "invoice": null,
      "livemode": false,
      "metadata": {
        "Module": "Magento2 v{{VERSION}}",
        "Order #": "'.$orderIncrementId.'",
        "Guest": "Yes"
      },
      "on_behalf_of": null,
      "order": null,
      "outcome": {
        "network_status": "approved_by_network",
        "reason": null,
        "risk_level": "normal",
        "risk_score": 47,
        "seller_message": "Payment complete.",
        "type": "authorized"
      },
      "paid": true,
      "payment_intent": "'.$paymentIntentId.'",
      "payment_method": "pm_1JEq0a2WmagXEVq4YglJH5ji",
      "payment_method_details": {
        "card": {
          "brand": "visa",
          "checks": {
            "address_line1_check": "pass",
            "address_postal_code_check": "pass",
            "cvc_check": "pass"
          },
          "country": "US",
          "exp_month": 12,
          "exp_year": 2022,
          "fingerprint": "rZMQtM121pFiy3SJ",
          "funding": "credit",
          "installments": null,
          "last4": "4242",
          "moto": null,
          "network": "visa",
          "three_d_secure": null,
          "wallet": null
        },
        "type": "card"
      },
      "receipt_email": "janedoe@example.com",
      "receipt_number": null,
      "receipt_url": "https://pay.stripe.com/receipts/acct_1031aK2WmagXEVq4/ch_1JEq0f2WmagXEVq494RkxyQs/rcpt_JsbDoYZTForjcCmla8AXX96yVAkKSkF",
      "refunded": false,
      "refunds": {
        "object": "list",
        "data": [
          {
            "id": "re_1JEsef2WmagXEVq4wai1LAZA",
            "object": "refund",
            "amount": '.$secondRefundAmount.',
            "balance_transaction": "txn_1JEseg2WmagXEVq4Oh8UEUUj",
            "charge": "ch_1JEq0f2WmagXEVq494RkxyQs",
            "created": 1626686453,
            "currency": "usd",
            "metadata": {
            },
            "payment_intent": "'.$paymentIntentId.'",
            "reason": "requested_by_customer",
            "receipt_number": null,
            "source_transfer_reversal": null,
            "status": "succeeded",
            "transfer_reversal": null
          },
          {
            "id": "re_1JEq4t2WmagXEVq4sn50s2pa",
            "object": "refund",
            "amount": '.$firstRefundAmount.',
            "balance_transaction": "txn_1JEq4t2WmagXEVq44z4Tz7Nu",
            "charge": "ch_1JEq0f2WmagXEVq494RkxyQs",
            "created": 1626676547,
            "currency": "usd",
            "metadata": {
            },
            "payment_intent": "'.$paymentIntentId.'",
            "reason": "requested_by_customer",
            "receipt_number": null,
            "source_transfer_reversal": null,
            "status": "succeeded",
            "transfer_reversal": null
          }
        ],
        "has_more": false,
        "total_count": 2,
        "url": "/v1/charges/ch_1JEq0f2WmagXEVq494RkxyQs/refunds"
      },
      "review": null,
      "shipping": {
        "address": {
          "city": "Culver City",
          "country": "US",
          "line1": "1234 Doesnt Exst, Suite 123",
          "line2": null,
          "postal_code": "12345-6789",
          "state": "MI"
        },
        "carrier": null,
        "name": "Jane Doe",
        "phone": "",
        "tracking_number": null
      },
      "source": null,
      "source_transfer": null,
      "statement_descriptor": null,
      "statement_descriptor_suffix": null,
      "status": "succeeded",
      "transfer_data": null,
      "transfer_group": null
    }');
    }
}


