<?php

namespace StripeIntegration\Payments\Test\Integration\Mock\Events;

abstract class AbstractEvent
{
    protected $eventID;

    public function __construct()
    {
        $this->eventID = time();
    }

    protected function getEventId()
    {
        return 'evt_xxx_' . $this->eventID++;
    }

    protected function object($type, $data)
    {
        return '{
  "id": "evt_xxx_'.$this->getEventId().'",
  "object": "event",
  "api_version": "2020-08-27",
  "created": 1626695217,
  "data": {
    "object": '.$data.'
    ,
    "previous_attributes": {
      "amount_captured": 0,
      "amount_refunded": 0,
      "balance_transaction": null,
      "captured": false,
      "refunds": {
        "data": [
        ],
        "total_count": 0
      }
    }
  },
  "livemode": false,
  "pending_webhooks": 1,
  "request": {
    "id": "req_VVnFrdETbdOFme",
    "idempotency_key": null
  },
  "type": "'.$type.'"
}';
    }
}
