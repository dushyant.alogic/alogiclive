<?php

namespace StripeIntegration\Payments\Test\Integration\OpenSource\Captures\Stripe;

class PartialTest extends \PHPUnit\Framework\TestCase
{
    public function setUp(): void
    {
        $this->objectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
        $this->cartManagement = $this->objectManager->get(\Magento\Quote\Api\CartManagementInterface::class);
        $this->webhooks = $this->objectManager->get(\StripeIntegration\Payments\Helper\Webhooks::class);
        $this->request = $this->objectManager->get(\Magento\Framework\App\Request\Http::class);
        $this->helper = $this->objectManager->get(\StripeIntegration\Payments\Helper\Generic::class);
        $this->mockChargeCaptured = $this->objectManager->get(\StripeIntegration\Payments\Test\Integration\Mock\Events\ChargeCaptured::class);
        $this->mockPaymentIntentSucceeded = $this->objectManager->get(\StripeIntegration\Payments\Test\Integration\Mock\Events\PaymentIntentSucceeded::class);
    }

    /**
     * @magentoConfigFixture current_store payment/stripe_payments/active 1
     * @magentoConfigFixture current_store payment/stripe_payments_basic/stripe_mode test
     * @magentoConfigFixture current_store payment/stripe_payments_basic/stripe_test_pk pk_test_51Ig7MJHLyfDWKHBqqOpnyTkavM0LlpuH1QnrM1IsRGe26qwwo1uhQZbHyrnaiJuWpiIEkoFHgzgZoeLlfLOXp4ef00ApmFEugB
     * @magentoConfigFixture current_store payment/stripe_payments_basic/stripe_test_sk sk_test_51Ig7MJHLyfDWKHBqRZ6h9gRk1C738LWP1ljHVAyWsON7CIennpQV25sHvISdpbfHBqQWCNBTivTsKiIFjAPJhyB500ytiSiSSF
     * @magentoConfigFixture current_store payment/stripe_payments/checkout_mode 0
     * @magentoConfigFixture current_store payment/stripe_payments/payment_action authorize
     *
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Data/Products.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/USGuestQuote.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Carts/NormalCart.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/ShippingAddress/NewYorkAddress.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/ShippingMethod/FlatRateShippingMethod.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/BillingAddress/NewYorkAddress.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/PaymentMethod/SuccessCard.php
     */
    public function testPartialCapture()
    {
        // Tests MAGENTO-68
        $quote = $this->objectManager->create(\Magento\Quote\Model\Quote::class);
        $quote->load('test_quote', 'reserved_order_id');

        $order = $this->cartManagement->submit($quote);
        $orderIncrementId = $order->getIncrementId();
        $currency = $order->getOrderCurrencyCode();
        $amount = $this->helper->convertMagentoAmountToStripeAmount($order->getGrandTotal(), $currency);
        $amountCaptured = 500;
        $amountPaid = ($amountCaptured / 100);

        $this->assertEquals("processing", $order->getStatus());
        $this->assertEquals(0, $order->getTotalPaid());
        $this->assertEquals($order->getGrandTotal(), $order->getTotalDue());

        // charge.captured
        $this->request->setMethod("POST");
        $chargeCapturedEvent = $this->mockChargeCaptured->getChargeCaptured($amount, $amountCaptured, $order);
        $this->request->setContent($chargeCapturedEvent);
        $this->webhooks->dispatchEvent();

        // payment_intent.succeeded
        $paymentIntentSucceededEvent = $this->mockPaymentIntentSucceeded->getPaymentIntentSucceeded($amount, $amountCaptured, $order);
        $this->request->setContent($paymentIntentSucceededEvent);
        $this->webhooks->dispatchEvent();

        // Refresh the order object
        $order = $this->helper->loadOrderByIncrementId($orderIncrementId);
        $this->assertEquals("processing", $order->getStatus());
        $this->assertEquals($amountPaid, $order->getTotalPaid());
        $this->assertEquals($order->getGrandTotal() - $amountPaid, $order->getTotalDue());
    }
}
