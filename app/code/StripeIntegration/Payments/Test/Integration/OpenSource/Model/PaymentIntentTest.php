<?php

namespace StripeIntegration\Payments\Test\Integration\OpenSource\Model;

use PHPUnit\Framework\Constraint\StringContains;

class PaymentIntentTest extends \PHPUnit\Framework\TestCase
{
    public function setUp(): void
    {
        $this->objectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
        $this->cartManagement = $this->objectManager->get(\Magento\Quote\Api\CartManagementInterface::class);
    }

    /**
     * @magentoConfigFixture current_store payment/stripe_payments/active 1
     * @magentoConfigFixture current_store payment/stripe_payments_basic/stripe_mode test
     * @magentoConfigFixture current_store payment/stripe_payments_basic/stripe_test_pk pk_test_51Ig7MJHLyfDWKHBqqOpnyTkavM0LlpuH1QnrM1IsRGe26qwwo1uhQZbHyrnaiJuWpiIEkoFHgzgZoeLlfLOXp4ef00ApmFEugB
     * @magentoConfigFixture current_store payment/stripe_payments_basic/stripe_test_sk sk_test_51Ig7MJHLyfDWKHBqRZ6h9gRk1C738LWP1ljHVAyWsON7CIennpQV25sHvISdpbfHBqQWCNBTivTsKiIFjAPJhyB500ytiSiSSF
     * @magentoConfigFixture current_store payment/stripe_payments/checkout_mode 0
     *
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Data/Products.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/USGuestQuote.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Carts/NormalCart.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/ShippingAddress/NewYorkAddress.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/ShippingMethod/FlatRateShippingMethod.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/BillingAddress/NewYorkAddress.php
     * @magentoDataFixture ../../../../app/code/StripeIntegration/Payments/Test/Integration/_files/Quotes/PaymentMethod/InsufficientFundsCard.php
     */
    public function testPreloadFromCache()
    {
        // Tests MAGENTO-65
        $quote = $this->objectManager->create(\Magento\Quote\Model\Quote::class);
        $quote->load('test_quote', 'reserved_order_id');

        $cartManagementDataResource = $this->getMockBuilder(\Magento\Quote\Api\CartManagementInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $cartManagementDataResource->method('submit')->willThrowException(new CouldNotSaveException('Your card has insufficient funds.'));
        $order = $this->cartManagement->submit($quote);

        $cartManagementDataResource->method('submit')->willThrowException(new CouldNotSaveException('Your card has insufficient funds.'));
        $order = $this->cartManagement->submit($quote);

        $cartManagementDataResource->method('submit')->willThrowException(new CouldNotSaveException('Your card has insufficient funds.'));
        $order = $this->cartManagement->submit($quote);
    }
}
