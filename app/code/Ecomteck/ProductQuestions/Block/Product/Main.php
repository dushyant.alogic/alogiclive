<?php
namespace Ecomteck\ProductQuestions\Block\Product;

/**
 * Class Main
 */
class Main extends \Magento\Framework\View\Element\Template {
	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $coreRegistry;

	/**
	 * Question Collection
	 *
	 * @var \Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory
	 */
	protected $questionCollectionFactory;
	protected $_storeManager;
	protected $_store;
	/**
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Magento\Framework\Registry $registry
	 * @param \Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory $collectionFactory
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\Registry $registry,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Store\Model\Store $store,
		\Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory $collectionFactory,
		array $data = []
	) {
		$this->coreRegistry = $registry;
		$this->_storeManager = $storeManager;
		$this->_store = $store;
		$this->questionCollectionFactory = $collectionFactory;
		parent::__construct($context, $data);

		$this->setTabTitle();
	}
	function _prepareLayout() {}
	public function getAjaxUrl() {
		return $this->_storeManager->getStore()->getUrl('question/product/autocomplete');
	}
}
