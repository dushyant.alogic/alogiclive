<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_ProductQuestions
 * @copyright   Copyright (c) 2019 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\ProductQuestions\Block\Product\View;
use \Zendesk\API\HttpClient as ZendeskAPI;

class ListView extends \Magento\Framework\View\Element\Template {
	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $coreRegistry = null;

	/**
	 * @var \Magento\Framework\DataObjectFactory
	 */
	protected $dataObjectFactory;

	/**
	 * @var \Ecomteck\ProductQuestions\Model\UserType
	 */
	protected $userType;

	/**
	 * @var \Ecomteck\ProductQuestions\Model\Config\Source\FormatDateTime
	 */
	protected $formatDateTime;

	/**
	 * @var \Ecomteck\ProductQuestions\Model\ResourceModel\Answer\CollectionFactory
	 */
	protected $answerColFactory;

	/**
	 * @var \Ecomteck\ProductQuestions\Helper\Data
	 */
	protected $questionData;
	protected $helperData;
	/**
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Magento\Framework\Registry $coreRegistry
	 * @param \Magento\Framework\DataObjectFactory $dataObjectFactory
	 * @param \Ecomteck\ProductQuestions\Model\UserType $userType
	 * @param \Ecomteck\ProductQuestions\Model\Config\Source\FormatDateTime $formatDateTime
	 * @param \Ecomteck\ProductQuestions\Model\ResourceModel\Answer\CollectionFactory $answerColFactory
	 * @param \Ecomteck\ProductQuestions\Helper\Data $questionData
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\Registry $coreRegistry,
		\Magento\Framework\DataObjectFactory $dataObjectFactory,
		\Ecomteck\ProductQuestions\Model\UserType $userType,
		\Ecomteck\ProductQuestions\Model\Config\Source\FormatDateTime $formatDateTime,
		\Ecomteck\ProductQuestions\Model\ResourceModel\Answer\CollectionFactory $answerColFactory,
		\Ecomteck\ProductQuestions\Helper\Data $questionData,
		\Alogic\Customtheme\Helper\Data $helperData,
		array $data = []
	) {
		$this->coreRegistry = $coreRegistry;
		$this->dataObjectFactory = $dataObjectFactory;
		$this->userType = $userType;
		$this->formatDateTime = $formatDateTime;
		$this->answerColFactory = $answerColFactory;
		$this->questionData = $questionData;
		$this->helperData = $helperData;
		parent::__construct($context, $data);
	}

	/**
	 * Retrieve the question list asked for the product
	 *
	 * @param array $data
	 */
	public function getQuestionProductList() {
		$questions = $this->coreRegistry->registry('ecomteck_question_product');
		$approvedQuestions = [];

		$username = $this->helperData->getGeneralConfig('username');
		$token = $this->helperData->getGeneralConfig('tokenid');
		$subdomain = $this->helperData->getGeneralConfig('subdomain');

		$client = new ZendeskAPI($subdomain);
		$client->setAuth('basic', ['username' => $username, 'token' => $token]);

		foreach ($questions as $question) {
			$zendeskTicketId = $question->getZendeskId();
			if ($zendeskTicketId) {
				$tickets = $client->tickets()->find($zendeskTicketId);

				$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/zendesk.log');
				$logger = new \Zend\Log\Logger();
				$logger->addWriter($writer);
				$logger->info("ticket Status");
				$logger->info($tickets->ticket->status);
				$logger->info($tickets->ticket->description);
				$logger->info(print_r($tickets->ticket, true));
				if ($tickets->ticket->status == 'solved') {
					$approvedQuestions[] = $question;
				}

			}

		}

		$data = [];
		if (!empty($approvedQuestions)) {
			$data['total_page'] = $questions->getLastPageNumber();
			$data['current_page'] = $questions->getCurPage();
			$data['allow_to_reply'] = $this->questionData->getAllowToReply();
			$data['data'] = null;
			$data['next_url'] = $this->getUrl(
				'question/product/listAjax',
				[
					'_secure' => $this->getRequest()->isSecure(),
					'id' => $this->getRequest()->getParam('id'),
					'page' => $data['current_page'] + 1,
				]
			);
			foreach ($approvedQuestions as $question) {
				$data['data'][] = $this->getQuestionInfo($question);
			}
		}
		return $data;
	}

	/**
	 * Retrieve the question information
	 *
	 * @param \Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory $question
	 * @return object
	 */
	public function getQuestionInfo($question) {
		/** @var \Magento\Framework\DataObjectFactory $dataObjectFactory */
		$questionData = $this->dataObjectFactory->create()
			->setId($question->getQuestionId())
			->setProductId($question->getProductId())
			->setTitle(nl2br($question->getQuestionDetail()))
			->setAuthorName(ucwords(strtolower($question->getQuestionAuthorName())))
			->setFirstCharacter(substr($question->getQuestionAuthorName(), 0, 1))
			->setLikes($question->getQuestionLikes())
			->setDislikes($question->getQuestionDislikes())
			->setAskedBy($this->getAddedBy($question->getQuestionUserTypeId()))
			->setCreatedAt($this->formatDateTime->formatCreatedAt($question->getQuestionCreatedAt()));
		$answers = [];
		foreach ($this->getAnswerList($question) as $answer) {
			$answers[] = $this->getAnswerInfo($answer);
		}
		$questionData->setAnswers($answers);
		return $questionData->getData();
	}

	/**
	 * Retrieve the added by
	 *
	 * @param int $userTypeId
	 * @return string
	 */
	protected function getAddedBy($userTypeId) {
		return $this->userType->getUserTypeText($userTypeId);
	}

	/**
	 * Retrieve the answer information
	 *
	 * @param \Ecomteck\ProductQuestions\Model\ResourceModel\Answer\CollectionFactory $answer
	 * @return array
	 */
	protected function getAnswerInfo($answer) {
		/** @var \Magento\Framework\DataObjectFactory $dataObjectFactory */
		return $this->dataObjectFactory->create()
			->setContent(nl2br($answer['answer_detail']))
			->getData();
	}

	/**
	 * Get list of answers of the question
	 *
	 * @param \Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory $question
	 * @return \Ecomteck\ProductQuestions\Model\ResourceModel\Answer\CollectionFactory
	 */
	public function getAnswerList($question) {

		$username = $this->helperData->getGeneralConfig('username');
		$token = $this->helperData->getGeneralConfig('tokenid');
		$subdomain = $this->helperData->getGeneralConfig('subdomain');

		$client = new ZendeskAPI($subdomain);
		$client->setAuth('basic', ['username' => $username, 'token' => $token]);
		$zendeskTicketId = $question->getZendeskId();
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/zendesk.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info("Collection data");
		$comments = [];
		if ($zendeskTicketId) {
			$logger->info("Collection data111");
			$tickets = $client->tickets($zendeskTicketId)->comments()->findAll();
			$logger->info("Collection data2222");
			//$logger->info(print_r($tickets, true));
			//$tickets = $client->tickets()->find($zendeskTicketId);

			$comments = $tickets->comments;
			unset($comments[0]);
			$logger->info(print_r($comments, true));
		}
		$answers = [];
		foreach ($comments as $comment) {
			$answers[] = ['answer_detail' => $comment->plain_body, 'answer_id' => 3, 'zendesk_id' => '', 'answer_author_name' => '', 'answer_author_email' => '', 'question_id' => '6', 'answer_status_id' => '', 'answer_user_type_id' => '', 'answer_user_id' => '', 'answer_created_by' => '', 'answer_created_by' => '', 'answer_visibility_id' => '', 'answer_likes' => '', 'answer_dislikes' => '', 'answer_created_at' => ''];
		}

		$collection = $this->answerColFactory->create()->addFieldToFilter(
			'main_table.question_id', $question->getQuestionId()
		)->addStatusFilter(
			\Ecomteck\ProductQuestions\Model\Status::STATUS_APPROVED
		)->addVisibilityFilter(
			\Ecomteck\ProductQuestions\Model\Visibility::VISIBILITY_VISIBLE
		);
		$logger->info("Collection data");
		$logger->info(print_r($answers, true));
		return $answers;
	}
}
