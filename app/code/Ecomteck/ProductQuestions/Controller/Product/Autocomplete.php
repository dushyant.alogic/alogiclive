<?php
/**
 * Ecomteck
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ecomteck.com license that is
 * available through the world-wide-web at this URL:
 * https://ecomteck.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Ecomteck
 * @package     Ecomteck_ProductQuestions
 * @copyright   Copyright (c) 2019 Ecomteck (https://ecomteck.com/)
 * @license     https://ecomteck.com/LICENSE.txt
 */

namespace Ecomteck\ProductQuestions\Controller\Product;

/**
 * Class ListAjax
 * @package Ecomteck\ProductQuestions\Controller\Product
 */
class Autocomplete extends \Magento\Framework\App\Action\Action {
	protected $_resultJsonFactory;
	/**
	 * Catalog product model
	 *
	 * @var \Magento\Catalog\Api\ProductRepositoryInterface
	 */
	protected $productRepository;

	/**
	 * Core model store manager interface
	 *
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;

	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $coreRegistry = null;

	/**
	 * Question Collection
	 *
	 * @var \Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory
	 */
	protected $questionCollectionFactory;

	/**
	 * @var \Magento\Framework\Controller\Result\RawFactory
	 */
	protected $resultRawFactory;

	/**
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
	protected $resultPageFactory;

	/**
	 * @var \Ecomteck\ProductQuestions\Helper\Data
	 */
	protected $questionData;

	/**
	 * @param \Magento\Framework\App\Action\Context $context
	 * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
	 * @param \Magento\Store\Model\StoreManagerInterface $storeManager
	 * @param \Magento\Framework\Registry $coreRegistry
	 * @param ProductStatus $productStatus
	 * @param ProductVisibility $productVisibility
	 * @param \Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory $collectionFactory
	 * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
	 * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
	 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
	 */
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Registry $coreRegistry,
		\Ecomteck\ProductQuestions\Model\ResourceModel\Question\CollectionFactory $collectionFactory,
		\Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Ecomteck\ProductQuestions\Helper\Data $questionData
	) {
		$this->_resultJsonFactory = $resultJsonFactory;
		$this->productRepository = $productRepository;
		$this->storeManager = $storeManager;
		$this->coreRegistry = $coreRegistry;
		$this->questionCollectionFactory = $collectionFactory;
		$this->resultRawFactory = $resultRawFactory;
		$this->resultPageFactory = $resultPageFactory;
		$this->questionData = $questionData;
		parent::__construct($context);
	}

	public function execute() {

		$postMessage = $this->getRequest()->getPost();

		$query = preg_replace('/[^A-Za-z0-9\ \_\'\-]/', '', $postMessage['query']);
		$collection = $this->questionCollectionFactory->create()->addStoreFilter(
			['0', $this->storeManager->getStore()->getId()]
		);
		$collection = $this->questionCollectionFactory->create()
			->addStoreFilter($this->storeManager->getStore()->getId())
			->setProductSku()
			->addFieldToFilter('sku', array('like' => '%' . $query . '%'))
			->setPageSize(5)
			->load();

		$productList = [];
		$i = 1;

		foreach ($collection as $question) {
			$url = $this->storeManager->getStore()->getUrl("question/view/index/", ["id" => $question['question_id']]);
			$productList[$i]['question_detail'] = $question['question_detail'];
			$productList[$i]['url'] = $url;

			$i++;
		}

		if ($collection->getSize() > 0) {
			return $this->_resultJsonFactory->create()->setData($productList);
		} else {
			return $this->_resultJsonFactory->create()->setData([]);
		}
	}
	public function getProductCollection($categoryId) {
		return $this->getCategory($categoryId)->getProductCollection()->addAttributeToSelect('*');
	}
}
