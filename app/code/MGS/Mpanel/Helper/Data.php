<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MGS\Mpanel\Helper;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

/**
 * Contact base helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper {
	protected $_storeManager;

	protected $_date;

	protected $_url;

	protected $_filesystem;

	protected $_request;

	protected $_acceptToUsePanel = false;

	protected $_useBuilder = false;

	protected $_customer;

	/**
	 * @var \Magento\Framework\Xml\Parser
	 */
	private $_parser;

	/**
	 * Asset service
	 *
	 * @var \Magento\Framework\View\Asset\Repository
	 */
	protected $_assetRepo;

	protected $filterManager;

	/**
	 * Block factory
	 *
	 * @var \Magento\Cms\Model\BlockFactory
	 */
	protected $_blockFactory;
	/**
	 * Page factory
	 *
	 * @var \Magento\Cms\Model\PageFactory
	 */
	protected $_pageFactory;

	protected $_file;

	/**
	 * @var \Magento\Framework\ObjectManagerInterface
	 */
	protected $_objectManager;

	protected $_fullActionName;

	protected $_currentCategory;

	protected $_currentProduct;

	protected $_category;

	protected $scopeConfig;

	protected $_ioFile;

	protected $_moduleManager;

	public function __construct(
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Stdlib\DateTime\DateTime $date,
		\Magento\Framework\ObjectManagerInterface $objectManager,
		\Magento\Framework\Url $url,
		\Magento\Framework\Filesystem $filesystem,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Framework\View\Element\Context $context,
		\Magento\Cms\Model\BlockFactory $blockFactory,
		\Magento\Catalog\Model\Category $category,
		\Magento\Cms\Model\PageFactory $pageFactory,
		\Magento\Framework\Filesystem\Driver\File $file,
		\Magento\Framework\Filesystem\Io\File $ioFile,
		\Magento\Framework\Xml\Parser $parser,
		\Magento\Framework\Module\Manager $moduleManager,
		CustomerSession $customerSession
	) {
		$this->scopeConfig = $context->getScopeConfig();
		$this->_storeManager = $storeManager;
		$this->_date = $date;
		$this->_url = $url;
		$this->_filesystem = $filesystem;
		$this->customerSession = $customerSession;
		$this->_objectManager = $objectManager;
		$this->_category = $category;
		$this->_request = $request;
		$this->filterManager = $context->getFilterManager();
		$this->_assetRepo = $context->getAssetRepository();
		$this->_blockFactory = $blockFactory;
		$this->_pageFactory = $pageFactory;
		$this->_file = $file;
		$this->_ioFile = $ioFile;
		$this->_moduleManager = $moduleManager;
		$this->_parser = $parser;

		$this->_fullActionName = $this->_request->getFullActionName();

		if ($this->_fullActionName == 'catalog_category_view') {
			$this->_currentCategory = $this->getCurrentCategory();
		}

		if ($this->_fullActionName == 'catalog_product_view') {
			$this->_currentProduct = $this->getCurrentProduct();
		}
	}

	public function getWebPImage($image,$_product,$type){


			if (exif_imagetype($image) != IMAGETYPE_JPEG) {
			    return $image;
			}

			$webp_path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('webpimages/');
			
			$url_components = parse_url($image);
			parse_str($url_components['query'], $params);

			if(isset($params['id'])){
				$image_webp_name = $_product->getSku().'-'.$params['id'].'-'.$type.'.webp';	
			}else{
				$image_webp_name = $_product->getSku().'-'.$type.'.webp';
			}

			$converted_image = $this->_url->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) .'webpimages/'.$image_webp_name;

			if(!file_exists($webp_path.$image_webp_name)){

				$image_array = explode('.',$image);
				$image = imagecreatefromjpeg($image);
				$result = imagewebp($image, $webp_path.$image_webp_name, 80);

				if($result){
					return $converted_image;
				}
			}else{
				return $converted_image;
			}
			return $image;
		}
	
	public function getSalableQty($_product) {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$StockState = $objectManager->get('\Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
		$qtys = $StockState->execute($_product->getSku());
		if ($this->_storeManager->getStore()->getCode() == 'in') {
			return $this->searchForId('India', $qtys);

		} elseif ($this->_storeManager->getStore()->getCode() == 'uk') {

			return $this->searchForId('United Kingdom', $qtys);
		} elseif ($this->_storeManager->getStore()->getCode() == 'default') {

			return $this->searchForId('Default Stock', $qtys);
		} elseif ($this->_storeManager->getStore()->getCode() == 'us') {

			return $this->searchForId('United States', $qtys);
		} else {
			return $this->searchForId('Default Stock', $qtys);
		}
		return false;
	}
	public function exludePromoSkus(){
		return ['SB-CT14BD','SB-WC12TBD','SB-WCC15TBD','SB-SCC08BD','SB-M10G2','SB-M10'];
	}
	public function getPromoGAN30Skus(){
		return ['DCG1X100-USANZ','WCM3X67-SGR','WCM1X67-SGR','WCG2X68SGR-US','WCG4X100XXX-US','WCG4X100SGR-US','WCG1X30SGR-US','WC1X20-US','WCG1X65-US','WCG4X100-US','WCG1X100-US','WCG2X68-US','WCC60','WCC60WH','WCC2A45WH','DCG1X100-USANZ','WCM3X67-SGR','WCM1X67-SGR','WCG2X40-US','WCG2X32-US','DCG1X100-USANZ','WCM3X67-SGR','WCM1X67-SGR','WCC60','WCC60WH','WCC2A45WH','WCG1X20SGR-UK','WCM3X67-SGR','WCM1X67-SGR','WCG1X30SGR-EU','WCG2X63-UK','WCG2X32-UK','WCG1X65-UK','WCG1X100-UK','WCG4X100-UK','WCC60','WCG2X40-EU','WCG2X32-EU','WC1X20-EU','WCG1X65-EU','WCG2X63-EU','WCG1X100-EU','WCG4X100-EU','WCC60WH','WCC2A45WH','DCG1X100-EUUK','WCM3X67-SGR','WCM1X67-SGR','DCG1X100-EUUK','WCM3X67-SGR','WCM1X67-SGR','WCC60','WCC60WH','WCC2A45WH'];
	}
	
	public function getMD30Skus(){
		return ['VPLUC10A100','ULCGEMN-SGR','ULCVGMN-SGR','VU3DUP','TB3D2D24K-H','UCPD1O-SGR','UCPD1O-BLK','TB3D2DPBL-SGR','ULDUNI-SGR','ULDPLS-SLV','TB3D2HDPBL-SGR','TB3DTRG2','DUTHDPR','DUCMA3','DUPRDX2-WW','DUPRDX3-WW','DUPRMX2-WW','DUPRMX3-WW','UD29A','UCVGA-ADP','U2CMC-01BLK','VPLUC3AGE','UC2HD-ADP','UC2DP-ADP','ULUCDP-ADP','ULC35APW-SGR','ULCC203-SGR','UCANCHD','UCFUPRGV-SGR','ULCGE-SGR','AALNBS-SGR','ALASS','C5-50-Blue','C6-15-Grey','C6A-25-Blue-SH','C6A-20-Blue-SH','C6A-02-Blue-SH','C6-30-Blue','C6A-1.5-Blue-SH','LCLC-25-OM3','SCSC-20-OS2','LCLC-20-OS2','LCLC-02-OM4','LCST-25-OM3','LCLC-15-OM4','STST-20-OS2','LCLC-50-OS2','SCASC-20-OS2','SCASCA-20-OS2','U2CSH-SGR','ALDHDLR','USB2-MCAB-ADP','U32HDDVVG','USB3HDDV-ADP','U3DVVG-ADP','U3-TCA01-MM','VPLUCAGE','VPLU3H7AP','VPLUCGE3ACH-SGR','MU28P-030SLV','VPLU3H10AP','VPLUCHDACH-SGR','MP-UCVGGECH','VPLUCCR2ACH-SGR','U3-TCC02-MM','U33HGHEDF','ELUCDV-02RBLK','MP-UC2HVGECH','ELPCC201-BK','VPCA104','PAW8PM067-SGR','VPM2CA102SGRC2','MP2CA156SGR','WCC2A45WH','QC10MSGR','QC10MSLV','QC10MCGD','QC10PCMBLK','P10QC10P18-BK','P10QC10P18-WH','QC10AWNQH','WCG1X100-ANZ','WCG1X65-ANZ','WCG2X68-ANZ','WCG2X40-ANZ','DUPRDX2-100','WCG4X100-UK','DP-10-MM','MDP-MDP-02-MM','VGA-MM-25','MDP-DVI-02-MM','HDMI-30-MM','MF-C19C20-10','MDP-DP-02-MM','DP-DVI-ADPC','VGA-MM-15-APS','VGA-MM-20-APS','VGA-MM-30-APS','MDP-SPL-ADP','VGA-HDUP','DP-VGDVHD-ADP','DP-HD4K-02-MM','DP-VGA-ADPC','HDAS-V203','ELMDPDP-01','DVDPU-ACTV','HDVG-MM-02','SULHD02-SGR','SULHD03-SGR','PHD-05-MM-V2','VELUC4A36','ULQC10W-SGR-AUNZ','HD4KSPL02-ADP','ULDNA-SGR','ULDNA-SLV','VWCA60','ULCHDPD01-SGR','ULCHDPD02-SGR','DUCDDV3','ULDPLSV2-SGR','DTS-SGR','UAFUUA-SGR','TB4D3TB','TB4H3TB','MSP21CS15W','MSP31CS15WAU','DUPRMX2-100','UCFUHDV2-SGR','UCFUPRGVV2-SGR','WCG4X100SGR-ANZ','DUCH2','DUCD2','WCG2X68SGR-ANZ','WCM1X67-SGR','ULC2HDPD-SGR','TB4C1M','temp-test-sku','temp-test-sku2-1','temp-test-sku2-2','U32ABRBK','U22AARBK','U22ABRBK','ULCSDMN-SGR','ULCDPMN-SGR','UCTDHDES','ULDUNI-SLV','ULDNAG2-SGR','ULDNAG2-SLV','ULDWAV-SGR','ULDNAMN-UA-SGR','ULDNAMN-UA-SLV','ULDWAV-SLV','DUTHD','SP7PBLH-BLK','UCMDP-ADP','UC3AGE','UCVGDVHD-ADP','UC3ACR','UCH2C2A','ULCHD01-SGR','ULCHD02-SGR','ULCDP01-SGR','ULCDP02-SGR','ULC8P1.5-SGR','ULC8P1.5-SLV','ULCAA-SLV','ULCC2030-SGR','ULCC21.5-SGR','ULCC2030-SLV','ULCC21.5-SLV','ULCC203-SLV','ULCA203-SGR','ULCA2030-SLV','UCANCCR','UCFUUA-SGR','ULCAMN-SLV','ULAGE-SGR','ALUS19','SULCC2G202-SGR','AAL6APNS-SGR','AALNBSS-SGR','C6A-01-Purple-SH','C6A-02-Purple-SH','C6A-05-Purple-SH','C6A-10-Purple-SH','C6A-01-Black-SH','C6A-0.5-Yellow-SH','C6A-1.5-Yellow-SH','C6A-10-Yellow-SH','C6A-02-Orange-SH','C6A-05-Orange-SH','C6A-10-Red-SH','C6A-1.5-Pink-SH','C6A-05-Pink-SH','C6A-0.5-Orange-SH','C6-25-Green','C5-1.5-Green','C5-1.5-Pink','C6-0.3-Blue','C6-0.5-Blue','C6-01-Grey','C6-02-Red','C6-05-Red','C6-01-Pink','C6-02-Blue','C6-02-Grey','C6-02-Pink','C6-03-Grey','C6-05-Blue','C6-05-Grey','C6-05-Pink','C6-10-Red','C5-01-Grey','C6-0.3-Red','C6-0.5-Red','C5-02-Blue','C5-02-Grey','C6-01-Green','C6-1.5-Grey','C5-03-Grey','C6-02-Green','C6-02-White','C6-03-Green','C6-03-White','C6-05-Green','C6-05-White','C6-10-Blue','C6-15-Blue','C6-20-Blue','C6-25-Blue','C6-50-Blue','C5-01-Black','C5-0.5-Red','C5-01-White','C5-02-Black','C6-0.3-Grey','C6-0.3-Pink','C5-03-Black','C5-02-White','C6-01-Black','C6-01-Orange','C6-01-Yellow','C6-01-Red-CSV','C6-1.5-Blue','C5-03-White','C5-05-Black','C6-02-Yellow','C6-03-Black','C6-03-Purple','C6-03-Red-CSV','C6-05-Purple','C6-05-Yellow','C6-05-Red-CSV','C5-10-Blue','C5-10-Grey','C6-10-Green','C6-10-White','C5-20-Blue','C5-25-Blue','C5-0.5-Pink','C5-01-Orange','C5-01-Purple','C6-0.3-Black','C5-02-Yellow','C6-1.5-Green','C6-1.5-White','C5-03-Orange','C5-03-Purple','C5-05-Orange','C5-05-Purple','C5-10-Black','C5-10-White','C6-10-Red-CSV','C6-10-Yellow','C5-0.5-White','C6-0.5-Orange','C6-0.5-Purple','C6-0.5-Yellow','C6-1.5-Orange','C5-1.5-Blue','C6-20-Yellow','C6A-03-Yellow-SH','C6A-01-White-SH','C6-1.5-Red-CSV','C6A-02-Red-SH','C6A-03-Red-SH','C6A-02-White-SH','C6A-05-Green-SH','C6A-01-Yellow-SH','C6A-02-Yellow-SH','C6A-1.5-Black-SH','C6A-02-Black-SH','C6-20-White','C6-20-Green','C6-04-Blue','C6-2.5-Black','C6-20-Black','C6-04-Green','C6-15-Yellow','C6-2.5-White','C6-15-White','C6-04-Grey','C6-20-Grey','C5-0.3-Blue','C5-0.3-Pink','C6S-01GRY','C6S-1.5GRY','C6S-05GRY','C6S-01GRN','C6S-03GRN','C6S-02BLK','C6S-05BLU','C6A-0.3-Blue-SH','C6A-0.3-Black-SH','C6A-0.3-Green-SH','C6A-0.3-Orange-SH','C6A-0.3-Red-SH','C6A-0.3-White-SH','C6A-0.3-Yellow-SH','C6A-0.3-Purple-SH','C6S-0.30BLU','C6S-0.30GRN','C6S-0.50GRN','C6S-1.5GRN','C6S-02GRN','C6S-05GRN','C6S-0.30BLK','C6S-0.50BLK','C6S-01BLK','C6S-1.5BLK','C6S-05BLK','C6S-01YEL','C6S-02YEL','C6S-05YEL','C6S-0.30GRY','C6S-0.50GRY','C6S-02WHT','C6S-0.30RED','C6S-01RED','C6S-1.5RED','C6S-03RED','C6S-05RED','C6S-0.30ORN','C6S-01ORN','C6S-02ORN','C6S-03ORN','C6S-05ORN','C6S-02PUR','C6S-03PUR','C6S-05PUR','C6S-1.5PNK','C6S-02PNK','C6S-03PNK','C6S-05PNK','C6-7.5-Yellow','C6-7.5-Black','C6-7.5-White','C5-1.5-Red','C5-1.5-Yellow','C5-20-Grey','C6S-01BLUR','C6A-10-Blue-SH','C6A-03-Blue-SH','C6A-1.5-Grey-SH','C5-0.5-Orange','C6-1.5-Yellow','C5-0.5-Purple','C5-10-Yellow','C6-15-Black','C5-1.5-White','C6A-7.5-Grey-SH','LCST-05-OM3','LCLC-20-OM3','LCSC-05-OM1','SCSC-05-OM3','STST-20-OM1','SCSC-05-OM1','LCLC-01-OM3','LCSC-10-OM3','SCSC-01-OM1','LCST-10-OM3','SCST-01-OM1','LCLC-10-OM1','LCST-01-OM3','STST-01-OM1','STST-10-OM3','LCST-10-OM1','LCLC-15-OM3','LCLC-02-OM3','LCST-02-OM1','SCST-10-OM1','LCST-15-OM3','LCST-02-OM3','SCSC-15-OM3','SCSC-02-OM3','STST-02-OM3','STST-02-OM1','LCSC-20-OM3','LCLC-03-OM3','LCLC-03-OM1','LCST-15-OM1','LCSC-03-OM3','LCSC-03-OM1','LCST-03-OM3','LCST-03-OM1','SCST-15-OM1','SCSC-03-OM1','STST-03-OM3','LCLC-20-OM1','STST-03-OM1','LCLC-01-OM4','LCSC-05-OM3','LCST-20-OM1','LCSC-01-OM4','SCSC-0.5-OS2','LCLC-01-OS2','LCLC-03-OM4','SCSC-01-OS2','LCLC-10-OS2','SCSC-05-OM4','LCSC-0.5-OS2','SCST-0.5-OS2','STST-01-OM4','SCST-02-OS2','LCST-03-OM4','SCST-03-OS2','LCSC-03-OS2','LCLC-05-OM4','STST-03-OM4','SCST-05-OS2','LCSC-05-OS2','SCST-10-OS2','LCSC-05-OM4','LCSC-10-OS2','LCSC-15-OS2','LCST-05-OM4','LCSC-20-OS2','LCST-0.5-OS2','STST-01-OS2','LCST-01-OS2','LCLC-0.5-OM4','LCST-02-OS2','LCLC-10-OM4','LCLC-02-OS2','LCST-03-OS2','LCLC-15-OS2','LCST-05-OS2','LCST-10-OM4','LCLC-03-OS2','LCST-10-OS2','SCSC-10-OS2','STST-15-OS2','LCST-15-OS2','LCLC-0.5-OS2','LCSC-02-OS2','LCSC-30-OS2','LCLC-1.5-OM3','SCASC-0.5-OS2','SCASC-01-OS2','SCASC-02-OS2','LCALCA-01-OS2','SCALC-15-OS2','SCASCA-01-OS2','SCASC-15-OS2','SCASCA-02-OS2','SCASCA-03-OS2','SCASCA-05-OS2','SCASCA-10-OS2','STSC-05-OM3','STSC-10-OM3','LCALCA-02-OS2','STSC-20-OM3','LCALCA-05-OS2','LCALCA-15-OS2','USB3-03-AA','USB2-05-AA','USB2-05EXT-ACTV','USB2-10EXT-ACTV','USB2-01-AB','USB2-02-AB','U34BHBK','USB3-02-AB','U3-TC25-AC','USB2-01-MCAB','USB2-02-MCAB','UCDP-ADP','MU31CAF-SGR','MU31CA-01SLV','U30AS25','MU2CA-03BLK','MU31CA-01BLK','MU31CA-01SGR','MU28P-01SLV','U3-TCA02-MM','U3-TCB01-MM','ELUCHD-02RBLK','MU23T1-01SGR','MU2CC-01SLV','ELUCDP-01RBLK','U3-TCC01-MM','MP-UCVGCHG2','U28P3T1-01WH','U28P3T1-030WH','USB3GE-ADPDF','U2MCAB-05EPR1','ELUCVG-02RBLK','ELPCC202-BK','ELPCA201-WH','MU28P-03BLK','ELPCA201-BK','ELPCC202-WH','U22MCABRBK','MU31CC-EXT-01BLK','USB2-02-AM-AM','USB3-02-AM-AM','USB2-05-AM-AM','MU31CC-EXT-050BLK','USB3-03-MCAB','MU23T1-030SLV','USB3-03-AB','USB2-03-AB','USB2-02-MAB','VPMCA052SGR','MF-AUS3P2C13-02','MF-PEXT-03-BLK','MF-PEXT-15','MF-3PC13-01','MF-3PC13-02','MF-3PC13-03','MF-3PC13-05','MF-3PC19-02','MF-PEXT-02','MF-PEXT-03','MF-PEXT-05','MF-PEXT-10','MF-AUS3PC5-02','MF-C19C20-0.5','MF-C13C14-01','MF-3PC13RT-02','MF-3PC13-0.5','MF-AUS2PC7-02','MF-PEXT-02-BLK','MF-3PC13-10','MF-AUS2PC7-0.5','MF-AUS2PC7-01','MF-PEXT-05-BLK','MF-PEXT-10-BLK','MF-AUS3PC5-01','MF-AUS3PC13-02-MC','MF-AUS3PC5-03','MF-AUS2PC7-02RBLK','MF-AUS3PC5-02RBLK','MF-3PC13-02RBLK','MF-PEXT-02R','WC2A17MBK','WCCA17MBK','WCC18WH','WCCA30WH','CO-C18C8PWH','ULQC10W-SGR','ULQC10W-SLV','RA05BK','WCCA30WHCCWH','SULA8P01-SGR','SULA8P02-SGR','SULC8P02-SGR','UP2QC10A-SGR','UP2QC10AWM-SGR','WC1X20-EU','WCG2X32-ANZ','WCG2X32-EU','MDP-HDMI-AIC','MM-AD-02','MM-AD-03','C5-03-Green','MM-AD-10','MM-AD-15','MM-AD-20','TV-MM-02','TV-MM-10','MF-3PC19-05','HDMI-DVI-15MF','DVI-HDMI-02-MM','AD-EXT-03','DVI-HDMI-03-MM','DVI-HDMI-05-MM','HDMI-DVI-MF','AD-EXT-10','AD-EXT-20','VGA-DVI-MF','DVI-DL-02-MM','MF-C14AUS3P-1.5','DP-VGA-02-MM','DP-DVI-ADP','DP-HDMI-02-MM','MDP-HDMI-02-MM','MF-AUS3P2C13-03','3.5M-2RCAF-ADP','MF-C19C20-03','MF-C19C20-01','DP-VGA-ADP','DVI-DL-05-MF','VGA-MF-05','VGA-MFF-SPL','VGA-MF-10','DP-DVI-02-MM','DP-DVI-03-MM','MF-C13C14-05','DP-DVI-01-MM','VGA-MM-02-APS','HDMI-01-MM-V4','AD-EXT-02','VGA-MM-7.5-APS','MDP-DVI-ACTV','MDP-DVI-AIC','MDP-VGA-AIC','MF-3PC15-02','MFF-C14C13-02','MF-3PC13RT-03','MF-3PC15-03','DVI-DL-02B-MM','DVI-DL-03B-MM','MDP-VGA-02-MM','MDP-DP-ADP','MF-C13C14-10','CMNHD-VGA-AD-ADP','MF-C13C14-01-WH','MF-C13C14-0.5-BLU','MF-C13C14-03-BLU','DVID-VGA-ADP','HDMI-MINI-ADPCL','TL-AD-10','MDP-DP-03-MM','MDP-DVI-01-MM','AD-SPL-01','MDP-VGBK-ADP','MDP-DPDVIHD-ADP','MDP-VGDVHD-ADP','MF-C14C15-02-BLU','MF-C14C15-1.5-BLU','HDC-DVI-02-MM','MF-AUS3PC13-03-MC','HDD-DVI-03-MM','MF-3PC15-05','DP-DPDVIHD-ADP','HDMI-7.5-MM-V4','DVI-HD02-MMCO','HDAS-V202CO','HDAS-V201','HDAS-V203CO','HDAS-V202','MDP-HDBK-ACO','MDP-DVBK-ACO','MDP-HD4K-02-ACTV','PAR-FTR-03','ELDP-01','ELMDPHD-01','ELMDPDP-02','HDDPU-ACTV','MDP-VGHD4K-ADP','VPB4P4A18','U3-TCB02-MM','U2-TCMNB02-MM','HDMCR','RJ1215R','DP2DP-ADP','RJ1202R','HDVG-MM-01','HDVG-MM-03','AE2RBK','AE5RBK','ACM5RBK','ULDPHD02-SGR','ULMDPHD02-SGR','ULDPHDA-SGR','ULMDPHDA-SGR','ULDP01-SGR','ULDP02-SGR','ULMDPDP02-SGR','ULMDPDP03-SGR','ULHD02-SGR','SSULHD02-SGR','WCG2X32-UK','MF-AUS3P2C13-04','PHD-03-MM-V2','MF-3PC19-03','MF-C14C15-0.5','MF-C14C15-03','MF-C19C20-01-BLU','MF-C19C20-02-BLU','MF-C13C14-0.5','MF-C13C14-1.5','TB-03-MM-BK','VC2A24','ASM34','HD4KSW05','MF-3PC19-01','MF-C13C14-03-RD','MF-C19C20-1.5-RD','MF-C13C14-02-RD','MF-C19C20-02-RD','MF-C19C20-01-RD','WC1X20-ANZ','ELPCAA-BK','FUCHD2-SGR','FUSCC2-SGR','ELPC8P01-BK','ELPA8P01-BK','ELPA8P02-BK','ELPC8P02-BK','ULDUNIV2-SGR','FUDP2-SGR','FUMDPDP2-SGR','UCFUPRGEV2-SGR','ELP8P35A-WH','EL2HDVGA-ADP','EL2HDDVI-01','EL2HD-03','ELPRACC01-BK','ELPRACC02-BK','ELPCCRA0.25-BK','EL2DP-01','EL2DP-02','EL2DP-03','ULCHDACPD-SGR','WCG1X30SGR-ANZ','27F34KCPD','EL2DPDVI-ADP','EL2DPHD-01','EL2DPDVI-03','EL2DPHD-ADP','EL2UCHD-ADP','EL2UCHD-01','EL2DPVGA-01','ELCCA3212-BK','WCG1X20SGR-ANZ','MSPB5K','MSCCM','MSCDD','MAGAWC','MSCDDWH','MSCDDAWC','MSCDDAWCWH','EL3IN11WH-1-1-1-1-1','EL3IN11WH-1-1-1-1-1-1','EPLSWCBK'];
	}
	public function getMD15Skus(){
		return [];
	}
	public function searchForId($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['stock_name'] === $id) {
				return $val['qty'];
			}
		}
		return null;
	}
	public function getModel($model) {
		return $this->_objectManager->create($model);
	}

	public function isActiveModule($module) {
		if ($this->_moduleManager->isOutputEnabled($module) && $this->_moduleManager->isEnabled($module)) {
			return true;
		}
		return false;
	}

	/**
	 * Retrieve current url in base64 encoding
	 *
	 * @return string
	 */
	public function getCurrentBase64Url() {
		return strtr(base64_encode($this->_url->getCurrentUrl()), '+/=', '-_,');
	}

	/**
	 * base64_decode() for URLs decoding
	 *
	 * @param    string $url
	 * @return   string
	 */
	public function decode($url) {
		$url = base64_decode(strtr($url, '-_,', '+/='));
		return $this->_url->sessionUrlVar($url);
	}

	/**
	 * Returns customer id from session
	 *
	 * @return int|null
	 */
	public function getCustomerId() {
		$customerInSession = $this->_objectManager->create('Magento\Customer\Model\Session');
		return $customerInSession->getCustomerId();
	}

	/* Get current customer */
	public function getCustomer() {
		if (!$this->_customer) {
			$this->_customer = $this->getModel('Magento\Customer\Model\Customer')->load($this->getCustomerId());
		}
		return $this->_customer;
	}

	public function getStore() {
		return $this->_storeManager->getStore();
	}

	/* Get system store config */
	public function getStoreConfig($node, $storeId = NULL) {
		if ($storeId != NULL) {
			return $this->scopeConfig->getValue($node, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
		}
		return $this->scopeConfig->getValue($node, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $this->getStore()->getId());
	}

	// Check to accept to use builder panel
	public function acceptToUsePanel() {
		if ($this->_acceptToUsePanel) {
			return true;
		} else {
			if ($this->showButton() && ($this->customerSession->getUsePanel() == 1)) {
				$this->_acceptToUsePanel = true;
				return true;
			}
			$this->_acceptToUsePanel = false;
			return false;
		}

	}

	/* Check to visible panel button */
	public function showButton() {

		if ($this->getStoreConfig('mpanel/general/is_enabled')) {
			$customer = $this->getCustomer();
			if ($customer->getIsBuilderAccount() == 1) {
				return true;
			}
			return false;
		}

		return false;
	}

	/* Get all settings of the theme */
	public function getThemeSettings() {
		return [
			'catalog' =>
			[
				'per_row' => $this->getStoreConfig('mpanel/catalog/product_per_row'),
				'featured' => $this->getStoreConfig('mpanel/catalog/featured'),
				'hot' => $this->getStoreConfig('mpanel/catalog/hot'),
				'ratio' => $this->getStoreConfig('mpanel/catalog/picture_ratio'),
				'new_label' => $this->getStoreConfig('mpanel/catalog/new_label'),
				'sale_label' => $this->getStoreConfig('mpanel/catalog/sale_label'),
				'preload' => $this->getStoreConfig('mpanel/catalog/preload'),
				'ajaxscroll' => $this->getStoreConfig('mpanel/catalog/ajaxscroll'),
				'wishlist_button' => $this->getStoreConfig('mpanel/catalog/wishlist_button'),
				'compare_button' => $this->getStoreConfig('mpanel/catalog/compare_button'),
				'sub_categories' => $this->getStoreConfig('mpanel/catalog/sub_categories'),
			],
			'catalogsearch' =>
			[
				'per_row' => $this->getStoreConfig('mpanel/catalogsearch/product_per_row'),
			],
			'catalog_brand' =>
			[
				'per_row' => $this->getStoreConfig('brand/list_page_settings/product_per_row'),
			],
			'product_details' =>
			[
				'sku' => $this->getStoreConfig('mpanel/product_details/sku'),
				'reviews_summary' => $this->getStoreConfig('mpanel/product_details/reviews_summary'),
				'wishlist' => $this->getStoreConfig('mpanel/product_details/wishlist'),
				'compare' => $this->getStoreConfig('mpanel/product_details/compare'),
				'preload' => $this->getStoreConfig('mpanel/product_details/preload'),
				'short_description' => $this->getStoreConfig('mpanel/product_details/short_description'),
				'upsell_products' => $this->getStoreConfig('mpanel/product_details/upsell_products'),
			],
			'product_tabs' =>
			[
				'show_description' => $this->getStoreConfig('mpanel/product_tabs/show_description'),
				'show_additional' => $this->getStoreConfig('mpanel/product_tabs/show_additional'),
				'show_reviews' => $this->getStoreConfig('mpanel/product_tabs/show_reviews'),
				'show_product_tag_list' => $this->getStoreConfig('mpanel/product_tabs/show_product_tag_list'),
			],
			'contact_google_map' =>
			[
				'display_google_map' => $this->getStoreConfig('mpanel/contact_google_map/display_google_map'),
				'api_key' => $this->getStoreConfig('mpanel/contact_google_map/api_key'),
				'address_google_map' => $this->getStoreConfig('mpanel/contact_google_map/address_google_map'),
				'html_google_map' => $this->getStoreConfig('mpanel/contact_google_map/html_google_map'),
				'pin_google_map' => $this->getStoreConfig('mpanel/contact_google_map/pin_google_map'),
			],
			'banner_slider' =>
			[
				'slider_tyle' => $this->getStoreConfig('mgstheme/banner_slider/slider_tyle'),
				'id_reslider' => $this->getStoreConfig('mgstheme/banner_slider/id_reslider'),
				'identifier_block' => $this->getStoreConfig('mgstheme/banner_slider/identifier_block'),
				'banner_owl_auto' => $this->getStoreConfig('mgstheme/banner_slider/banner_owl_auto'),
				'banner_owl_speed' => $this->getStoreConfig('mgstheme/banner_slider/banner_owl_speed'),
				'banner_owl_loop' => $this->getStoreConfig('mgstheme/banner_slider/banner_owl_loop'),
				'banner_owl_nav' => $this->getStoreConfig('mgstheme/banner_slider/banner_owl_nav'),
				'banner_owl_dot' => $this->getStoreConfig('mgstheme/banner_slider/banner_owl_dot'),
			],
		];
	}

	public function getEnableChangeProductPerRow() {
		return $this->getStoreConfig('mpanel/catalog/change_product_per_row');
	}

	/* Get col for responsive */
	public function getColClass($perrow = NULL) {
		if (!$perrow) {
			$settings = $this->getThemeSettings();
			$perrow = $settings['catalog']['per_row'];

			if ($this->_request->getFullActionName() == 'catalog_category_view') {
				$category = $this->getCurrentCategory();
				$categoryPerrow = $category->getPerRow();
				if ($categoryPerrow != '') {
					$perrow = $categoryPerrow;
				}
			}

			if ($this->_request->getFullActionName() == 'catalogsearch_result_index') {
				$perrow = $settings['catalogsearch']['per_row'];
			}

		}

		switch ($perrow) {
		case 2:
			return 'col-lg-6 col-md-6 col-sm-6 col-xs-6';
			break;
		case 3:
			return 'col-lg-4 col-md-4 col-sm-4 col-xs-6';
			break;
		case 4:
			return 'col-lg-3 col-md-3 col-sm-6 col-xs-6';
			break;
		case 5:
			return 'col-lg-custom-5 col-md-custom-5 col-sm-6 col-xs-6';
			break;
		case 6:
			return 'col-lg-2 col-md-2 col-sm-3 col-xs-6';
			break;
		case 7:
			return 'col-lg-custom-7 col-md-custom-7 col-sm-6 col-xs-6';
			break;
		case 8:
			return 'col-lg-custom-8 col-md-custom-8 col-sm-6 col-xs-6';
			break;
		}
		return;
	}
	/* Get product image size */
	public function getImageSize($ratio = NULL) {
		if (!$ratio) {
			$ratio = $this->getStoreConfig('mpanel/catalog/picture_ratio');
			if ($this->_request->getFullActionName() == 'catalog_category_view') {
				$category = $this->getCurrentCategory();
				$categoryRatio = $category->getPictureRatio();
				if ($categoryRatio != '') {
					$ratio = $categoryRatio;
				}
			}
		}

		$maxWidth = $this->getStoreConfig('mpanel/catalog/max_width_image');
		$result = [];
		switch ($ratio) {
		// 1/1 Square
		case 1:
			$result = array('width' => round($maxWidth), 'height' => round($maxWidth));
			break;
		// 1/2 Portrait
		case 2:
			$result = array('width' => round($maxWidth), 'height' => round($maxWidth * 2));
			break;
		// 2/3 Portrait
		case 3:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 1.5)));
			break;
		// 3/4 Portrait
		case 4:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 4) / 3));
			break;
		// 2/1 Landscape
		case 5:
			$result = array('width' => round($maxWidth), 'height' => round($maxWidth / 2));
			break;
		// 3/2 Landscape
		case 6:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 2) / 3));
			break;
		// 4/3 Landscape
		case 7:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 3) / 4));
			break;
		}

		return $result;
	}

	/* Get product image padding */
	public function getImagePadding($ratio = NULL) {
		if (!$ratio) {
			$ratio = $this->getStoreConfig('mpanel/catalog/picture_ratio');
			if ($this->_request->getFullActionName() == 'catalog_category_view') {
				$category = $this->getCurrentCategory();
				$categoryRatio = $category->getPictureRatio();
				if ($categoryRatio != '') {
					$ratio = $categoryRatio;
				}
			}
		}
		$result = "";
		switch ($ratio) {
		// 1/1 Square
		case 1:
			$result = 100;
			break;
		// 1/2 Portrait
		case 2:
			$result = 200;
			break;
		// 2/3 Portrait
		case 3:
			$result = 150;
			break;
		// 3/4 Portrait
		case 4:
			$value = (400 / 3);
			$result = round($value, 4);
			break;
		// 2/1 Landscape
		case 5:
			$result = 50;
			break;
		// 3/2 Landscape
		case 6:
			$value = (200 / 3);
			$result = round($value, 4);
			break;
		// 4/3 Landscape
		case 7:
			$result = 75;
			break;
		}

		$result .= "%";

		return $result;
	}

	/* Get product image size for product details page*/
	public function getImageSizeForDetails() {
		$ratio = $this->getStoreConfig('mpanel/catalog/picture_ratio');
		$maxWidth = $this->getStoreConfig('mpanel/catalog/max_width_image_detail');
		$result = [];
		switch ($ratio) {
		// 1/1 Square
		case 1:
			$result = array('width' => round($maxWidth), 'height' => round($maxWidth));
			break;
		// 1/2 Portrait
		case 2:
			$result = array('width' => round($maxWidth), 'height' => round($maxWidth * 2));
			break;
		// 2/3 Portrait
		case 3:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 1.5)));
			break;
		// 3/4 Portrait
		case 4:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 4) / 3));
			break;
		// 2/1 Landscape
		case 5:
			$result = array('width' => round($maxWidth), 'height' => round($maxWidth / 2));
			break;
		// 3/2 Landscape
		case 6:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 2) / 3));
			break;
		// 4/3 Landscape
		case 7:
			$result = array('width' => round($maxWidth), 'height' => round(($maxWidth * 3) / 4));
			break;
		}

		return $result;
	}

	public function getImageMinSize($ratio = NULL) {
		if (!$ratio) {
			$ratio = $this->getStoreConfig('mpanel/catalog/picture_ratio');
		}
		$result = [];
		switch ($ratio) {
		// 1/1 Square
		case 1:
			$result = array('width' => 100, 'height' => 100);
			break;
		// 1/2 Portrait
		case 2:
			$result = array('width' => 100, 'height' => 200);
			break;
		// 2/3 Portrait
		case 3:
			$result = array('width' => 100, 'height' => 150);
			break;
		// 3/4 Portrait
		case 4:
			$result = array('width' => 120, 'height' => 160);
			break;
		// 2/1 Landscape
		case 5:
			$result = array('width' => 100, 'height' => 50);
			break;
		// 3/2 Landscape
		case 6:
			$result = array('width' => 120, 'height' => 80);
			break;
		// 4/3 Landscape
		case 7:
			$result = array('width' => 120, 'height' => 90);
			break;
		}

		return $result;
	}

	public function getCurrentDateTime() {
		$now = $this->_date->gmtDate();
		return $now;
	}

	public function getProductLabel($product) {
		return;
		$html = '';
		$newLabel = $this->getStoreConfig('mpanel/catalog/new_label');
		$saleLabel = $this->getStoreConfig('mpanel/catalog/sale_label');
		$salable = 1;
		if ($product->getTypeId() == 'simple') {
			$salable = $this->getSalableQty($product);
		}

		$soldLabel = __('Out of Stock');
		// Out of stock label
		if ($salable <= 0 || !$product->isAvailable()) {
			$html .= '<span class="product-label sold-out-label"><span>' . $soldLabel . '</span></span>';
		} else {
			// New label
			$numberLabel = 0;
			$now = $this->getCurrentDateTime();
			$dateTimeFormat = \Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT;
			$newFromDate = $product->getNewsFromDate();
			if ($newFromDate) {
				$newFromDate = date($dateTimeFormat, strtotime($newFromDate));
			}
			$newToDate = $product->getNewsToDate();
			if ($newToDate) {
				$newToDate = date($dateTimeFormat, strtotime($newToDate));
			}
			if ($newLabel != '') {
				if (!(empty($newToDate))) {
					if (!(empty($newFromDate)) && ($newFromDate < $now) && ($newToDate > $now)) {
						$html .= '<span class="product-label new-label"><span>' . $newLabel . '</span></span>';
						$numberLabel = 1;
					}
				}
			}

			// Sale label
			$price = $product->getOrigData('price');
			$finalPrice = $product->getFinalPrice();
			$fiPrice = $product->getPriceInfo()->getPrice('final_price')->getValue();
			if ($this->getStoreConfig('mpanel/catalog/sale_label_discount') == 1) {
				if (($finalPrice < $price)) {
					$save = $price - $finalPrice;
					$percent = round(($save * 100) / $price);
					if ($numberLabel == 1) {
						$html .= '<span class="product-label sale-label multiple-label"><span>-' . $percent . '%</span></span>';
					} else {
						$html .= '<span class="product-label sale-label"><span>-' . $percent . '%</span></span>';
					}
				}
			} else {
				if ($saleLabel != '') {
					if (($finalPrice < $price) || ($fiPrice < $price)) {
						if ($numberLabel == 1) {
							$html .= '<span class="product-label sale-label multiple-label"><span>' . $saleLabel . '</span></span>';
						} else {
							$html .= '<span class="product-label sale-label"><span>' . $saleLabel . '</span></span>';
						}
					}
				}
			}
		}
		return $html;
	}

	public function getUrlBuilder() {
		return $this->_url;
	}

	public function getCssUrl() {
		return $this->_url->getUrl('mpanel/index/css', ['store' => $this->getStore()->getId()]);
	}

	public function getPanelCssUrl() {
		return $this->_url->getUrl('mpanel/index/panelstyle');
	}

	public function getFonts() {
		return [
			['css-name' => 'Lato', 'font-name' => __('Lato')],
			['css-name' => 'Open+Sans', 'font-name' => __('Open Sans')],
			['css-name' => 'Roboto', 'font-name' => __('Roboto')],
			['css-name' => 'Roboto Slab', 'font-name' => __('Roboto Slab')],
			['css-name' => 'Oswald', 'font-name' => __('Oswald')],
			['css-name' => 'Source+Sans+Pro', 'font-name' => __('Source Sans Pro')],
			['css-name' => 'PT+Sans', 'font-name' => __('PT Sans')],
			['css-name' => 'PT+Serif', 'font-name' => __('PT Serif')],
			['css-name' => 'Droid+Serif', 'font-name' => __('Droid Serif')],
			['css-name' => 'Josefin+Slab', 'font-name' => __('Josefin Slab')],
			['css-name' => 'Montserrat', 'font-name' => __('Montserrat')],
			['css-name' => 'Ubuntu', 'font-name' => __('Ubuntu')],
			['css-name' => 'Titillium+Web', 'font-name' => __('Titillium Web')],
			['css-name' => 'Noto+Sans', 'font-name' => __('Noto Sans')],
			['css-name' => 'Lora', 'font-name' => __('Lora')],
			['css-name' => 'Playfair+Display', 'font-name' => __('Playfair Display')],
			['css-name' => 'Bree+Serif', 'font-name' => __('Bree Serif')],
			['css-name' => 'Vollkorn', 'font-name' => __('Vollkorn')],
			['css-name' => 'Alegreya', 'font-name' => __('Alegreya')],
			['css-name' => 'Noto+Serif', 'font-name' => __('Noto Serif')],
			['css-name' => 'Libre+Baskerville', 'font-name' => __('Libre Baskerville')],
			['css-name' => 'Poppins', 'font-name' => __('Poppins')],
		];
	}

	public function getLinksFont() {
		$setting = [
			'default_font' => $this->getStoreConfig('mgstheme/fonts/default_font'),
			'h1' => $this->getStoreConfig('mgstheme/fonts/h1'),
			'h2' => $this->getStoreConfig('mgstheme/fonts/h2'),
			'h3' => $this->getStoreConfig('mgstheme/fonts/h3'),
			'h4' => $this->getStoreConfig('mgstheme/fonts/h4'),
			'h5' => $this->getStoreConfig('mgstheme/fonts/h5'),
			'h6' => $this->getStoreConfig('mgstheme/fonts/h6'),
			'price' => $this->getStoreConfig('mgstheme/fonts/price'),
			'menu' => $this->getStoreConfig('mgstheme/fonts/menu'),
			'btn' => $this->getStoreConfig('mgstheme/fonts/menu'),
			'custom_font_fml' => $this->getStoreConfig('mgstheme/fonts/custom_font_fml'),
		];
		$fonts = [];
		$fonts[] = $setting['default_font'];

		if (!in_array($setting['h1'], $fonts)) {
			$fonts[] = $setting['h1'];
		}

		if (!in_array($setting['h2'], $fonts)) {
			$fonts[] = $setting['h2'];
		}

		if (!in_array($setting['h3'], $fonts)) {
			$fonts[] = $setting['h3'];
		}

		if (!in_array($setting['price'], $fonts)) {
			$fonts[] = $setting['price'];
		}

		if (!in_array($setting['menu'], $fonts)) {
			$fonts[] = $setting['menu'];
		}

		if (!in_array($setting['btn'], $fonts)) {
			$fonts[] = $setting['btn'];
		}

		if (!in_array($setting['custom_font_fml'], $fonts)) {
			$fonts[] = $setting['custom_font_fml'];
		}

		$fonts = array_filter($fonts);
		$links = '';

		foreach ($fonts as $_font) {
			$links .= "@import url('//fonts.googleapis.com/css?family=" . $_font . ":300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,900,900italic');";
		}

		return $links;
	}

	// get theme color
	public function getThemecolorSetting($storeId, $themeName) {
		$setting = [];
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath('code/MGS/Mpanel/data/themes/' . $themeName);
		$themeStyleFile = $dir . '/theme_style.xml';
		if (is_readable($themeStyleFile)) {
			$parsedArray = $this->_parser->load($themeStyleFile)->xmlToArray();
			if (isset($parsedArray['class_setting']['theme_color'])) {
				foreach ($parsedArray['class_setting']['theme_color'] as $classAttribute => $classString) {
					$classAttribute = str_replace('_', '-', $classAttribute);
					$setting[$classString] = [$classAttribute => $this->getStoreConfig('color/general/theme_color', $storeId)];
				}
			}
		}
		$setting = array_filter($setting);
		return $setting;
	}

	// get header custom color
	public function getHeaderColorSetting($storeId, $themeName) {

		$setting = $arrAttribute = [];
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath('code/MGS/Mpanel/data/themes/' . $themeName);
		$themeStyleFile = $dir . '/theme_style.xml';
		if (is_readable($themeStyleFile)) {
			$parsedArray = $this->_parser->load($themeStyleFile)->xmlToArray();
			if (isset($parsedArray['class_setting']['header'])) {
				foreach ($parsedArray['class_setting']['header'] as $configNote => $classAttribute) {
					foreach ($classAttribute as $attribute => $classString) {
						$attribute = str_replace('_', '-', $attribute);
						$arrAttribute[$classString] = [
							$attribute => $this->getStoreConfig('color/header/' . $configNote, $storeId),
						];
						$setting = array_merge_recursive($setting, $arrAttribute);
						$arrAttribute = [];
					}
				}
			}
		}

		$setting = array_filter($setting);
		return $setting;
	}

	// get main content custom color
	public function getMainColorSetting($storeId, $themeName) {
		$setting = [
			/* Text & Link color */
			'body' => [
				'color' => $this->getStoreConfig('color/main/text_color', $storeId),
			],
			'a' => [
				'color' => $this->getStoreConfig('color/main/link_color', $storeId),
			],
			'a:hover,a:focus' => [
				'color' => $this->getStoreConfig('color/main/link_color_hover', $storeId),
			],
			'.product-tab .menu-product-tabs li:hover a, .product-tab .menu-product-tabs li a:focus' => [
				'border-color' => $this->getStoreConfig('color/main/link_color_hover', $storeId),
			],
			'.price-box .price' => [
				'color' => $this->getStoreConfig('color/main/price_color', $storeId),
			],
			'.price-box .old-price .price' => [
				'color' => $this->getStoreConfig('color/main/price_old_color', $storeId),
			],
			'.price-box .special-price .price' => [
				'color' => $this->getStoreConfig('color/main/price_special_color', $storeId),
			],
			/* Default button color */
			'.btn-default' => [
				'color' => $this->getStoreConfig('color/main/button_text', $storeId),
				'background-color' => $this->getStoreConfig('color/main/button_background', $storeId),
				'border-color' => $this->getStoreConfig('color/main/button_border', $storeId),
			],
			'.btn-default:hover,.btn-default:focus,.btn-default:active' => [
				'color' => $this->getStoreConfig('color/main/button_text_hover', $storeId),
				'background-color' => $this->getStoreConfig('color/main/button_background_hover', $storeId),
				'border-color' => $this->getStoreConfig('color/main/button_border_hover', $storeId),
			],
			/* Default button 2 color */
			'.btn-default2' => [
				'color' => $this->getStoreConfig('color/main/button_text_df', $storeId),
				'background-color' => $this->getStoreConfig('color/main/button_background_df', $storeId),
				'border-color' => $this->getStoreConfig('color/main/button_border_df', $storeId),
			],
			'.btn-default2:hover,.btn-default2:focus,.btn-default2:active' => [
				'color' => $this->getStoreConfig('color/main/button_text_hover_df', $storeId),
				'background-color' => $this->getStoreConfig('color/main/button_background_hover_df', $storeId),
				'border-color' => $this->getStoreConfig('color/main/button_border_hover_df', $storeId),
			],
			/* Primary button color */
			'.btn-primary, .tocart.btn-cart' => [
				'color' => $this->getStoreConfig('color/main/primary_button_text', $storeId),
				'background-color' => $this->getStoreConfig('color/main/primary_button_background', $storeId),
				'border-color' => $this->getStoreConfig('color/main/primary_button_border', $storeId),
			],
			'.btn-primary:hover,.btn-primary:focus,.btn-primary:active,.tocart.btn-cart:hover,.tocart.btn-cart:focus,.tocart.btn-cart:active' => [
				'color' => $this->getStoreConfig('color/main/primary_button_text_hover', $storeId),
				'background-color' => $this->getStoreConfig('color/main/primary_button_background_hover', $storeId),
				'border-color' => $this->getStoreConfig('color/main/primary_button_border_hover', $storeId),
			],
			/* Secondary button color */
			'.btn-secondary' => [
				'color' => $this->getStoreConfig('color/main/secondary_button_text', $storeId),
				'background-color' => $this->getStoreConfig('color/main/secondary_button_background', $storeId),
				'border-color' => $this->getStoreConfig('color/main/secondary_button_border', $storeId),
			],
			'.btn-secondary:hover,.btn-secondary:focus,.btn-secondary:active' => [
				'color' => $this->getStoreConfig('color/main/secondary_button_text_hover', $storeId),
				'background-color' => $this->getStoreConfig('color/main/secondary_button_background_hover', $storeId),
				'border-color' => $this->getStoreConfig('color/main/secondary_button_border_hover', $storeId),
			],
		];
		$setting = array_filter($setting);
		return $setting;
	}

	// get main content custom color
	public function getFooterColorSetting($storeId, $themeName) {
		$setting = $arrAttribute = [];
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath('code/MGS/Mpanel/data/themes/' . $themeName);
		$themeStyleFile = $dir . '/theme_style.xml';
		if (is_readable($themeStyleFile)) {
			$parsedArray = $this->_parser->load($themeStyleFile)->xmlToArray();
			if (isset($parsedArray['class_setting']['footer'])) {
				foreach ($parsedArray['class_setting']['footer'] as $configNote => $classAttribute) {
					foreach ($classAttribute as $attribute => $classString) {
						$attribute = str_replace('_', '-', $attribute);
						$arrAttribute[$classString] = [
							$attribute => $this->getStoreConfig('color/footer/' . $configNote, $storeId),
						];
						$setting = array_merge_recursive($setting, $arrAttribute);
						$arrAttribute = [];
					}
				}
			}
		}

		$setting = array_filter($setting);
		return $setting;
	}

	/* Get css content of panel */
	public function getPanelStyle() {
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath('code/MGS/Mpanel/view/frontend/web/css/panel.css');
		$content = file_get_contents($dir);
		return $content;
	}

	/* Check store view has use homepage builder or not */
	public function useBuilder() {
		if ($this->_useBuilder) {
			return true;
		} else {
			$storePanelCollection = $this->getModel('MGS\Mpanel\Model\Store')
				->getCollection()
				->addFieldToFilter('store_id', $this->getStore()->getId())
				->addFieldToFilter('status', 1);
			if (count($storePanelCollection) > 0) {
				$this->_useBuilder = true;
				return true;
			}
			$this->_useBuilder = false;
			return false;
		}

	}

	/* Check current page is homepage or not */
	public function isHomepage() {
		if ($this->_request->getFullActionName() == 'cms_index_index') {
			return true;
		}
		return false;
	}

	/* Check current page is homepage or not */
	public function isCmsPage() {
		if ($this->_request->getFullActionName() == 'cms_page_view') {
			return true;
		}
		return false;
	}

	/* Get Animation Effect */
	public function getAnimationEffect() {
		return [
			'bounce' => 'Bounce',
			'flash' => 'Flash',
			'pulse' => 'Pulse',
			'rubberBand' => 'Rubber Band',
			'shake' => 'Shake',
			'swing' => 'Swing',
			'tada' => 'Tada',
			'wobble' => 'Wobble',
			'bounceIn' => 'Bounce In',
			'fadeIn' => 'Fade In',
			'fadeInDown' => 'Fade In Down',
			'fadeInDownBig' => 'Fade In Down Big',
			'fadeInLeft' => 'Fade In Left',
			'fadeInLeftBig' => 'Fade In Left Big',
			'fadeInRight' => 'Fade In Right',
			'fadeInRightBig' => 'Fade In Right Big',
			'fadeInUp' => 'Fade In Up',
			'fadeInUpBig' => 'Fade In Up Big',
			'flip' => 'Flip',
			'flipInX' => 'Flip In X',
			'flipInY' => 'Flip In Y',
			'lightSpeedIn' => 'Light Speed In',
			'rotateIn' => 'Rotate In',
			'rotateInDownLeft' => 'Rotate In Down Left',
			'rotateInDownRight' => 'Rotate In Down Right',
			'rotateInUpLeft' => 'Rotate In Up Left',
			'rotateInUpRight' => 'Rotate In Up Right',
			'rollIn' => 'Roll In',
			'zoomIn' => 'Zoom In',
			'zoomInDown' => 'Zoom In Down',
			'zoomInLeft' => 'Zoom In Left',
			'zoomInRight' => 'Zoom In Right',
			'zoomInUp' => 'Zoom In Up',
		];
	}

	public function getViewFileUrl($fileId, array $params = []) {
		try {
			$params = array_merge(['_secure' => $this->_request->isSecure()], $params);
			return $this->_assetRepo->getUrlWithParams($fileId, $params);
		} catch (\Magento\Framework\Exception\LocalizedException $e) {
			$this->_logger->critical($e);
			return $this->_getNotFoundUrl();
		}
	}

	public function getColorAccept($type, $color = NULL) {
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath('code/MGS/Mpanel/view/frontend/web/images/panel/colour/');
		$html = '';
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				$html .= '<ul>';

				while ($files[] = readdir($dh));
				sort($files);
				foreach ($files as $file) {
					$file_parts = pathinfo($dir . $file);
					if (isset($file_parts['extension']) && $file_parts['extension'] == 'png') {
						$colour = str_replace('.png', '', $file);
						$wrapper = str_replace('_', '-', $type);
						$_color = explode('.', $colour);
						$colour = $wrapper . '-' . strtolower(end($_color));
						$html .= '<li>';
						$html .= '<a href="#" onclick="changeInputColor(\'' . $colour . '\', \'' . $type . '\', this, \'' . $wrapper . '-content\'); return false"';
						if ($color != NULL && $color == $colour) {
							$html .= ' class="active"';
						}
						$html .= '>';
						$html .= '<img src="' . $this->getViewFileUrl('MGS_Mpanel::images/panel/colour/' . $file) . '" alt=""/>';
						$html .= '</a>';
						$html .= '</li>';
					}
				}
				$html .= '</ul>';
			}
		}
		return $html;
	}

	public function convertPerRowtoCol($perRow) {
		switch ($perRow) {
		case 1:
			$result = 12;
			break;
		case 2:
			$result = 6;
			break;
		case 3:
			$result = 4;
			break;
		case 4:
			$result = 3;
			break;
		case 5:
			$result = 'custom-5';
			break;
		case 6:
			$result = 2;
			break;
		case 7:
			$result = 'custom-7';
			break;
		case 8:
			$result = 'custom-8';
			break;
		}

		return $result;
	}

	public function convertColClass($col, $type) {
		if (($type == 'row') && ($col == 'custom-5' || $col == 'custom-7' || $col == 'custom-8')) {
			return 'row-' . $col;
		}
		if ($type == 'col') {
			if (($col == 'custom-5' || $col == 'custom-7' || $col == 'custom-8')) {
				return 'col-md-' . $col . ' col-sm-3 col-xs-6';
			} else {
				$class = 'col-lg-' . $col . ' col-md-' . $col;
				if ($col == 12) {
					$class .= ' col-sm-12 col-xs-12';
				}
				if ($col == 6) {
					$class .= ' col-sm-6 col-xs-6';
				}
				if (($col == 4) || ($col == 3)) {
					$class .= ' col-sm-4 col-xs-6';
				}
				if ($col == 2) {
					$class .= ' col-sm-3 col-xs-6';
				}

				return $class;
			}
		}
	}

	/* Get class clear left */
	public function getClearClass($perrow = NULL, $nb_item) {
		if (!$perrow) {
			$settings = $this->getThemeSettings();
			$perrow = $settings['catalog']['per_row'];
		}
		$clearClass = '';
		switch ($perrow) {
		case 2:
			if ($nb_item % 2 == 1) {
				$clearClass .= " first-row-item first-sm-item first-xs-item";
			}
			return $clearClass;
			break;
		case 3:
			if ($nb_item % 3 == 1) {
				$clearClass .= " first-row-item first-sm-item";
			}
			if ($nb_item % 2 == 1) {
				$clearClass .= " first-xs-item";
			}
			return $clearClass;
			break;
		case 4:
			if ($nb_item % 4 == 1) {
				$clearClass .= " first-row-item";
			}
			if ($nb_item % 3 == 1) {
				$clearClass .= " first-sm-item";
			}
			if ($nb_item % 2 == 1) {
				$clearClass .= " first-xs-item";
			}
			return $clearClass;
			break;
		case 5:
			if ($nb_item % 5 == 1) {
				$clearClass .= " first-row-item";
			}
			if ($nb_item % 3 == 1) {
				$clearClass .= " first-sm-item";
			}
			if ($nb_item % 2 == 1) {
				$clearClass .= " first-xs-item";
			}
			return $clearClass;
			break;
		case 6:
			if ($nb_item % 6 == 1) {
				$clearClass .= " first-row-item";
			}
			if ($nb_item % 3 == 1) {
				$clearClass .= " first-sm-item";
			}
			if ($nb_item % 2 == 1) {
				$clearClass .= " first-xs-item";
			}
			return $clearClass;
			break;
		case 7:
			if ($nb_item % 7 == 1) {
				$clearClass .= " first-row-item";
			}
			if ($nb_item % 2 == 1) {
				$clearClass .= " first-sm-item first-xs-item";
			}
			return $clearClass;
			break;
		case 8:
			if ($nb_item % 8 == 1) {
				$clearClass .= " first-row-item";
			}
			if ($nb_item % 3 == 1) {
				$clearClass .= " first-sm-item";
			}
			if ($nb_item % 2 == 1) {
				$clearClass .= " first-xs-item";
			}
			return $clearClass;
			break;
		}
		return $clearClass;
	}

	public function getRootCategory() {
		$store = $this->getStore();
		$categoryId = $store->getRootCategoryId();
		$category = $this->getModel('Magento\Catalog\Model\Category')->load($categoryId);
		return $category;
	}

	public function getTreeCategory($category, $parent, $ids = array(), $checkedCat) {
		$rootCategoryId = $this->getRootCategory()->getId();
		$children = $category->getChildrenCategories();
		$childrenCount = count($children);
		//$checkedCat = explode(',',$checkedIds);
		$htmlLi = '<li lang="' . $category->getId() . '">';
		$html[] = $htmlLi;
		//if($this->isCategoryActive($category)){
		$ids[] = $category->getId();
		//$this->_ids = implode(",", $ids);
		//}

		$html[] = '<a id="node' . $category->getId() . '">';

		if ($category->getId() != $rootCategoryId) {
			$html[] = '<input lang="' . $category->getId() . '" type="checkbox" id="radio' . $category->getId() . '" name="setting[category_id][]" value="' . $category->getId() . '" class="checkbox' . $parent . '"';
			if (in_array($category->getId(), $checkedCat)) {
				$html[] = ' checked="checked"';
			}
			$html[] = '/>';
		}

		$html[] = '<label for="radio' . $category->getId() . '">' . $category->getName() . '</label>';

		$html[] = '</a>';

		$htmlChildren = '';
		if ($childrenCount > 0) {
			foreach ($children as $child) {
				$_child = $this->getModel('Magento\Catalog\Model\Category')->load($child->getId());
				$htmlChildren .= $this->getTreeCategory($_child, $category->getId(), $ids, $checkedCat);
			}
		}
		if (!empty($htmlChildren)) {
			$html[] = '<ul id="container' . $category->getId() . '">';
			$html[] = $htmlChildren;
			$html[] = '</ul>';
		}

		$html[] = '</li>';
		$html = implode("\n", $html);
		return $html;
	}

	public function truncate($content, $length) {
		return $this->filterManager->truncate($content, ['length' => $length, 'etc' => '']);
	}

	public function convertToLayoutUpdateXml($child) {
		$settings = json_decode($child->getSetting(), true);
		$content = $child->getBlockContent();
		$content = preg_replace('/(mgs_panel_title="")/i', '', $content);
		$content = preg_replace('/(mgs_panel_title=".+?)+(")/i', '', $content);
		$content = preg_replace('/(mgs_panel_note="")/i', '', $content);
		$content = preg_replace('/(mgs_panel_note=".+?)+(")/i', '', $content);
		$content = preg_replace('/(labels=".+?)+(")/i', '', $content);
		$arrContent = explode(' ', $content);
		$arrContent = array_filter($arrContent);
		$class = $arrContent[1];
		$class = str_replace('type=', 'class=', $class);
		unset($arrContent[0], $arrContent[1]);

		$lastData = end($arrContent);
		//print_r($arrContent); die();
		array_pop($arrContent);

		$arrContent = array_values($arrContent);

		$argumentString = '&nbsp;&nbsp;&nbsp;&nbsp;&lt;arguments&gt;<br/>';

		if (isset($settings['title']) && ($settings['title'] != '')) {
			$argumentString .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;argument name="mgs_panel_title" xsi:type="string"&gt;' . htmlspecialchars($this->encodeHtml($settings['title'])) . '&lt;/argument&gt;<br/>';
		}
		if (isset($settings['additional_content']) && ($settings['additional_content'] != '')) {
			$argumentString .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;argument name="mgs_panel_note" xsi:type="string"&gt;' . htmlspecialchars($this->encodeHtml($settings['additional_content'])) . '&lt;/argument&gt;<br/>';
		}
		if (isset($settings['tabs']) && ($settings['tabs'] != '')) {
			usort($settings['tabs'], function ($item1, $item2) {
				if ($item1['position'] == $item2['position']) {
					return 0;
				}

				return $item1['position'] < $item2['position'] ? -1 : 1;
			});
			$tabType = $tabLabel = [];
			foreach ($settings['tabs'] as $tab) {
				$tabLabel[] = $tab['label'];
			}
			$labels = implode(',', $tabLabel);
			$argumentString .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;argument name="labels" xsi:type="string"&gt;' . $labels . '&lt;/argument&gt;<br/>';
		}
		$template = '';

		foreach ($arrContent as $argument) {
			$argumentData = explode('=', $argument);
			if ($argumentData[0] != 'template' && isset($argumentData[0]) && isset($argumentData[1])) {
				$argumentString .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;argument name="' . $argumentData[0] . '" xsi:type="string"&gt;' . str_replace('"', '', $argumentData[1]) . '&lt;/argument&gt;<br/>';
			} else {
				$template = $argumentData[1];
			}

		}

		$html = '&lt;block ' . $class;

		$lastDataArr = explode('=', $lastData);
		if (isset($lastDataArr[0]) && isset($lastDataArr[1])) {
			if ($lastDataArr[0] == 'template') {
				$template = str_replace('}}', '', $lastDataArr[1]);
			} else {
				$argumentString .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;argument name="' . $lastDataArr[0] . '" xsi:type="string"&gt;' . str_replace('"', '', str_replace('}}', '', $lastDataArr[1])) . '&lt;/argument&gt;<br/>';
			}
		}

		$html .= ' template=' . $template;

		$argumentString .= '&nbsp;&nbsp;&nbsp;&nbsp;&lt;/arguments&gt;';

		$html .= '&gt;<br/>';
		$html .= $argumentString;
		$html .= '<br/>&lt;/block&gt;';

		return $html;
	}

	/* Get all images from pub/media/wysiwyg/$type folder */
	public function getPanelUploadImages($type) {
		$path = 'wysiwyg/' . $type . '/';
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath($path);
		$result = [];
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while ($files[] = readdir($dh));
				sort($files);
				foreach ($files as $file) {
					$file_parts = pathinfo($dir . $file);
					if (isset($file_parts['extension']) && in_array(strtolower($file_parts['extension']), ['jpg', 'jpeg', 'png', 'gif'])) {
						$result[] = $file;
					}
				}
			}
		}
		return $result;
	}

	/* Convert short code to insert image */
	public function convertImageWidgetCode($type, $image) {
		return '&lt;img src="{{media url="wysiwyg/' . $type . '/' . $image . '"}}" alt=""/&gt;';
	}

	public function encodeHtml($html) {
		$result = str_replace("<", "&lt;", $html);
		$result = str_replace(">", "&gt;", $result);
		$result = str_replace('"', '&#34;', $result);
		$result = str_replace("'", "&#39;", $result);
		return $result;
	}

	public function decodeHtmlTag($content) {
		$result = str_replace("&lt;", "<", $content);
		$result = str_replace("&gt;", ">", $result);
		$result = str_replace('&#34;', '"', $result);
		$result = str_replace("&#39;", "'", $result);
		return $result;
	}

	public function getCmsBlockByIdentifier($identifier) {
		$block = $this->_blockFactory->create();
		$block->setStoreId($this->getStore()->getId())->load($identifier);
		return $block;
	}

	public function getPageById($id) {
		$page = $this->_pageFactory->create();
		$page->setStoreId($this->getStore()->getId())->load($id, 'identifier');
		return $page;
	}

	public function getHeaderClass() {
		$header = $this->getStoreConfig('mgstheme/general/header');
		if ($header != '') {
			$class = str_replace('.phtml', '', $header);
			$class = str_replace('_', '', $class);
		} else {
			$class = "header1";
		}
		if ($this->_acceptToUsePanel) {
			$class .= ' builder-container header-builder';
		}
		return $class;
	}

	public function getFooterClass() {
		if ($this->getStoreConfig('mgstheme/general/footer')) {
			$footer = $this->getStoreConfig('mgstheme/general/footer');
			$class = str_replace('.phtml', '', $footer);
			$class = str_replace('_', '', $class);
		} else {
			$class = "footer1";
		}
		if ($this->_acceptToUsePanel) {
			$class .= ' builder-container footer-builder';
		}
		return $class;
	}

	public function getContentVersion($type, $themeId) {
		$theme = $this->getModel('Magento\Theme\Model\Theme')->load($themeId);
		$themePath = $theme->getThemePath();
		$themePath = substr($themePath, (strpos($themePath, "/") + 1));
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('mgs/' . $themePath . '/' . $type);

		$result = [];
		$files = [];
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while ($files[] = readdir($dh));
				sort($files);
				foreach ($files as $file) {
					$file_parts = pathinfo($dir . $file);
					if (isset($file_parts['extension']) && $file_parts['extension'] == 'png') {
						$fileName = str_replace('.png', '', $file);
						$result[] = array('value' => $fileName, 'label' => $this->convertFilename($fileName), 'path' => $themePath);
					}
				}
				closedir($dh);
			}
		}

		if (count($result) == 0) {
			$dir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('mgs/mgsblank/' . $type);
			if (is_dir($dir)) {
				if ($dh = opendir($dir)) {
					while ($files[] = readdir($dh));
					sort($files);
					foreach ($files as $file) {
						$file_parts = pathinfo($dir . $file);
						if (isset($file_parts['extension']) && $file_parts['extension'] == 'png') {
							$fileName = str_replace('.png', '', $file);
							$result[] = array('value' => $fileName, 'label' => $this->convertFilename($fileName), 'path' => 'mgsblank');
						}
					}
					closedir($dh);
				}
			}
		}
		return $result;
	}

	public function convertFilename($filename) {
		$filename = str_replace('_', ' ', $filename);
		$filename = ucfirst($filename);
		return $filename;
	}

	public function isFile($path, $type, $fileName) {
		$path = str_replace('Mgs/', '', $path);
		$filePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('mgs/' . $path . '/' . $type . 's/') . $fileName . '.png';
		if ($this->_file->isExists($filePath)) {
			return $this->_url->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . 'mgs/' . $path . '/' . $type . 's/' . $fileName . '.png';
		}
		return false;
	}

	public function getCurrentCategory() {

		$id = $this->_request->getParam('id');
		$this->_currentCategory = $this->getModel('Magento\Catalog\Model\Category')->load($id);
		return $this->_currentCategory;

	}

	public function getCurrentProduct() {

		$id = $this->_request->getParam('id');
		$this->_currentProduct = $this->getModel('Magento\Catalog\Model\Product')->load($id);
		return $this->_currentProduct;

	}

	public function isCategoryPage() {
		if ($this->_request->getFullActionName() == 'catalog_category_view') {
			return true;
		}
		return false;
	}

	public function isSearchPage() {
		if ($this->_request->getFullActionName() == 'catalogsearch_result_index') {
			return true;
		}
		return false;
	}

	public function isProductPage() {
		if ($this->_request->getFullActionName() == 'catalog_product_view') {
			return true;
		}
		return false;
	}

	public function isPopup() {
		if (
			$this->_request->getFullActionName() == 'mgs_quickview_catalog_product_view' ||
			$this->_request->getFullActionName() == 'mpanel_edit_section' ||
			$this->_request->getFullActionName() == 'mpanel_create_block' ||
			$this->_request->getFullActionName() == 'mpanel_create_element' ||
			$this->_request->getFullActionName() == 'mpanel_edit_footer' ||
			$this->_request->getFullActionName() == 'mpanel_edit_header' ||
			$this->_request->getFullActionName() == 'mpanel_edit_staticblock'
		) {
			return true;
		}
		return false;
	}
	/* Search with categories */
	public function getCategories() {
		$rootCategoryId = $this->_storeManager->getStore()->getRootCategoryId();
		$categoriesArray = $this->_category
			->getCollection()
			->setStoreId($this->_storeManager->getStore()->getId())
			->addAttributeToSelect('*')
			->addAttributeToFilter('is_active', 1)
			->addAttributeToFilter('include_in_menu', 1)
			->addAttributeToFilter('path', array('like' => "1/{$rootCategoryId}/%"))
			->addAttributeToSort('path', 'asc')
			->load()
			->toArray();
		$categories = array();
		if (isset($categoriesArray['items'])) {
			foreach ($categoriesArray['items'] as $categoryId => $category) {
				if (isset($category['name'])) {
					$categories[] = array(
						'label' => $category['name'],
						'level' => $category['level'],
						'value' => $category['entity_id'],
					);
				}
			}
		} else {
			foreach ($categoriesArray as $categoryId => $category) {
				if (isset($category['name'])) {
					$categories[] = array(
						'label' => $category['name'],
						'level' => $category['level'],
						'value' => $category['entity_id'],
					);
				}
			}
		}
		return $categories;
	}

	public function getCurrentlySelectedCategoryId() {
		$params = $this->getModel('Magento\Framework\App\Request\Http')->getParams();
		if (isset($params['cat'])) {
			return $params['cat'];
		}
		return '';
	}

	public function getProductLayout($product) {
		$pageLayout = $product->getPageLayout();

		if ($pageLayout == '') {
			$pageLayout = $this->getStoreConfig('mpanel/product_details/product_layout');
		}

		if ($pageLayout == '') {
			$pageLayout = '1column';
		}

		return $pageLayout;
	}

	public function getRotateImages($productId) {
		$dir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('wysiwyg/360/' . $productId);

		$result = [];
		$files = [];
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while ($files[] = readdir($dh));
				sort($files);
				foreach ($files as $file) {
					$file_parts = pathinfo($dir . $file);
					if (isset($file_parts['extension']) && (($file_parts['extension'] == 'jpg') || ($file_parts['extension'] == 'png'))) {
						$result[] = $this->getMediaUrl() . 'wysiwyg/360/' . $productId . '/' . $file;
					}
				}
				closedir($dh);
			}
		}
		return $result;
	}

	public function getMediaUrl() {
		return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
	}

	public function convertContent($layoutContent, $builderContent = NULL) {
		$class = "";
		if ($this->getStoreConfig('mgstheme/general/header') == 'header_5') {
			$class .= ' menu_vertical_fixed';
		}
		if ($this->getStoreConfig('mgstheme/general/lazy_load')) {
			$class .= ' lazy-loading-img';
		}
		$beginMain = '<div class="page-wrapper ' . $class . '" data-ratioimage="ratio-' . $this->getStoreConfig('mpanel/catalog/picture_ratio') . '">';

		if ($this->getStoreConfig('mgstheme/general/header_absolute')) {
			$beginMain .= '<div id="placeholder-header" class="absolute-header"></div>';
		} else {
			$beginMain .= '<div id="placeholder-header"></div>';
		}

		$endMain = '</div>';

		$layoutContent = str_replace('<main>', '<main>' . $beginMain, $layoutContent);
		$layoutContent = str_replace('</main>', $endMain . '</main>', $layoutContent);

		if ($this->_acceptToUsePanel) {
			$beginHeader = '<div class="edit-panel edit-header"><ul><li><a class="popup-link" href="' . $this->getUrlBuilder()->getUrl('mpanel/edit/header', ['type' => 'header']) . '" title="' . __('Edit Header') . '"><em class="fa fa-gear"></em></a></li></ul></div>';
			$layoutContent = str_replace('<header class="header">', '<header class="header">' . $beginHeader, $layoutContent);

			$beginFooter = '<div class="edit-panel edit-footer"><ul><li><a class="popup-link" href="' . $this->getUrlBuilder()->getUrl('mpanel/edit/footer', ['type' => 'footer']) . '" title="' . __('Edit Footer') . '"><em class="fa fa-gear"></em></a></li></ul></div>';
			$layoutContent = str_replace('<footer class="footer">', '<footer class="footer">' . $beginFooter, $layoutContent);
		}

		if ($this->_acceptToUsePanel && ($this->isCmsPage() || $this->isHomepage())) {
			$arrContent = explode('<section id="maincontent" class="page-main">', $layoutContent);

			$topContent = $arrContent[0];

			$arrContent = explode('</section>', $arrContent[1]);

			$bottomContent = $arrContent[1];

			$condition = '#<\!--\[if[^\>]*>\s*<script.*</script>\s*<\!\[endif\]-->#isU';
			preg_match_all($condition, $arrContent[0], $matches);
			$ifJs = implode('', $matches[0]);

			$temp = preg_replace($condition, '', $arrContent[0]);

			$condition = '@(?:<script|<script)(.*)</script>@msU';
			preg_match_all($condition, $temp, $matches);
			$js = implode('', $matches[0]);

			$formKey = explode('<input name="form_key" type="hidden" value="', $layoutContent);
			$formKey = explode('"', $formKey[1]);

			$script = '<input name="form_key" type="hidden" value="' . $formKey[0] . '"/>' . $js . $ifJs;

			$builderContent = $script . $builderContent;

			$layoutContent = $topContent . $builderContent . $bottomContent;
		}

		$layoutContent = str_replace('<header class="header">', '<header class="header ' . $this->getHeaderClass() . '">', $layoutContent);
		$layoutContent = str_replace('<footer class="footer">', '<footer class="footer ' . $this->getFooterClass() . '">', $layoutContent);

		return $layoutContent;
	}

	public function generateCssForAll() {
		$stores = $this->_storeManager->getWebsite()->getStores();
		foreach ($stores as $_store) {
			$this->generateCssByStore($_store->getId());
		}
	}

	public function generateCssByStore($storeId) {
		$html = $this->getLinksFont();

		$themeId = $this->getStoreConfig('design/theme/theme_id', $storeId);
		$theme = $this->getModel('Magento\Theme\Model\Theme')->load($themeId);
		$themePath = $theme->getThemePath();
		$themeName = substr($themePath, (strpos($themePath, "/") + 1));

		$fontName = $this->getStoreConfig('mgstheme/custom_style/font_name', $storeId);
		if ($fontName != '') {
			$fontDir = str_replace('http:', '', $this->getUrlBuilder()->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA])) . \MGS\Mpanel\Model\Config\Backend\Font::UPLOAD_DIR . '/';
			$ttfFile = $fontDir . $this->getStoreConfig('mgstheme/custom_style/ttf_file', $storeId);
			$eotFile = $fontDir . $this->getStoreConfig('mgstheme/custom_style/eot_file', $storeId);
			$woffFile = $fontDir . $this->getStoreConfig('mgstheme/custom_style/woff_file', $storeId);
			$svgFile = $fontDir . $this->getStoreConfig('mgstheme/custom_style/svg_file', $storeId);

			if ($ttfFile != '' && $eotFile != '') {
				$html .= '@font-face {
						font-family: "' . $fontName . '";
						src: url("' . $eotFile . '");
						src: url("' . $eotFile . '?#iefix") format("embedded-opentype"),
							 url("' . $woffFile . '") format("woff"),
							 url("' . $ttfFile . '") format("truetype"),
							 url("' . $svgFile . '#' . $fontName . '") format("svg");
						font-weight: normal;
						font-style: normal;
				}';
			}

		}

		$html .= 'body{';
		$backgroundColor = $this->getStoreConfig('mgstheme/background/background_color', $storeId);
		$backgroundImage = $this->getStoreConfig('mgstheme/background/background_image', $storeId);
		if ($backgroundColor != '') {
			$html .= 'background-color:' . $backgroundColor . ';';
		}
		if ($backgroundImage != '') {
			$folderName = \MGS\Mpanel\Model\Config\Backend\Image::UPLOAD_DIR;

			$path = $folderName . '/' . $backgroundImage;
			$backgroundImageUrl = $this->getUrlBuilder()->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $path;

			$html .= 'background-image:url(' . $backgroundImageUrl . ');';
			$backgroundCover = $this->getStoreConfig('mgstheme/background/background_cover', $storeId);
			if ($backgroundCover) {
				$html .= 'background-size:cover;';
			} else {
				$backgroundRepeat = $this->getStoreConfig('mgstheme/background/background_repeat', $storeId);
				$html .= 'background-repeat:' . $backgroundRepeat . ';';
			}
			$backgroundPositionX = $this->getStoreConfig('mgstheme/background/background_position_x', $storeId);
			$backgroundPositionY = $this->getStoreConfig('mgstheme/background/background_position_y', $storeId);
			$html .= 'background-position:' . $backgroundPositionX . ' ' . $backgroundPositionY . ';';
		}

		if ($this->getStoreConfig('mgstheme/fonts/default_font', $storeId) != '') {
			$html .= 'font-family: "' . str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/default_font', $storeId)) . '", arial, tahoma;font-weight: normal;';
		}

		$fontSize = $this->getStoreConfig('mgstheme/fonts/default_font_size', $storeId);
		if ($fontSize != '') {
			$html .= 'font-size:' . $fontSize . ';';
		}

		$html .= '}';
		$custom_font = $this->getStoreConfig('mgstheme/fonts/custom_fonts_element', $storeId);
		$fontStyle = [
			'#mainMenu li a.level0, .navigation ul.container .level0 > a' => [
				'font-size' => $this->getStoreConfig('mgstheme/fonts/menu_font_size', $storeId),
			],
			'#mainMenu' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/menu', $storeId)),
			],
			'h1' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/h1', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/h1_font_size', $storeId),
			],
			'h2' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/h2', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/h2_font_size', $storeId),
			],
			'h3' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/h3', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/h3_font_size', $storeId),
			],
			'h4' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/h4', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/h4_font_size', $storeId),
			],
			'h5' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/h5', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/h5_font_size', $storeId),
			],
			'h6' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/h6', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/h6_font_size', $storeId),
			],
			'.price, .price-box .price' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/price', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/price_font_size', $storeId),
			],
			'.btn' => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/btn', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/btn_font_size', $storeId),
			],
			$custom_font => [
				'font-family' => str_replace('+', ' ', $this->getStoreConfig('mgstheme/fonts/custom_font_fml', $storeId)),
				'font-size' => $this->getStoreConfig('mgstheme/fonts/custom_font_size', $storeId),
			],
		];

		$fontStyle = array_filter($fontStyle);

		foreach ($fontStyle as $class => $style) {
			$style = array_filter($style);
			if (count($style) > 0) {
				$html .= $class . '{';
				foreach ($style as $_style => $value) {
					if ($_style == 'font-family') {
						$html .= $_style . ': "' . $value . '";';
					} else {
						$html .= $_style . ': ' . $value . ';';
					}
				}
				$html .= '}
				';
			}
		}

		if (($this->getStoreConfig('color/general/theme_color', $storeId) != '') && ($this->getStoreConfig('color/general/theme_color', $storeId) != 'transparent')) {
			$themeColorSetting = $this->getThemecolorSetting($storeId, $themeName);
			if (count($themeColorSetting) > 0) {
				foreach ($themeColorSetting as $class => $style) {
					$style = array_filter($style);
					if (count($style) > 0) {
						$html .= $class . '{';
						foreach ($style as $_style => $value) {
							$html .= $_style . ': ' . $value . ';';
						}
						$html .= '}';
					}
				}
			}
		}

		if ($this->getStoreConfig('color/header/header_custom', $storeId)) {
			$headerColorSetting = $this->getHeaderColorSetting($storeId, $themeName);
			if (count($headerColorSetting) > 0) {
				foreach ($headerColorSetting as $class => $style) {
					$style = array_filter($style);
					if (count($style) > 0) {
						$html .= $class . '{';
						foreach ($style as $_style => $value) {
							$html .= $_style . ': ' . $value . ' !important;';
						}
						$html .= '}';
					}
				}
			}
		}

		if ($this->getStoreConfig('color/main/main_custom', $storeId)) {
			$mainColorSetting = $this->getMainColorSetting($storeId, $themeName);
			if (count($mainColorSetting) > 0) {
				foreach ($mainColorSetting as $class => $style) {
					$style = array_filter($style);
					if (count($style) > 0) {
						$html .= $class . '{';
						foreach ($style as $_style => $value) {
							$html .= $_style . ': ' . $value . ' !important;';
						}
						$html .= '}';
					}
				}
			}
		}

		if ($this->getStoreConfig('color/footer/footer_custom', $storeId)) {
			$footerColorSetting = $this->getFooterColorSetting($storeId, $themeName);
			if (count($footerColorSetting) > 0) {
				foreach ($footerColorSetting as $class => $style) {
					$style = array_filter($style);
					if (count($style) > 0) {
						$html .= $class . '{';
						foreach ($style as $_style => $value) {
							$html .= $_style . ': ' . $value . ' !important;';
						}
						$html .= '}';
					}
				}
			}
		}

		if ($this->getStoreConfig('mgstheme/custom_style/style', $storeId) != '') {
			$html .= $this->getStoreConfig('mgstheme/custom_style/style', $storeId);
		}

		$this->generateFile($storeId, $html);

		return;
	}

	public function generateFile($storeId, $content) {
		$filePath = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('mgs/css/' . $storeId . '/');
		$io = $this->_ioFile;
		$file = $filePath . 'custom_config.css';
		$io->setAllowCreateFolders(true);
		$io->open(array('path' => $filePath));
		$io->write($file, $content, 0644);
		$io->streamClose();
	}

	public function getFullActionName() {
		$request = $this->_objectManager->get('\Magento\Framework\App\Request\Http');
		return $request->getFullActionName();
	}
	public function getPanelUploadSrc($type, $file) {
		return $this->_url->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . 'wysiwyg/' . $type . '/' . $file;
	}

	public function getValuesNumberProductPerRow() {
		$stringValues = $this->getStoreConfig('mpanel/catalog/config_change_product_per_row');
		if ($stringValues == null) {
			return $stringValues;
		}
		$arrayValues = explode(',', $stringValues);
		return $arrayValues;
	}
	public function getDefaultValueNumberProductPerRow() {
		$defaultValue = $this->getStoreConfig('mpanel/catalog/default_number_product_per_row');
		return $defaultValue;
	}
	public function getRequest() {
		return $this->_request;
	}

	public function getConfigShowTooltip() {
		return $this->getStoreConfig('catalog/frontend/show_swatch_tooltip');
	}
}
