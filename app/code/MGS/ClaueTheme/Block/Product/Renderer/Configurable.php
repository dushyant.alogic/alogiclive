<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MGS\ClaueTheme\Block\Product\Renderer;

/**
 * Swatch renderer block
 *
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Configurable extends \Magento\Swatches\Block\Product\Renderer\Configurable {
	/**
	 * Get product images for configurable variations
	 *
	 * @return array
	 * @since 100.2.0
	 */
	protected function getOptionImages() {
		$images = [];
		foreach ($this->getAllowProducts() as $product) {
			$productImages = $this->helper->getGalleryImages($product) ?: [];

			$prodImages = [];
			$baseImage = '';

			foreach ($productImages as $galImage) {
				if ($galImage->getIsBaseImage()) {
					$baseImage = $galImage;
					continue;
				}

				$prodImages[] = $galImage;
			}
			$prodImages[] = $baseImage;
			$prodImages = array_reverse($prodImages);

			foreach ($prodImages as $image) {
				if ($image) {
					$images[$product->getId()][] =
						[
						'thumb' => $image->getData('small_image_url'),
						'img' => $image->getData('medium_image_url'),
						'full' => $image->getData('large_image_url'),
						'zoom' => $image->getData('zoom_image_url'),
						'caption' => $image->getLabel(),
						'position' => $image->getPosition(),
						'isMain' => $image->getFile() == $product->getImage(),
						'type' => str_replace('external-', '', $image->getMediaType()),
						'videoUrl' => $image->getVideoUrl(),
					];
				}

			}
			if($product->getId() == '7042'){
				$nimg = $images[$product->getId()][0];
				$images[$product->getId()][0] = $images[$product->getId()][1];
				$images[$product->getId()][1] = $nimg;	
			}
			
		}

		return $images;
	}
}

