<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MGS\InstantSearch\Controller\Ajax;

use MGS\InstantSearch\Controller\AbstractSearch;
use \Magento\Framework\Controller\ResultFactory;
class Result extends AbstractSearch
{
    /**
     * Display search result
     *
     * @return void
     */
    public function execute()
    {
        $searchType = '';
        if($this->getRequest()->getParam('search-type')){
            $searchType = $this->getRequest()->getParam('search-type');
        }
            $resultPage = $this->_resultPageFactory->create();
            $responseData = [];
            /* @var $query \Magento\Search\Model\Query */
            $query = $this->_queryFactory->get();

            $query->setStoreId($this->_storeManager->getStore()->getId());
            if ($query->getQueryText() != '') {
                $query->setId(0)->setIsActive(1)->setIsProcessed(1);
                $results = $this->_search->getData();                
                $responseData['result'] = $results;                
            }

        if($searchType == 'yes'){

             $resultPage = $this->_resultPageFactory->create();
             $resultPage->addHandle('instantsearch_product_result'); //loads the layout of instantsearch_product_result.xml file with its name
             /** @var Template $block */
             $block = $resultPage->getLayout()->getBlock('instantSearch.product.result');
             $products = $results['product']['data'];
             $skus = array_map(function ($ar) {return $ar['sku'];}, $products);
             $collection = $this->getCustomCollection($skus);
             $pager = $resultPage->getLayout()->createBlock(
        'Magento\Theme\Block\Html\Pager',
        'custom.history.pager1'
      )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
        ->setShowPerPage(true)->setCollection(
          $collection
        );


             $block->setData('pager',$pager);
             $block->setData('collection', $collection);

                
          $collection->load();


             return $resultPage;
        }else{
            /** @var \Magento\Framework\Controller\Result\Json $resultJson */
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData($responseData);
            return $resultJson;
        }
        
    }

    public function getCustomCollection($skus)
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest(
         
        )->getParam('limit') : 5;

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $productCollectionFactory = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory')->create();

        // Get product collection
        $collection = $productCollectionFactory->addAttributeToSelect('*')
            ->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
            ->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
            ->addAttributeToFilter('sku', ['in'=>$skus])
            ->addStoreFilter(1) 
            ->setOrder('entity_id', 'DESC');

        //$collection = $this->customCollection->getCollection();
        $collection->setPageSize($collection->count());
        $collection->setCurPage($page);
        return $collection;
    }


    /**
     *
     * @return void
     */
    public function _isAllowedType(){}
}