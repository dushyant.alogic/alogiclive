<?php
namespace MGS\InstantSearch\Block;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Pricing\Helper\Data as priceHelper;
class Customresult extends Template
{
  protected $customCollection;
  protected $priceHepler;
  public function __construct(Context $context, priceHelper $priceHepler)
  {
    
    $this->priceHepler = $priceHepler;
    parent::__construct($context);
  }
  protected function _prepareLayout()
  {
    parent::_prepareLayout();
    $this->pageConfig->getTitle()->set(__('Custom Pagination'));    
    return $this;
  }
  public function getPagerHtml()
  {
    return $this->getChildHtml('pager');
  }
  
  public function getFormattedPrice($price)
  {
    return $this->priceHepler->currency(number_format($price, 2), true, false);
  }
}