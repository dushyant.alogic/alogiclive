define([
    'jquery',
    'uiComponent',
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/checkout-data',
    'mage/validation'
], function ($, Component, ko, quote, checkoutData) {
    'use strict';
	
		return Component.extend({
			
			initObservable: function () {
				this._super();
				
				
				jQuery(document).on('keyup','input[name="street[0]"]',  function (e) {
					
					var handler = jQuery(this).attr('id'); 
					
					if(e.keyCode == 38 || e.keyCode == 40){
						console.log(e.keyCode);
					}else{
						var componentForm = {
						  subpremise: 'long_name',
						  street_number: 'long_name',
						  route: 'long_name',
						  neighborhood: 'long_name',
						  locality: 'long_name',
						  administrative_area_level_1: 'long_name',
						  country: 'short_name',
						  postal_code: 'long_name'
						};
						var area = {};
						var autocomplete = '';
						if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(function(position) {
						  var geolocation = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						  };
						  var circle = new google.maps.Circle({
							center: geolocation,
							radius: position.coords.accuracy
						  });
						 autocomplete.setBounds(circle.getBounds());
						});
						}
						  autocomplete = new google.maps.places.Autocomplete((document.getElementById(handler)), { types: ['geocode'] });
						
						
						google.maps.event.addListener(autocomplete, 'place_changed', function() {
							var place = autocomplete.getPlace();
							
							for (var i = 0; i < place.address_components.length; i++) {
								var addressType = place.address_components[i].types[0];
								
								if (componentForm[addressType]) {
								   area[addressType] = place.address_components[i][componentForm[addressType]];
								
								}
							  }
							if(place.address_components[1].types[0] == 'locality'){
								var street = place.address_components[0]['long_name'];
							}else{
								if(area['country'] == "DE"){
                                    var street = place.address_components[1]['long_name'] + ' ' + place.address_components[0]['long_name'];
								} else {
									if(place.address_components[0].types[0] == "subpremise") {
										var street = area['subpremise'] + '/' + place.address_components[0]['long_name'] + ' ' + place.address_components[1]['long_name'];
									} else {
										var street = place.address_components[0]['long_name'] + ' ' + place.address_components[1]['long_name'];
									}
                                    
                                }
							}
							
							jQuery('select[name="country_id"]').val(area['country']);
							jQuery('select[name="country_id"]').trigger('change');
							jQuery('input[name="street[0]"]').val(street);
							jQuery('input[name="street[0]"]').trigger('keyup');
							jQuery('input[name="city"]').val(area['locality']);
							jQuery('input[name="city"]').trigger('keyup');
							jQuery('input[name="postcode"]').val(area['postal_code']);
							jQuery('input[name="postcode"]').trigger('keyup');
							
							var flag = 0;
							if(jQuery("[name=region_id]").parent().parent().css('display') != "none"){
								jQuery('[name="region_id"] option').each(function () {
									if (jQuery(this).html() == area['administrative_area_level_1']) {
										jQuery(this).prop("selected", true);
										jQuery(this).trigger('change');
										flag = 1;
										return;
									}
								});
							}
							if(jQuery("[name=region]").parent().parent().css('display') != "none"){
								if(flag == 0){
									jQuery('[name="region"]').val(area['administrative_area_level_1']);
									jQuery('input[name="region"]').trigger('keyup');
								}
							} else {
								jQuery('[name="region"]').val('');
								jQuery('input[name="region"]').trigger('keyup');
							}
						});  
					}
				});
				return this;
			},
		});
});