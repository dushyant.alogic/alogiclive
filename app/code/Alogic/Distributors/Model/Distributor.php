<?php

namespace Alogic\Distributors\Model;

class Distributor extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {
	protected function _construct() {
		$this->_init('Alogic\Distributors\Model\ResourceModel\Distributor');
	}

	public function getIdentities() {
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues() {
		$values = [];

		return $values;
	}
}