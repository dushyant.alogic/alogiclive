<?php

namespace Alogic\Storeswitcher\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;

class CheckCountryObserver implements ObserverInterface {
	const COOKIE_NAME = 'test';
	const COOKIE_DURATION = 86400; // lifetime in seconds
	/**
	 * @var \Magento\Framework\Stdlib\CookieManagerInterface
	 */
	protected $_cookieManager;
/**
 * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
 */
	protected $_cookieMetadataFactory;
/**
 * @param \Magento\Framework\App\Action\Context $context
 * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
 * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
 */
	/**
	 * @var \Magento\Framework\App\Response\RedirectInterface
	 */
	protected $redirect;
	protected $_redirect;
	private $storeManager;
	/**
	 * Customer session
	 *
	 * @var \Magento\Customer\Model\Session
	 */
	protected $_customerSession;
	protected $geoIpService;
	protected $_state;

	/**
	 * @var \Magento\Framework\App\ResponseFactory
	 */
	private $responseFactory;
	public function __construct(
		\Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
		\Magento\Framework\App\State $state,
		\Magento\Framework\App\ResponseFactory $responseFactory,
		\MagePal\GeoIp\Service\GeoIpService $geoIpService,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
		\Magento\Customer\Model\Session $customerSession,
		StoreManagerInterface $storeManager,
		\Magento\Framework\App\Response\Http $redirectUrl,
		\Magento\Framework\App\Response\RedirectInterface $redirect

	) {
		$this->_cookieManager = $cookieManager;
		$this->_state = $state;
		$this->responseFactory = $responseFactory;
		$this->geoIpService = $geoIpService;
		$this->_cookieMetadataFactory = $cookieMetadataFactory;
		$this->_customerSession = $customerSession;
		$this->storeManager = $storeManager;
		$this->_redirect = $redirectUrl;
		$this->redirect = $redirect;

	}
	public function getArea() {
		return $this->_state->getAreaCode();
	}

	public function getCurrentUrl() {
		return $this->storeManager->getStore()->getCurrentUrl();
	}
/** Set Custom Cookie using Magento 2 */
	public function setCustomCookie($countryCode = null) {
		$publicCookieMetadata = $this->_cookieMetadataFactory->createPublicCookieMetadata();
		$publicCookieMetadata->setDurationOneYear();
		$publicCookieMetadata->setPath('/');
		$publicCookieMetadata->setHttpOnly(false);

		return $this->_cookieManager->setPublicCookie(
			'magento2cookie',
			$countryCode,
			$publicCookieMetadata
		);
	}

	/** Get Custom Cookie using */
	public function getCustomCookie() {
		return $this->_cookieManager->getCookie(
			'magento2cookie'
		);
	}
	public function execute(\Magento\Framework\Event\Observer $observer) {
		$area = $this->getArea();
		$controller = $observer->getControllerAction();
		if ($area == 'frontend') {
			$countryCode = $this->geoIpService->getCountry();

			$cookieValue = $this->getCustomCookie();
			$writer = new \Laminas\Log\Writer\Stream(BP . '/var/log/magelog.log');
			$logger = new \Laminas\Log\Logger();
			$logger->addWriter($writer);
			$web_url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
			if ($cookieValue == '') {
				$this->setCustomCookie($countryCode);
				$cookieValue = $this->getCustomCookie();
				$storeCode = '';
				if ($countryCode == 'IN') {
		$logger->info("cookieValue " . $cookieValue);
					$logger->info("countryCode " . $countryCode);
					$storeCode = 'in';
				} elseif ($countryCode == 'US') {
					$storeCode = 'us';
				} elseif ($countryCode == 'CA') {
					$storeCode = 'ca';
				} elseif ($countryCode == 'GB') {
					$storeCode = 'uk';
				} elseif ($countryCode == 'DE') {
					$storeCode = 'de';
				} elseif ($countryCode == 'NZ') {
					$storeCode = 'nz';
				} elseif ($countryCode == 'SE') {
					$storeCode = 'se';
				}
				$web_url = $web_url . $storeCode;

				$logger->info("web_url" . $web_url);
				//$this->redirect->redirect($controller->getResponse(), $storeCode);
				 $CustomRedirectionUrl ='https://alogic.co/'.$storeCode;
				 $logger->info("redirecting..." . $CustomRedirectionUrl);

                                 $this->_redirect->setRedirect($CustomRedirectionUrl);

				//$this->responseFactory->create()->setRedirect($web_url)->sendResponse();
			}
		}
		return $this;

	}

}
