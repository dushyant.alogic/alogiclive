<?php
namespace Alogic\Customadmin\Block\Adminhtml\Banner\Edit\Tab;
use Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button;
use Magento\Backend\Block\Widget\Form\Element\Dependence;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Cms\Model\Wysiwyg\Config as WysiwygConfig;
use Magento\Config\Model\Config\Source\Enabledisable;
use Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory;
use Magento\Framework\Convert\DataObject;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Mageplaza\BannerSlider\Block\Adminhtml\Banner\Edit\Tab\Render\Image as BannerImage;
use Mageplaza\BannerSlider\Block\Adminhtml\Banner\Edit\Tab\Render\Slider;
use Mageplaza\BannerSlider\Helper\Data;
use Mageplaza\BannerSlider\Helper\Image as HelperImage;
use Mageplaza\BannerSlider\Model\Config\Source\Template;
use Mageplaza\BannerSlider\Model\Config\Source\Type;

class Banner extends \Mageplaza\BannerSlider\Block\Adminhtml\Banner\Edit\Tab\Banner {

	/**
	 * Type options
	 *
	 * @var Type
	 */
	protected $typeOptions;

	/**
	 * Template options
	 *
	 * @var Template
	 */
	protected $template;
	/**
	 * @var Store
	 */
	protected $_systemStore;
	/**
	 * Status options
	 *
	 * @var Enabledisable
	 */
	protected $statusOptions;

	/**
	 * @var HelperImage
	 */
	protected $imageHelper;

	/**
	 * @var FieldFactory
	 */
	protected $_fieldFactory;

	/**
	 * @var DataObject
	 */
	protected $_objectConverter;

	/**
	 * @var WysiwygConfig
	 */
	protected $_wysiwygConfig;

	/**
	 * Banner constructor.
	 *
	 * @param Type $typeOptions
	 * @param Template $template
	 * @param Enabledisable $statusOptions
	 * @param Context $context
	 * @param Registry $registry
	 * @param FormFactory $formFactory
	 * @param HelperImage $imageHelper
	 * @param FieldFactory $fieldFactory
	 * @param DataObject $objectConverter
	 * @param WysiwygConfig $wysiwygConfig
	 * @param array $data
	 */
	public function __construct(
		Type $typeOptions,
		Template $template,
		Enabledisable $statusOptions,
		Context $context,
		Registry $registry,
		FormFactory $formFactory,
		HelperImage $imageHelper,
		FieldFactory $fieldFactory,
		DataObject $objectConverter,
		WysiwygConfig $wysiwygConfig,
		Store $systemStore,
		array $data = []
	) {
		$this->typeOptions = $typeOptions;
		$this->template = $template;
		$this->statusOptions = $statusOptions;
		$this->imageHelper = $imageHelper;
		$this->_fieldFactory = $fieldFactory;
		$this->_objectConverter = $objectConverter;
		$this->_systemStore = $systemStore;
		$this->_wysiwygConfig = $wysiwygConfig;

		parent::__construct($typeOptions, $template, $statusOptions, $context, $registry, $formFactory, $imageHelper, $fieldFactory, $objectConverter, $systemStore, $wysiwygConfig, $data);
	}

	/**
	 * @return Generic
	 * @throws LocalizedException
	 */
	protected function _prepareForm() {
		/** @var \Mageplaza\BannerSlider\Model\Banner $banner */
		$banner = $this->_coreRegistry->registry('mpbannerslider_banner');
		$form = $this->_formFactory->create();
		$form->setHtmlIdPrefix('banner_');
		$form->setFieldNameSuffix('banner');
		$fieldset = $form->addFieldset('base_fieldset', [
			'legend' => __('Banner Information'),
			'class' => 'fieldset-wide',
		]);

		if ($banner->getId()) {
			$fieldset->addField(
				'banner_id',
				'hidden',
				['name' => 'banner_id']
			);
		}

		$fieldset->addField('name', 'text', [
			'name' => 'name',
			'label' => __('Name'),
			'title' => __('Name'),
			'required' => true,
		]);

		$fieldset->addField('status', 'select', [
			'name' => 'status',
			'label' => __('Status'),
			'title' => __('Status'),
			'values' => $this->statusOptions->toOptionArray(),
		]);
		$fieldset->addField('sortorder', 'text', [
			'name' => 'sortorder',
			'label' => __('Position 123'),
			'title' => __('Position'),
			'required' => false,
		]);
		$fieldset->addField('banner_type', 'select', [
			'name' => 'banner_type',
			'label' => __('Banner Type111'),
			'title' => __('Banner Type111'),
			'values' => [1 => 'Slider', 2 => 'New Arrival'],
		]);
		/** @var RendererInterface $rendererBlock */
		//$rendererBlock = $this->getLayout()->createBlock(Element::class);
		$fieldset->addField('store_ids', 'select', [
			'name' => 'store_ids',
			'label' => __('Store Views'),
			'title' => __('Store Views'),
			'required' => true,
			'values' => $this->_systemStore->getStoreValuesForForm(false, true),
		]);
		$typeBanner = $fieldset->addField('type', 'select', [
			'name' => 'type',
			'label' => __('Type'),
			'title' => __('Type'),
			'values' => $this->typeOptions->toOptionArray(),
		]);

		$uploadBanner = $fieldset->addField('image', BannerImage::class, [
			'name' => 'image',
			'label' => __('Upload Image'),
			'title' => __('Upload Image'),
			'path' => $this->imageHelper->getBaseMediaPath(HelperImage::TEMPLATE_MEDIA_TYPE_BANNER),
		]);

		$titleBanner = $fieldset->addField('title', 'text', [
			'name' => 'title',
			'label' => __('Banner title'),
			'title' => __('Banner title'),
		]);

		$urlBanner = $fieldset->addField('url_banner', 'text', [
			'name' => 'url_banner',
			'label' => __('Url'),
			'title' => __('Url'),
			'class' => 'validate-url validate-no-html-tags',
		]);

		$newTab = $fieldset->addField('newtab', 'select', [
			'name' => 'newtab',
			'label' => __('Open new tab after click'),
			'title' => __('Open new tab after click'),
			'values' => $this->statusOptions->toOptionArray(),
			'note' => __('Automatically open new tab after clicking on the banner'),

		]);

		if (!$banner->getId()) {
			$defaultImage = array_values(Data::jsonDecode($this->template->getImageUrls()))[0];
			$demoTemplate = $fieldset->addField('default_template', 'select', [
				'name' => 'default_template',
				'label' => __('Demo template'),
				'title' => __('Demo template'),
				'values' => $this->template->toOptionArray(),
				'note' => '<img src="' . $defaultImage . '" alt="demo"  class="article_image" id="mp-demo-image">',
			]);

			$insertVariableButton = $this->getLayout()->createBlock(Button::class, '', [
				'data' => [
					'type' => 'button',
					'label' => __('Load Template'),
				],
			]);
			$insertButton = $fieldset->addField('load_template', 'note', [
				'text' => $insertVariableButton->toHtml(),
				'label' => '',
			]);
		}

		$content = $fieldset->addField('content', 'editor', [
			'name' => 'content',
			'required' => false,
			'config' => $this->_wysiwygConfig->getConfig([
				'hidden' => true,
				'add_variables' => false,
				'add_widgets' => false,
				'add_directives' => true,
			]),
		]);

		$fieldset->addField('sliders_ids', Slider::class, [
			'name' => 'sliders_ids',
			'label' => __('Sliders'),
			'title' => __('Sliders'),
		]);
		if (!$banner->getSlidersIds()) {
			$banner->setSlidersIds($banner->getSliderIds());
		}

		$bannerData = $this->_session->getData('mpbannerslider_banner_data', true);
		if ($bannerData) {
			$banner->addData($bannerData);
		} else {
			if (!$banner->getId()) {
				$banner->addData($banner->getDefaultValues());
			}
		}

		$dependencies = $this->getLayout()->createBlock(Dependence::class)
			->addFieldMap($typeBanner->getHtmlId(), $typeBanner->getName())
			->addFieldMap($urlBanner->getHtmlId(), $urlBanner->getName())
			->addFieldMap($uploadBanner->getHtmlId(), $uploadBanner->getName())
			->addFieldMap($titleBanner->getHtmlId(), $titleBanner->getName())
			->addFieldMap($newTab->getHtmlId(), $newTab->getName())
			->addFieldMap($content->getHtmlId(), $content->getName())
			->addFieldDependence($urlBanner->getName(), $typeBanner->getName(), '0')
			->addFieldDependence($uploadBanner->getName(), $typeBanner->getName(), '0')
			->addFieldDependence($titleBanner->getName(), $typeBanner->getName(), '0')
			->addFieldDependence($newTab->getName(), $typeBanner->getName(), '0')
			->addFieldDependence($content->getName(), $typeBanner->getName(), '1');

		if (!$banner->getId()) {
			$dependencies->addFieldMap($demoTemplate->getHtmlId(), $demoTemplate->getName())
				->addFieldMap($insertButton->getHtmlId(), $insertButton->getName())
				->addFieldDependence($demoTemplate->getName(), $typeBanner->getName(), '1')
				->addFieldDependence($insertButton->getName(), $typeBanner->getName(), '1');
		}

		// define field dependencies
		$this->setChild('form_after', $dependencies);

		$form->addValues($banner->getData());
		$this->setForm($form);

		return Generic::_prepareForm();
	}

}
