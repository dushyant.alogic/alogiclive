<?php
/**
 * @category   Alogic
 * @package    Alogic_Resellers
 * @author     dushyant.joshi@alogic.co
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Alogic\Resellers\Block;

use Alogic\Resellers\Model\ResellersFactory;
use Magento\Framework\View\Element\Template\Context;

/**
 * Resellers List block
 */
class ResellersListData extends \Magento\Framework\View\Element\Template {
	/**
	 * @var Resellers
	 */
	protected $_resellers;
	private $scopeConfig;
	private $countryFactory;
	public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Directory\Model\CountryFactory $countryFactory,
		Context $context,
		ResellersFactory $resellers
	) {
		$this->_resellers = $resellers;
		$this->scopeConfig = $scopeConfig;
		$this->countryFactory = $countryFactory;
		parent::__construct($context);
	}

	public function _prepareLayout() {
		$this->pageConfig->getTitle()->set(__('Alogic Resellers Module List Page'));

		if ($this->getResellersCollection()) {
			$pager = $this->getLayout()->createBlock(
				'Magento\Theme\Block\Html\Pager',
				'alogic.resellers.pager'
			)->setAvailableLimit(array(5 => 5, 10 => 10, 15 => 15))->setShowPerPage(true)->setCollection(
				$this->getResellersCollection()
			);
			$this->setChild('pager', $pager);
			$this->getResellersCollection()->load();
		}
		return parent::_prepareLayout();
	}
	public function getCountryname($countryCode) {
		$country = $this->countryFactory->create()->loadByCode($countryCode);
		return $country->getName();
	}
	public function getCountryCode() {
		return $this->scopeConfig->getValue('general/country/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

	}
	public function getMediaBaseUrl() {
		return $this->scopeConfig->getValue('web/unsecure/base_url');

	}
	public function getResellersCollection() {
		$page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
		//$pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;

		$resellers = $this->_resellers->create();
		$collection = $resellers->getCollection();
		$collection->addFieldToFilter('status', '1');
		//$resellers->setOrder('id','ASC');
		//$collection->setPageSize($pageSize);
		//$collection->setCurPage($page);

		return $collection;
	}

	public function getPagerHtml() {
		return $this->getChildHtml('pager');
	}
}