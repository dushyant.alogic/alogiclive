<?php
namespace Alogic\Dockingstation\Block;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Json\Helper\Data as jsonHelper;

/**
 * Class Main
 */
class Main extends \Magento\Catalog\Block\Product\AbstractProduct {
	function _prepareLayout() {}
	protected $productCollectionFactory;
	protected $categoryFactory;
	protected $eavConfig;
	protected $productRepository;
	/**
	 * @var \Magento\Framework\Json\Helper\Data
	 */
	protected $jsonHelper;
/**
 * @var \Magento\Framework\Url\Helper\Data
 */
	protected $urlHelper;
	public function __construct(
		\Magento\Catalog\Block\Product\Context $context,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Eav\Model\Config $eavConfig,
		jsonHelper $jsonHelper,
		\Magento\Framework\Url\Helper\Data $urlHelper,
		ProductRepositoryInterface $productRepository,
		array $data = []
	) {
		$this->productCollectionFactory = $productCollectionFactory;
		$this->categoryFactory = $categoryFactory;
		$this->eavConfig = $eavConfig;
		$this->urlHelper = $urlHelper;
		$this->jsonHelper = $jsonHelper;
		$this->productRepository = $productRepository;
		parent::__construct($context, $data);
	}
	public function getProductById() {

		try
		{
			$data = explode('/', $this->getData('id_path'));
			$productId = $data[1] ?? 0;
			$product = $this->productRepository->getById($productId);
			$this->setData('product', $product);
			return $product;
		} catch (NoSuchEntityException $e) {

			return 'Data not found';
		}

	}
	public function getOptions() {
		$_animationType = true;

		$respons = array(
			'updateCartUrl' => $this->getUrl('ajaxcart/index/updatecart'),
			'redirectCartUrl' => $this->getUrl('checkout/cart'),
			'animationType' => $_animationType,
		);
		return $this->jsonHelper->jsonEncode($respons);
	}
	public function getAddToCartPostParams(\Magento\Catalog\Model\Product $product) {
		$url = $this->getAddToCartUrl($product);
		return [
			'action' => $url,
			'data' => [
				'product' => $product->getEntityId(),
				\Magento\Framework\App\ActionInterface::PARAM_NAME_URL_ENCODED =>
				$this->urlHelper->getEncodedUrl($url),
			],
		];
	}
	public function getProductCollection() {

		$storeid = 1;
		$post = $this->getRequest()->getParams();
		/*echo "<pre>";
			print_r($post);
		*/
		$collection = $this->productCollectionFactory->create();
		$collection->addAttributeToSelect('is_docking_station');
		foreach ($post as $attribute_code => $value) {
			if (is_array($value)) {
				$collection->addAttributeToFilter($attribute_code, array('in' => $value));
			} else {
				$collection->addAttributeToFilter($attribute_code, array('eq' => $value));
			}
		}
		$collection->addAttributeToFilter('is_docking_station', array('eq' => 1));
		$collection->addAttributeToFilter('type_id', array('eq' => 'simple'));
/*		echo $collection->getSelect();
echo $collection->count();
die();
//$collection->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
//$collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
//$collection->addAttributeToFilter('status', array('eq' => 1));
if (isset($resolution_type)) {
$collection->addFieldToFilter('resolution_type', array('eq' => $this->eavConfig->getAttribute('catalog_product', 'resolution_type')->getSource()->getOptionId($resolution_type)));
//$collection->addAttributeToFilter('resolution_type', array('eq' => $resolution_type));
}

//$collection->addStoreFilter($storeid);*/

		return $collection;
	}

}

