<?php
namespace Alogic\Dockingstation\Controller\Search;
class Index extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}

	public function execute() {
		$this->resultPage = $this->resultPageFactory->create();
		$this->resultPage->getConfig()->getTitle()->set((__('Docking Station Finder: ALOGIC')));
		return $this->resultPage;
		//return $this->resultPageFactory->create();
	}
}

