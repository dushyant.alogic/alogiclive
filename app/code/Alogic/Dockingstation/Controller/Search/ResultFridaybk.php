<?php
namespace Alogic\Dockingstation\Controller\Search;
use Magento\Framework\Controller\Result\JsonFactory;

class Result extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;
	protected $_resultJsonFactory;
	protected $productCollectionFactory;
	protected $_storeManager;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		JsonFactory $resultJsonFactory) {
		$this->_resultJsonFactory = $resultJsonFactory;
		$this->productCollectionFactory = $productCollectionFactory;
		$this->resultPageFactory = $resultPageFactory;
		$this->_storeManager = $storeManager;
		parent::__construct($context);
	}

	public function execute() {
		$result = $this->_resultJsonFactory->create();
		$resultPage = $this->resultPageFactory->create();

		$productCollection = $this->getProductCollection();
		$data = array('collection' => $productCollection);

		$block = $resultPage->getLayout()
			->createBlock('Alogic\Dockingstation\Block\Main')
			->setTemplate('results.phtml')
			->setData('data', $data)
			->toHtml();

		$result->setData(['output' => $block]);
		return $result;

		// return $this->resultPageFactory->create();
	}
	public function getProductCollection() {

		$storeid = $this->getStoreId();
		$post = $this->getRequest()->getParams();
		$postData = [];
		foreach ($post as $key => $value) {
			if ($value == 'select') {
				continue;
			}
			$postData[$key] = $value;
		}

		$collection = $this->productCollectionFactory->create();
		$collection->addAttributeToSelect('*');
		$collection->addStoreFilter($storeid);
		$filter = [];
		$filterAttr = [];
		foreach ($postData as $attribute_code => $value) {

			if (is_array($value)) {

				if ($attribute_code == 'filter_video_outputs' || $attribute_code == 'additional_feature' || $attribute_code == 'filter_operating_sys' || $attribute_code == 'display_types') {
					foreach ($value as $key => $value1) { 
						if ($value1 == 'select') {
                                continue;
                        }
						$filter = [
							'attribute' => $attribute_code,
							'finset' => $value1,
						];
						if (count($filter) > 0) {
							//	$collection->addAttributeToFilter($filter);
						}
						$collection->addFieldToFilter($attribute_code, array("finset" => array($value1)));

					}} else {
					foreach ($value as $val) {
						if ($val != 'select') {
							continue;
						}
					}

					if (count($value) == 1 && in_array('select', $value)) {
						continue;
					}
					$collection->addAttributeToFilter($attribute_code, array('in' => $value));
				}

			} else {

				$collection->addAttributeToFilter($attribute_code, array('eq' => $value));
			}
		}

		$collection->addAttributeToFilter('type_id', array('eq' => 'simple'));

		return $collection;
	}
	/**
	 * Get store identifier
	 *
	 * @return  int
	 */
	public function getStoreId() {
		return $this->_storeManager->getStore()->getId();
	}

}

