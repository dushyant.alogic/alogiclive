/*global alert*/
define(
    [
        'Magento_Checkout/js/view/summary/abstract-total'        
    ],
    function (Component) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Alogic_Customtheme/checkout/cart/customshipping'
            },
            
            getValue: function() {
                
                return "Calculated at checkout";
            }
        });
    }
);