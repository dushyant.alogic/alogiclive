require(['jquery'], function ($) {
		$(document).ready(function () {

			$("#showPane1").click(function (e) {
				e.preventDefault();
				$(this).addClass('highlight');
				$('#showPane2').removeClass('highlight');
				$("#pane1").hide(500);
				$("#swapimage1").hide(500);
				$("#pane2").show(500);
				$("#swapimage2").show(500);


			});

			$("#showPane2").click(function (e) {
				e.preventDefault();
				$(this).addClass('highlight');
				$('#showPane1').removeClass('highlight');
				$("#pane2").hide(500);
				$("#swapimage2").hide(500);
				$("#pane1").show(500);
				$("#swapimage1").show(500);


			});
			$("#showPane11").click(function (e) {
				e.preventDefault();
				$(this).addClass('highlight');
				$('#showPane21').removeClass('highlight');

				$("#swapimage11").hide(500);

				$("#swapimage21").show(500);


			});

			$("#showPane21").click(function (e) {
				e.preventDefault();
				$(this).addClass('highlight');
				$('#showPane11').removeClass('highlight');

				$("#swapimage21").hide(500);

				$("#swapimage11").show(500);


			});

		});
	});
var observer = new IntersectionObserver(function (entries) {
	if (entries[0]['isIntersecting'] === true) {
		if (entries[0]['intersectionRatio'] === 1) {

			document.getElementById("leftText").classList.add("animate");
			document.getElementById("leftText").classList.add("fadeInLeft");
			document.getElementById("leftText").classList.add("one");

			document.getElementById("rightText").classList.add("animate");
			document.getElementById("rightText").classList.add("fadeInRight");
			document.getElementById("rightText").classList.add("one");
		}

	} else {

		document.getElementById("leftText").classList.remove("animate");
		document.getElementById("leftText").classList.remove("fadeInLeft");
		document.getElementById("leftText").classList.remove("one");

		document.getElementById("rightText").classList.remove("animate");
		document.getElementById("rightText").classList.remove("fadeInRight");
		document.getElementById("rightText").classList.remove("one");
	}
}, {
	threshold: [0, 0.5, 1]
});
var mq = window.matchMedia("(min-width: 570px)");
if (mq.matches) {
	observer.observe(document.querySelector(".animText"));
} else {
	document.getElementById("rightText").style.visibility = 'visible';
	document.getElementById("leftText").style.visibility = 'visible';
}
