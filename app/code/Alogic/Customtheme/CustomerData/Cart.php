<?php

namespace Alogic\Customtheme\CustomerData;

use Magento\Checkout\CustomerData\ItemPoolInterface;

/**
 * Cart source
 */
class Cart extends \Magento\Checkout\CustomerData\Cart {

	/**
	 * @var \Magento\Customer\Model\Session
	 */
	protected $checkoutSession;

	/**
	 * @var \Magento\Checkout\Model\Cart
	 */
	protected $checkoutCart;

	/**
	 * @var \Magento\Catalog\Model\ResourceModel\Url
	 */
	protected $catalogUrl;

	/**
	 * @var \Magento\Quote\Model\Quote|null
	 */
	protected $quote = null;

	/**
	 * @var \Magento\Checkout\Helper\Data
	 */
	protected $checkoutHelper;

	/**
	 * @var \Magento\Checkout\CustomerData\ItemPoolInterface
	 */
	protected $itemPoolInterface;

	/**
	 * @var int|float
	 */
	protected $summeryCount;

	/**
	 * @var \Magento\Framework\View\LayoutInterface
	 */
	protected $layout;
	/**
	 * @var \Magento\Store\Model\StoreManagerInterface
	 */
	protected $storeManager;
	/**
	 * @var \Magento\SalesRule\Model\RuleFactory
	 */
	protected $ruleFactory;
	/**
	 * @var \Magento\Catalog\Model\ProductFactory
	 */
	protected $productFactory;

	/**
	 * @param \Magento\Checkout\Model\Session $checkoutSession
	 * @param \Magento\Catalog\Model\ResourceModel\Url $catalogUrl
	 * @param \Magento\Checkout\Model\Cart $checkoutCart
	 * @param \Magento\Checkout\Helper\Data $checkoutHelper
	 * @param \Magento\Checkout\CustomerData\ItemPoolInterface $itemPoolInterface
	 * @param \Magento\Framework\View\LayoutInterface $layout
	 * @param array $data
	 * @codeCoverageIgnore
	 */
	public function __construct(
		\Magento\Checkout\Model\Session $checkoutSession,
		\Magento\Catalog\Model\ResourceModel\Url $catalogUrl,
		\Magento\Checkout\Model\Cart $checkoutCart,
		\Magento\Checkout\Helper\Data $checkoutHelper,
		ItemPoolInterface $itemPoolInterface,
		\Magento\Framework\View\LayoutInterface $layout,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\SalesRule\Model\RuleFactory $ruleFactory,
		\Magento\Catalog\Model\ProductFactory $productFactory,
		array $data = []
	) {
		$this->checkoutSession = $checkoutSession;
		$this->catalogUrl = $catalogUrl;
		$this->checkoutCart = $checkoutCart;
		$this->checkoutHelper = $checkoutHelper;
		$this->itemPoolInterface = $itemPoolInterface;
		$this->layout = $layout;
		$this->storeManager = $storeManager;
		$this->ruleFactory = $ruleFactory;
		$this->productFactory = $productFactory;
		parent::__construct($checkoutSession, $catalogUrl, $checkoutCart, $checkoutHelper, $itemPoolInterface, $layout, $data);
	}
	/**
	 * {@inheritdoc}
	 */
	public function getSectionData() {

		$totals = $this->getQuote()->getTotals();
		$subtotalAmount = $totals['subtotal']->getValue();
		$quote_items = $this->getAllQuoteItems();
		$action_available = true;

		return [
			'summary_count' => $this->getSummaryCount(),
			'subtotalAmount' => $subtotalAmount,
			'subtotal' => isset($totals['subtotal'])
			? $this->checkoutHelper->formatPrice($subtotalAmount)
			: 0,
			'possible_onepage_checkout' => $this->isPossibleOnepageCheckout(),
			'items' => $this->getRecentItems(),
			'extra_actions' => $this->layout->createBlock(\Magento\Catalog\Block\ShortcutButtons::class)->toHtml(),
			'isGuestCheckoutAllowed' => $this->isGuestCheckoutAllowed(),
			'website_id' => $this->getQuote()->getStore()->getWebsiteId(),
			'storeId' => $this->getQuote()->getStore()->getStoreId(),
			'ispromotion' => $action_available,
		];
	}
}