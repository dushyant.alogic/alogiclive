<?php
namespace Alogic\Customtheme\Controller\Geolocation;
use Magento\Checkout\Model\Session;
use Magento\Framework\Controller\ResultFactory;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Amasty\Geoip\Model\Geolocation;

class Index extends \Magento\Framework\App\Action\Action
{
    const COOKIE_NAME = "geopupupcoocookie";
    protected $_cookieManager;
    protected $_cookieMetadataFactory;
    private $storeManager;
    private $geolocation;

    protected $objectManager;
    protected $countryFactory;
    protected $productRepository;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        ResultFactory $resultFactory,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,

        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        Geolocation $geolocation,
        \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        $this->_cookieManager = $cookieManager;

        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->storeManager = $storeManager;

        $this->_countryFactory = $countryFactory;
        $this->objectManager = $objectManager;
        $this->geolocation = $geolocation;
        $this->resultFactory = $resultFactory;
        parent::__construct($context);
    }
    public function getCurrentStore(){
        return $this->storeManager->getStore();
    }
    public function getCountryname($countryCode)
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
    public function getGeoLocationData()
    {

        //$visitorIp = $this->getVisitorIp();
	$visitorIp = $this->getUserIpAddr();
        $geolocationData = $this->geolocation->locate($visitorIp);
        return $geolocationData;
    }


function getUserIpAddr(){
$IParray=array_values(array_filter(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])));
return reset($IParray);

        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
	    //$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    $IParray=array_values(array_filter(explode(',',$_SERVER['HTTP_X_FORWARDED_FOR'])));
            $ip =  reset($IParray);

        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    function getVisitorIp()
    {
        $remoteAddress = $this->objectManager->create(
            "Magento\Framework\HTTP\PhpEnvironment\RemoteAddress"
        );
        return $remoteAddress->getRemoteAddress();
    }
    public function getCountryCode()
    {
        $code = $this->geoIpService->getCountry();
        if ($code == "au") {
            $code = "";
        }
        return $code;
    }

    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        /*$block->setData('quote_items', [1,2,3,4,5]);*/
        $geoData = $this->getGeoLocationData()->getCountry();
        $countryCode = $this->getGeoLocationData()->getCountry();
        $countryName = $this->getCountryname($countryCode);


                $currentStoreCode = $this->getCurrentStore()->getCode();
                $baseurl = $this->getCurrentStore()->getBaseUrl();
                $showPopup = true;
                $defaultCountryCode = '';
                
                if(!in_array(strtolower($countryCode), ['au','hk','my','nz','ph','sg','th','in','ae','dk','fi','fr','de','ie','no','se','uk','ca','us','gb'])){
                    $showPopup = false;
                    $defaultCountryCode = 'au';
                }
                if($countryCode == 'GB'){
                    $countryCode = 'UK';
                }
                if(strtolower($countryCode) == $currentStoreCode){
                    $showPopup = false;   
                }
                if(strtolower($countryCode) == 'au' && $currentStoreCode == 'default' || $currentStoreCode == 'business'){
                    $showPopup = false;   
                }

               
                $popupHtml = '';
                $popupHtml .= '<div style="padding:4%;text-align: center;">';
        $popupHtml .= '<h2>WELCOME to ALOGIC!</h2>';
        $popupHtml .= '<h3> We\'ve noticed you are in '. $countryName .'</h3>';


                $popupHtml .= '<div>
            <img src="https://1097633070.nxcli.io/media/flags/'.strtolower($countryCode).'-popup.png">
        </div>';    
        if($defaultCountryCode !='' || $countryCode == 'AU'){
            $popupHtml .= '<p><a href="https://1097633070.nxcli.io/">Visit ALOGIC  AU</a></p>';
        }elseif($countryCode == 'IN'){    
            $popupHtml .= '<p><a href="https://alogic.in/">Visit ALOGIC '. $countryCode. '</a></p>';
        }else{    
            $popupHtml .= '<p><a href="https://'.strtolower($countryCode).'.alogic.co/">Visit ALOGIC '. $countryCode. '</a></p>';
        }
        $popupHtml .= '<a href="javascript:;" id="close-geopopup">Stay on this site</a>';
        $popupHtml .= '</div>';

        $resultPage->setData([
            "message" => "Invalid Data",
            "suceess" => true,
            "counry_code" => $countryCode,
            "counry_name" => $countryName,
            "popupHtml" => $popupHtml,
            "showPopup" => $showPopup
        ]);
        return $resultPage;
    }
}
