<?php
/**
 * Webkul Hello CustomPrice Observer
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 */
namespace Alogic\Customtheme\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;

class CustomPrice implements ObserverInterface {
	protected $productrepository;
	protected $cart;
	protected $formKey;
	protected $product;

	private $storeManager;
	public function __construct(
		\Magento\Catalog\Api\ProductRepositoryInterface $productrepository,
		\Magento\Checkout\Model\Cart $cart,
		Product $product,
		\Magento\Framework\Data\Form\FormKey $formKey,
		StoreManagerInterface $storeManager
	) {
		$this->cart = $cart;
		$this->product = $product;
		$this->productrepository = $productrepository;
		$this->formKey = $formKey;
		$this->storeManager = $storeManager;
	}
	public function execute(\Magento\Framework\Event\Observer $observer) {

		$storeId = $this->storeManager->getStore()->getId();

		$event = $observer->getEvent();

		/** @var Product $product */
		$item = $event->getData('product');
		$sku = $item->getSku();
		/*$item = ($item->getParentItem() ? $item->getParentItem() : $item);	*/

		$writer = new \Zend_Log_Writer_Stream(BP . '/var/log/CustomPrice.log');
		$logger = new \Zend_Log();
		$logger->addWriter($writer);

		//if ($sku == '27F34KCPD' && $storeId == 35) {
		if (false) {
			$items = $this->cart->getQuote()->getItems();
			foreach ($items as  $item) {
				$itemId = $item->getItemId();				
				if($item->getSku() == 'IUWA09'){
					$this->cart->getQuote()->removeItem($itemId)->save();
				}	
			}
			$monitor_product = $this->productrepository->get('IUWA09');
			$productId = $monitor_product->getId();
			$params = array(
				'form_key' => $this->formKey->getFormKey(),
				'product' => $productId,
				'selected_configurable_option' => '',
				'related_product' => '',
				'qty' => 1,
			);

			$this->cart->addProduct($monitor_product, $params);
			$this->cart->save();
			$couponCode = '9to5ALG';
			if (true) {
				$this->cart->getQuote()->setCouponCode($couponCode)->collectTotals()->save();
			} else {
				$this->cart->getQuote()->setCouponCode('')->collectTotals()->save();
			}
		}
	}

}