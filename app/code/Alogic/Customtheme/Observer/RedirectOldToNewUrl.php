<?php

namespace Alogic\Customtheme\Observer;

class RedirectOldToNewUrl implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $_responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;
    protected $csv;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    public function __construct(
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\File\Csv $csv,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_storeManager = $storeManagerInterface;
        $this->_responseFactory = $responseFactory;
        $this->_response = $response;
        $this->_url = $url;
        $this->csv = $csv;
        $this->_productRepository = $productRepository;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $pathInfo = $observer
            ->getEvent()
            ->getRequest()
            ->getPathInfo();

        $request = $observer->getEvent()->getRequest();
        if ($this->_storeManager->getStore()->getId() == 35 || $this->_storeManager->getStore()->getId() == 42) {
            $writer = new \Zend_Log_Writer_Stream(
                BP . "/var/log/CustomUrls.log"
            );
            $logger = new \Zend_Log();
            $logger->addWriter($writer);
            $requestUrl = $request->getUriString();
            $logger->info(print_r($requestUrl, true));
            $csvData = $this->csv->getData(
                "/home/a387331c/public_html/pub/media/urls2_main.csv"
            );
            $record = $this->searchForId($requestUrl, $csvData);
            /*echo "<pre>";
            print_r($record);*/
            $csvData = array_slice($csvData, 1);
             /*echo "<pre>";
             print_r($csvData);
             die();*/
            if (isset($record[2]) && !is_null($record[2]) && !empty(($record[2]))) {
                
                $CustomRedirectionUrl = $record[2];                
                $this->_responseFactory
                    ->create()
                    ->setRedirect($CustomRedirectionUrl)
                    ->sendResponse();
                    exit();
            }else{
                /*$CustomRedirectionUrl = 'https://us.alogic.co/';                
                $this->_responseFactory
                    ->create()
                    ->setRedirect($CustomRedirectionUrl)
                    ->sendResponse();
                    exit();*/
            }
        }
    }

    function searchForId($id, $array)
    {
        foreach ($array as $key => $val) {
            //echo $val[1] . "<br />";
            if(isset($val[1])){
               if ($val[1] === $id) {
                return $array[$key];
                } 
            }
            
        }
        return null;
    }
}
