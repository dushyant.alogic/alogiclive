<?php
namespace Alogic\Customtheme\Observer\Sales;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;

class SalesOrderInvoicePay implements ObserverInterface {
	protected $zendClient;
/**
 * @var StoreManagerInterface
 */
	private $storeManager;
	public function __construct(
		\Zend\Http\Client $zendClient,
		StoreManagerInterface $storeManager

	) {
		$this->zendClient = $zendClient;
		$this->storeManager = $storeManager;

	}

/**
 * @param EventObserver $observer
 * @return $this
 */
	public function execute(EventObserver $observer) {
		$shipment = $observer->getEvent()->getShipment();
		/** @var \Magento\Sales\Model\Order $order */
		$order = $shipment->getOrder();
		$invoiceList = $order->getInvoiceCollection();

		$store_code = $this->getStoreCodeById($order->getStoreId());
		$writer = new \Laminas\Log\Writer\Stream(BP . '/var/log/invoice.log');
		$logger = new \Laminas\Log\Logger();
		$logger->addWriter($writer);

		//$logger->info(print_r($invoiceList, true));
		//if (count($invoiceList->getItems()) > 1) {
		foreach ($invoiceList as $invoice) {
			$invoiceId = $invoice->getEntityId();
			$logger->info($invoiceId);
		}
		$url = 'https://alogic.co/rest/' . $store_code . '/V1/invoices/' . $invoiceId . '/emails';

		$logger->info($url);
		//}

		try
		{
			$this->zendClient->reset();
			$this->zendClient->setUri($url);
			$this->zendClient->setMethod(\Zend\Http\Request::METHOD_POST);
			$this->zendClient->setHeaders([
				'Content-Type' => 'application/json',
				'Accept' => 'application/json',
				'Authorization' => 'Bearer 4l2pf8yldjfm32skpghl1nk3m2oilsyv',
			]);
			/*$this->zendClient->setParameterPost([
				'yourparameter1' => 'yourvalue1',
			]);*/
			$logger->info("Sent");
			$this->zendClient->send();
			$response = $this->zendClient->getResponse();
			$logger->info(print_r($response->getBody(), true));
		} catch (\Zend\Http\Exception\RuntimeException $runtimeException) {
			echo $runtimeException->getMessage();
		}

	}
	/**
	 * Get Store code by id
	 *
	 * @param int $id
	 *
	 * @return string|null
	 */
	public function getStoreCodeById(int $id) {
		try {
			$storeData = $this->storeManager->getStore($id);
			$storeCode = (string) $storeData->getCode();
		} catch (LocalizedException $localizedException) {
			$storeCode = null;
			$this->logger->error($localizedException->getMessage());
		}
		return $storeCode;
	}
}
