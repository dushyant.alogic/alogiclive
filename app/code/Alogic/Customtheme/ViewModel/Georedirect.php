<?php
namespace Alogic\Customtheme\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Amasty\Geoip\Model\Geolocation;

class Georedirect implements ArgumentInterface
{
    const COOKIE_NAME = 'storeredirectcookie';
    protected $_cookieManager;
    protected $_cookieMetadataFactory;
    private $storeManager;    
    protected $_state;
    private $geolocation;
    protected $objectManager;
    protected $countryFactory;
    public function __construct(
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,        
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        Geolocation $geolocation
    ) {
        $this->_cookieManager = $cookieManager;        
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->storeManager = $storeManager;
        $this->geolocation = $geolocation;
        $this->_countryFactory = $countryFactory;
        $this->objectManager = $objectManager;

    }
    public function getCountryname($countryCode)
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
    public function getGeoLocationData()
    {
        $visitorIp = $this->getVisitorIp();
        $geolocationData = $this->geolocation->locate($visitorIp);
        return $geolocationData;        
    }
    function getVisitorIp() {       
        $remoteAddress = $this->objectManager->create('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
        return $remoteAddress->getRemoteAddress();
    }
 /*   public function getCountryCode()
    {
        $code = $this->geoIpService->getCountry();
        if($code == 'au')
        {
            $code = '';
        }
        return $code;
    }*/
    public function getCurrentStore(){
        return $this->storeManager->getStore();
    }
    public function setCustomCookie($countryCode = null) {
        $publicCookieMetadata = $this->_cookieMetadataFactory->createPublicCookieMetadata();
        $publicCookieMetadata->setDurationOneYear();
        $publicCookieMetadata->setPath('/');
        $publicCookieMetadata->setHttpOnly(false);

        return $this->_cookieManager->setPublicCookie(
            self::COOKIE_NAME,
            $countryCode,
            $publicCookieMetadata
        );
    }

    /** Get Custom Cookie using */
    public function getCustomCookie() {
        return $this->_cookieManager->getCookie(
            self::COOKIE_NAME
        );
    }
}