<?php
/**
 * Created By : Rohan Hapani
 */
namespace Alogic\Customtheme\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {
	public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
		$installer = $setup;
		$installer->startSetup();
		$tableName = $installer->getTable('mgs_portfolio_item_store'); // Get mgs_portfolio_item_store table
		// Check if the table already exists
		if ($installer->getConnection()->isTableExists($tableName) != true) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('mgs_portfolio_item_store')
			)->addColumn(
				'id',
				Table::TYPE_INTEGER,
				null,
				['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
				'Portfolio Store Id'
			)->addColumn(
				'portfolio_id',
				Table::TYPE_INTEGER,
				null,
				['unsigned' => true, 'nullable' => false],
				'Portfolio Id'
			)->addColumn(
				'store_id',
				Table::TYPE_TEXT,
				null,
				['nullable' => true],
				'Store Id'
			);
		}
		$installer->endSetup();
	}
}