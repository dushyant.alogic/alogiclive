<?php
namespace Alogic\Customtheme\Block;
class Categories extends \Magento\Framework\View\Element\Template {
	protected $_categoryCollectionFactory;

	protected $_categoryHelper;
	protected $_storeManager;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,

		\Magento\Catalog\Helper\Category $categoryHelper,
		array $data = []
	) {
		$this->_categoryCollectionFactory = $categoryCollectionFactory;
		$this->_categoryHelper = $categoryHelper;
		$this->_storeManager = $storeManager;
		parent::__construct($context, $data);
	}

	public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false) {
		$collection = $this->_categoryCollectionFactory->create();
		$collection->addAttributeToSelect('*');

		// select only active categories
		if ($isActive) {
			$collection->addIsActiveFilter();
		}

		// select categories of certain level
		if ($level) {
			$collection->addLevelFilter($level);
		}

		// sort categories by some value
		if ($sortBy) {
			$collection->addOrderField($sortBy);
		}

		// set pagination
		if ($pageSize) {
			$collection->setPageSize($pageSize);
		}

		return $collection;
	}

	public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true) {
		return $this->_categoryHelper->getStoreCategories($sorted = false, $asCollection = false, $toLoad = true);
	}
	public function getAllStoreCategories() {

		$currentStore = $this->_storeManager->getStore();
		$storeCategories = $this->_categoryCollectionFactory->create()
			->addAttributeToSelect('*')
			->addAttributeToFilter('is_active', 1)
			->addLevelFilter(3)
			->setStore($currentStore);
		return $storeCategories;
	}
}