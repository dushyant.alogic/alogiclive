<?php
namespace Alogic\Customtheme\Block;

/**
 * Class Main
 */
class Main extends \Magento\Framework\View\Element\Template {

	/**
	 * Construct
	 *
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Magento\Customer\Model\Session $customerSession
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Api\AccountManagementInterface $accountManagement,
		\Magento\Customer\Model\Address\Config $addressConfig,
		\Magento\Customer\Model\Address\Mapper $addressMapper,
		\Magento\Sales\Model\OrderFactory $orderFactory,
		\Magento\Framework\App\Http\Context $httpContext,
		array $data = []
	) {
		$this->accountManagement = $accountManagement;
		$this->_addressConfig = $addressConfig;
		$this->addressMapper = $addressMapper;
		$this->customerSession = $customerSession;
		$this->_orderFactory = $orderFactory;
		parent::__construct($context, $data);

	}
	public function _prepareLayout() {

		return parent::_prepareLayout();
	}
	/**
	 * $customerId
	 */
	public function getDefaultShippingAddress($customerId) {
		try {
			$address = $this->accountManagement->getDefaultBillingAddress($customerId);
		} catch (NoSuchEntityException $e) {
			return __('You have not set a default shipping address.');
		}
		return $address;
	}

	/**
	 * $customerId
	 */
	public function getDefaultBillingAddress($customerId) {
		try {
			$address = $this->accountManagement->getDefaultBillingAddress($customerId);
		} catch (NoSuchEntityException $e) {
			return __('You have not set a default billing address.');
		}
		return $address;
	}
	/* Html Format */
	public function getDefaultShippingAddressHtml($address) {
		if ($address) {
			return $this->_getAddressHtml($address);
		} else {
			return __('You have not set a default Shipping address.');
		}
	}
	/* Html Format */
	public function getDefaultBillingAddressHtml($address) {
		if ($address) {
			return $this->_getAddressHtml($address);
		} else {
			return __('You have not set a default billing address.');
		}
	}

	/**
	 * Render an address as HTML and return the result
	 *
	 * @param AddressInterface $address
	 * @return string
	 */
	protected function _getAddressHtml($address) {
		/** @var \Magento\Customer\Block\Address\Renderer\RendererInterface $renderer */
		$renderer = $this->_addressConfig->getFormatByCode('html')->getRenderer();
		return $renderer->renderArray($this->addressMapper->toFlatArray($address));
	}
	public function getOrders() {
		$customerId = $this->customerSession->getCustomer()->getId();
		$this->orders = $this->_orderFactory->create()->getCollection()->addFieldToSelect(
			'*'
		)->addFieldToFilter(
			'customer_id',
			$customerId
		)->setOrder(
			'created_at',
			'desc'
		);
		return $this->orders;
	}
}
