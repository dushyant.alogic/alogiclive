<?php
namespace Alogic\Driversdownloads\Block;

use Alogic\Distributors\Model\DistributorFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ProductFactory;
use MageWorx\Downloads\Model\ResourceModel\Attachment\CollectionFactory;

/**
 * Class Main
 */
class Main extends \Magento\Framework\View\Element\Template {
	protected $_productCollectionFactory;
	protected $collectionFactory;
	protected $productRepository;
	protected $imageHelper;
	protected $productFactory;

	protected $_distributorFactory;

	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		CollectionFactory $collectionFactory,
		ProductRepositoryInterface $productRepository,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		Image $imageHelper,
		ProductFactory $productFactory,
		DistributorFactory $_distributorFactory,
		array $data = []
	) {
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->collectionFactory = $collectionFactory;
		$this->productRepository = $productRepository;
		$this->imageHelper = $imageHelper;
		$this->productFactory = $productFactory;
		$this->_distributorFactory = $_distributorFactory;
		parent::__construct($context, $data);
	}

	public function getProductCollection($params = []) {

		$productSkus = [];
		$collection = $this->_productCollectionFactory->create()->addAttributeToSelect('*');
		if (isset($params['pid'])) {
			$sku = $params['pid'];
			$productSkus[] = $sku;
			//$product = $this->loadMyProduct($sku);
			$collection->addAttributeToFilter('sku', $sku);

		}

		//echo "pid" . $product->getId();
		//$collection->setPageSize(100); // fetching only 3 products
		return $collection;
	}
	public function getProduct() {
		$result = $this->_distributorFactory->create();
		$collection = $result->getCollection()->addFieldToFilter('type', array('eq' => 2))->load();
		$params = $this->getRequest()->getParams();

		$sku = $params['pid'];
		return $product = $this->loadMyProduct($sku);
	}
	public function getProductImageUrl() {
		try
		{
			$product = $this->getProduct();
		} catch (NoSuchEntityException $e) {
			return 'Data not found';
		}
		$url = $this->imageHelper->init($product, 'product_base_image')->getUrl();
		return $url;
	}
	public function getDownloadCollection() {
		$params = $this->getRequest()->getParams();
		$items = [];
		$productCollection = $this->getProductCollection($params);

		foreach ($productCollection as $product) {

			$product = $this->loadMyProduct($product->getSku());

			$items['download_mac_driver_link'] = $product->getData('download_mac_driver_link');
			$items['download_windows_driver_link'] = $product->getData('download_windows_driver_link');
			$items['download_specsheet_2'] = $product->getData('download_specsheet_2');
			$items['download_specsheet'] = $product->getData('download_specsheet');
			$items['download_product_manual'] = $product->getData('download_product_manual');
			$items['download_driver_zip'] = $product->getData('download_driver_zip');
		}
		return $items;
	}
	public function getAttachmentCollection() {
		$params = $this->getRequest()->getParams();
		$items = [];
		$collection = $this->collectionFactory->create();
		$collection->getSelect()
			->joinLeft(
				['product_relation_table' => \MageWorx\Downloads\Model\ResourceModel\Attachment::PRODUCT_RELATION_TABLE],
				'main_table.attachment_id = product_relation_table.attachment_id',
				[]
			);

		if (isset($params['pid'])) {
			$sku = $params['pid'];
			$product = $this->loadMyProduct($sku);
			$collection->getSelect()->where('product_relation_table.product_id = ?', $product->getId());
		}
		$collection->getSelect()->group('main_table.attachment_id');
		//$collection->getSelect()->reset(\Zend_Db_Select::GROUP);
		//$collection->setPageSize(10);
		//die();
		foreach ($collection as $item) {
			$items['items'][] = $item->toArray([]);
		}

		return $items;
	}
	public function loadMyProduct($sku) {

		try
		{
			return $this->productRepository->get($sku);
		} catch (NoSuchEntityException $e) {

			return 'Data not found';
		}

	}
	function _prepareLayout() {
		$title = __('Drivers and downloads');

		$breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
		/*$breadcrumbsBlock->addCrumb(
				'home',
				[
					'label' => __('Home'),
					'title' => __('Go to Home Page'),
					'link' => $this->_storeManager->getStore()->getBaseUrl(),
				]
			);

		*/

		$this->pageConfig->getTitle()->set($title);
		return parent::_prepareLayout();
	}
}
