<?php
namespace Alogic\Driversdownloads\Controller\Index;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Index extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		ProductRepositoryInterface $productRepository,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->productRepository = $productRepository;
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}

	public function execute() {

		$sku = $this->getRequest()->getParam('pid');
		$resultLayout = $this->resultPageFactory->create();
		try {
			$product = $this->productRepository->get($sku);
		} catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
			$this->messageManager->addError(__('This product no longer exists.'));
			$this->_redirect('product-support');
			return;
		}

		/*try
			{

				$product = $this->productRepository->get($sku);
				if (!$model->getId()) {
					$this->messageManager->addError(__('This item no longer exists.'));
					$this->_redirect('driversdownloads/*');
					return;
				}

			} catch (NoSuchEntityException $e) {

				$this->messageManager->addError(__('This item no longer exists.'));
				$this->_redirect('driversdownloads/*');
				return;
		*/

		return $this->resultPageFactory->create();
	}
}
