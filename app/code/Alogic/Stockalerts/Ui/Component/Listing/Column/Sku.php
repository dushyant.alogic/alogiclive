<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_BannerSlider
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Alogic\Stockalerts\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Listing\Columns\Column;
use Mageplaza\BannerSlider\Helper\Data as bannerHelper;

/**
 * Class Banners
 * @package Mageplaza\BannerSlider\Ui\Component\Listing\Column
 */
class Sku extends Column {
	protected $_productRepository;

	/**
	 * Banners constructor.
	 *
	 * @param ContextInterface $context
	 * @param UiComponentFactory $uiComponentFactory
	 * @param bannerHelper $helperData
	 * @param array $components
	 * @param array $data
	 */
	public function __construct(
		ContextInterface $context,
		UiComponentFactory $uiComponentFactory,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		array $components = [],
		array $data = []
	) {
		$this->_productRepository = $productRepository;

		parent::__construct($context, $uiComponentFactory, $components, $data);
	}
	public function getProductById($id) {
		return $this->_productRepository->getById($id);
	}
	/**
	 * Prepare Data Source
	 *
	 * @param array $dataSource
	 *
	 * @return array
	 */
	public function prepareDataSource(array $dataSource) {
		if (isset($dataSource['data']['items'])) {
			foreach ($dataSource['data']['items'] as &$item) {
				if (isset($item['product_id'])) {
					$id = $item['product_id'];
					$data = $this->getProductById($id);
					$item[$this->getData('name')] = $data->getSku();
					//$item[$this->getData('name')] = ($data > 0) ? $data . '<span> banners </span>' : '<b>' . __("No banner added") . '</b>';
				}
			}
		}

		return $dataSource;
	}
}
