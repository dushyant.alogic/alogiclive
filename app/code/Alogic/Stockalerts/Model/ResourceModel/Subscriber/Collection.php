<?php
namespace Alogic\Stockalerts\Model\ResourceModel\Subscriber;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'alert_stock_id';
	protected $_eventPrefix = 'alogic_stockalerts_subscriber_collection';
	protected $_eventObject = 'stockalert_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Alogic\Stockalerts\Model\Subscriber', 'Alogic\Stockalerts\Model\ResourceModel\Subscriber');
	}

}

