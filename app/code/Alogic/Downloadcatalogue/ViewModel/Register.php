<?php

namespace Alogic\Downloadcatalogue\ViewModel;

use Magento\Framework\App\Http\Context;
use Magento\Framework\Session\SessionManagerInterface;

class Register implements \Magento\Framework\View\Element\Block\ArgumentInterface {

	protected $_httpContext;
	protected $_sessionManager;

	public function __construct(Context $httpContext, SessionManagerInterface $session) {
		$this->_httpContext = $httpContext;
		$this->_sessionManager = $session;
	}

	public function getSessionData($field) {
		return $this->_sessionManager->getData($field);
	}
	public function unsetSessionData($field) {
		return $this->_sessionManager->setData($field, '');
	}
}