<?php
namespace Alogic\Categorybanner\Block;

use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
use Magento\Catalog\Model\Category;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use \Magento\Cms\Model\Page;

/**
 * Class Main
 */
class Main extends \Amasty\Mage24Fix\Block\Theme\Html\Title {
//class Main extends \Magento\Theme\Block\Html\Title {
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */

	public function __construct(
		Context $context,
		Category $categoryModel,
		StoreManagerInterface $storemanager,
		Page $page,
		ProductRepositoryInterfaceFactory $productRepositoryFactory,
		Registry $registry,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		Http $request
	) {
//		parent::__construct($context, $scopeConfig);
		$this->_categoryModel = $categoryModel;
		$this->_storeManager = $storemanager;
		$this->_productRepositoryFactory = $productRepositoryFactory;
		$this->_page = $page;
		$this->_registry = $registry;
		$this->_request = $request;
	}

	public function getBannerImageUrl() {
		$url = false;
		$store = $this->_storeManager->getStore();
		echo $this->_request->getFullActionName();
		if ($this->_request->getFullActionName() == 'products_index_index') {
			$main_category = $this->_categoryModel->load(275);
			$image = $main_category->getCateLandingImg();

			if ($image) {
				if (is_string($image)) {
					$url = $this->_storeManager->getStore()->getBaseUrl(
						\Magento\Framework\UrlInterface::URL_TYPE_WEB
					) . $image;

					//$url = $image;
				} else {
					throw new \Magento\Framework\Exception\LocalizedException(
						__('Something went wrong while getting the image url.')
					);
				}
			}
			return $url;
		}
		if ($this->_request->getFullActionName() == 'accounthome_index_index') {
			return $url = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) . 'orderimage.png';
		}
		if ($this->_request->getFullActionName() == 'checkout_cart_index' || $this->_request->getFullActionName() == 'checkout_index_index' || $this->_request->getFullActionName() == 'onepagecheckout_index_index') {
			return $url = $this->_storeManager->getStore()->getBaseUrl(
				\Magento\Framework\UrlInterface::URL_TYPE_MEDIA
			) . 'shopping-cart-banner.jpg';
		}
		echo $this->_request->getFullActionName();

		if ($this->_request->getFullActionName() == 'catalog_category_view') {

			$_category = $this->_registry->registry('current_category');
			echo $image = $_category->getCateLandingImg();
			echo "===";
			if ($image) {
				if (is_string($image)) {
					echo $url = $this->_storeManager->getStore()->getBaseUrl(
						\Magento\Framework\UrlInterface::URL_TYPE_WEB
					) . $image;

					//$url = $image;
				} else {
					throw new \Magento\Framework\Exception\LocalizedException(
						__('Something went wrong while getting the image url.')
					);
				}
			}

		} elseif ($this->_request->getFullActionName() == 'catalog_product_view') {
			$params = $this->_request->getParams();

			if (isset($params['id'])) {
				$product = $this->_productRepositoryFactory->create()->getById($params['id']);

				if ($product->getProductBanner()) {
					$url = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getProductBanner();
				} else {
					$categoryIds = $product->getCategoryIds();
					if (count($categoryIds)) {
						foreach ($categoryIds as $catId) {
							$_category = $this->_categoryModel->load($catId);
							if ($_category->getCateLandingImg()) {
								$image = $_category->getCateLandingImg();

								if ($image) {
									if (is_string($image)) {
										$url = $this->_storeManager->getStore()->getBaseUrl(
											\Magento\Framework\UrlInterface::URL_TYPE_WEB
										) . $image;

										//$url = $image;
									} else {
										throw new \Magento\Framework\Exception\LocalizedException(
											__('Something went wrong while getting the image url.')
										);
									}
								}

							}

						}

					}
				}

			}
		} else {
			$cmspages = array('cms_index_index', 'cms_page_view');
			if (in_array($this->_request->getFullActionName(), $cmspages)) {
				//CMS page
				$page = $this->_page->getData();

				if (isset($page['cms_hero_image'])) {
					$url = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'cms/hero/image/' . $page['cms_hero_image'];
				}
			}
		}

		return $url;
	}
}
