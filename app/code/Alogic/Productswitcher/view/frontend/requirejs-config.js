var config = {
    config: {
        mixins: {
            'Magento_ConfigurableProduct/js/configurable': {
                'Alogic_Productswitcher/js/model/skuswitch': true
            },
            'Magento_Swatches/js/swatch-renderer': {
                'Alogic_Productswitcher/js/model/swatch-skuswitch': true
            }
        }
	}
};