    define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {
	'use strict';
 
    return function(targetModule){
 
        var reloadPrice = targetModule.prototype._reloadPrice;
        var reloadPriceWrapper = wrapper.wrap(reloadPrice, function(original){
        var result = original();
        var simpleSku = this.options.spConfig.skus[this.simpleProduct];
        var simpleBarcode = this.options.spConfig.barcodes[this.simpleProduct];
        var simpleName = this.options.spConfig.names[this.simpleProduct];
        
            if(simpleSku != '') {
                $('div.product-info-main .sku .value').html(simpleSku);
            }
            if(simpleBarcode != '') {
                $('div.product-info-main .part-no .value').html(simpleSku);
            }     
            
             if(simpleName != '') {
                $('div.product-info-main .product-name').html(simpleName);
            }        
            return result;
        });
        targetModule.prototype._reloadPrice = reloadPriceWrapper;
        return targetModule;
	};
});