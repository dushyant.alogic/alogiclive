<?php
namespace Alogic\Pdfdownload\Controller\Index;

use Magento\Framework\View\LayoutFactory;
use WeProvide\Dompdf\Controller\Result\Dompdf;
use WeProvide\Dompdf\Controller\Result\DompdfFactory;

class Index extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;
	protected $dompdfFactory;
	protected $layoutFactory;
	protected $_productRepository;
	protected $imageHelper;
	protected $swatchesHelper;
	protected $_logo;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		DompdfFactory $dompdfFactory,
		LayoutFactory $layoutFactory,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		\Magento\Catalog\Helper\Image $imageHelper,
		\Magento\Swatches\Helper\Data $swatchesHelper,
		\Magento\Theme\Block\Html\Header\Logo $logo,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->resultPageFactory = $resultPageFactory;
		$this->_productRepository = $productRepository;
		$this->dompdfFactory = $dompdfFactory;
		$this->imageHelper = $imageHelper;
		$this->swatchesHelper = $swatchesHelper;
		$this->_logo = $logo;
		$this->layoutFactory = $layoutFactory;
		parent::__construct($context);
	}
	public function getHtmlForPdf() {
		/** @var \Magento\Framework\View\Element\Template $block */
		$block = $this->layoutFactory->create()->createBlock('Magento\Framework\View\Element\Template');
		$block->setTemplate('Alogic_Pdfdownload::content.phtml');
		$logoSrc = $this->_logo->getLogoSrc();

		$params = $this->getRequest()->getParams();
		$sku = $params['sku'];
		$_product = $this->_productRepository->get($sku);

		$image_url = $this->imageHelper->init($_product, 'product_base_image')->getUrl();
		$attributes = $_product->getCustomAttributes();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$childs = [];
		$colorHashes = [];
		$configAttributes = [];
		if ($_product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
			$children = $_product->getTypeInstance()->getUsedProducts($_product);

			$productTypeInstance = $objectManager->get('Magento\ConfigurableProduct\Model\Product\Type\Configurable');

			foreach ($children as $child) {
				$sku = $child->getSku();

				$productAttributeOptions = $productTypeInstance->getConfigurableAttributesAsArray($_product);
				foreach ($productAttributeOptions as $key => $value) {

					$attribute_code = $value['attribute_code'];
					$attribute_label = $value['label'];
					$attribute_value = $child->getResource()->getAttribute($attribute_code)->getFrontend()->getValue($child);
					if (isset($configAttributes[$attribute_label])) {
						$configAttributes[$attribute_label] = $configAttributes[$attribute_label] . ',' . $attribute_value;
					} else {
						$configAttributes[$attribute_label] = $attribute_value;
					}

					if (!in_array($attribute_code, ['color', 'metallic_colour'])) {
						continue;
					}

					/*foreach ($value['values'] as $swatch) {

						$hashcodeData = $this->swatchesHelper->getSwatchesByOptionsId([$swatch['value_index']]);
						$hashCode = $hashcodeData[$swatch['value_index']]['value'];
						$colorHashes[$sku] = $hashCode;

					}*/
					$childs[$sku] = $attribute_value;
				}
			}
		} else {
			$attribute_value = $_product->getResource()->getAttribute('metallic_colour')->getFrontend()->getValue($_product);
			$childs[$sku] = $attribute_value;
		}

		$data = [
			'attributes' => $attributes,
			'product' => $_product,
			'image_url' => $image_url,
			'childs' => $childs,
			'colorHashes' => $colorHashes,
			'logoSrc' => $logoSrc,
			'configAttributes' => $configAttributes,
		];
		$block->setData($data);

		return $block->toHtml();
	}
	public function execute() {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productTypeInstance = $objectManager->get('Magento\UrlRewrite\Model\UrlRewriteFactory')->create();
		$collection = $productTypeInstance->getCollection()		
		->addFieldToFilter('entity_type', 'product')
		->addFieldToFilter('store_id', 35);
		foreach ($collection as $url) {
			echo "<pre>";
			print_r($url->getData());
			
		}
		/** @var Dompdf $response */
		// $response = $this->dompdfFactory->create();
		// $response->setData($this->getHtmlForPdf());

		// return $response;
	}
}
