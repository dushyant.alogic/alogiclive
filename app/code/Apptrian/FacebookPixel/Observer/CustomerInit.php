<?php
/**
 * @category  Apptrian
 * @package   Apptrian_FacebookPixel
 * @author    Apptrian
 * @copyright Copyright (c) Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
 
namespace Apptrian\FacebookPixel\Observer;

class CustomerInit implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    public $registry;
    
    /**
     * Constructor.
     *
     * @param \Apptrian\FacebookPixel\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
    }
    
    /**
     * Execute method.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return \Apptrian\FacebookPixel\Observer\CustomerInit
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customer        = null;
        $customerId      = 0;
        $customerSession = $observer->getEvent()->getCustomerSession();
        
        if ($customerSession) {
            $customer = $customerSession->getCustomer();
            if ($customer) {
                $customerId = $customer->getId();
            }
        }
        
        // You need to unregister first just in case it is already registrated
        $this->registry->unregister('apptrian_facebookpixel_customer_id');
        
        $this->registry->register('apptrian_facebookpixel_customer_id', $customerId);
        
        return $this;
    }
}
