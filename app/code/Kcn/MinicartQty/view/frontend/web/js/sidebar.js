define([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/modal/confirm',
    'underscore',
    'jquery/ui',
    'mage/decorate',
    'mage/collapsible',
    'mage/cookies'
], function ($, authenticationPopup, customerData, alert, confirm, _) {
    'use strict';

    return function (widget) {

        $.widget('mage.sidebar', widget, {

            _updateItemQty: function (elem) {
                var itemId = elem.data('cart-item');

                var btnplus = elem.data('btn-plus');
                var btnminus = elem.data('btn-minus');

                this._ajax(this.options.url.update, {
                    'item_id': itemId,
                    'item_qty': $('#cart-item-' + itemId + '-qty').val(),
                    'item_btn_plus': btnplus,
                    'item_btn_minus': btnminus
                }, elem, this._updateItemQtyAfter);
                this._customerData();
            },

            /**
             * Update content after update qty
             *
             * @param {HTMLElement} elem
             */
            _updateItemQtyAfter: function (elem) {
                var productData = this._getProductById(Number(elem.data('cart-item')));

                if (!_.isUndefined(productData)) {
                    $(document).trigger('ajax:updateCartItemQty');

                    if (window.location.href === this.shoppingCartUrl) {
                        window.location.reload(false);
                    }
                }
                this._hideItemButton(elem);
            },

            _customerData : function ()  {
                var sections = ['cart'];
                customerData.invalidate(sections);
                customerData.reload(sections, true);
            },
            _removeItem: function(elem) {
                
            var itemId = elem.data('post');
            
            this._ajax(this.options.url.remove, {
                item_id: itemId
            }, elem, this._removeItemAfter);
        },
        _removeItemAfter: function(elem, response) {
            var self = this;
            self.ajaxCross();            
        },
        ajaxCross: function () {
            var self = this;            
            $.ajax({
                url: BASE_URL + 'accounthome/crosssell/index',

                type: 'post',

                beforeSend: function () {
                    $('#ajax_loader').show();

                },
                success: function (res) {
                    
                    $('#crosssellmini').html(res);
                    $('button.tocart').click(function (event) {
                        event.preventDefault();
                        var tag = $(this).parents('form:first');

                        var data = tag.serializeArray();
                        //self.initAjaxAddToCart(tag, 'catalog-add-to-cart-' + $.now(), tag.attr('action'), data);


                    });
                    jQuery('#ajax_loader').hide();
                }
            });


        },
        initAjaxAddToCart: function (tag, actionId, url, data) {

            data.push({
                name: 'action_url',
                value: tag.attr('action')
            });

            var $addToCart = tag.find('.tocart').text();

            var self = this;
            data.push({
                name: 'ajax',
                value: 1
            });

            $.ajax({
                url: url,
                data: $.param(data),
                type: 'post',
                dataType: 'json',
                beforeSend: function (xhr, options) {
                    /*if (tag.find('.tocart').length) {
                        tag.find('.tocart').addClass('disabled');
                        tag.find('.tocart').text('Adding...');
                        tag.find('.tocart').attr('title', 'Adding...');
                    } else {
                        tag.addClass('disabled');
                        tag.text('Adding...');
                        tag.attr('title', 'Adding...');
                    }*/
                },
                success: function (response, status) {
                    if (status == 'success') {
                        var $source = '';
                        if (response.backUrl) {
                            console.log('Back url');
                            data.push({
                                name: 'action_url',
                                value: response.backUrl
                            });
                            self.initAjaxAddToCart(tag, 'catalog-add-to-cart-' + $.now(), response.backUrl, data);
                        } else {
                            console.log('else Back url');
                            console.log('Response', response);
                            if (jQuery('.action.showcart').length) {
                                jQuery('.action.showcart').trigger('click');
                            }

                            if (tag.find('.tocart').length) {
                                tag.find('.tocart').removeClass('disabled');
                                tag.find('.tocart').text($addToCart);
                                tag.find('.tocart').attr('title', $addToCart);
                                if (tag.closest('.product-item-info').length) {
                                    $source = tag.closest('.product-item-info');
                                    var width = $source.outerWidth();
                                    var height = $source.outerHeight();
                                } else {
                                    $source = tag.find('.tocart');
                                    var width = 300;
                                    var height = 300;
                                }

                            } else {
                                tag.removeClass('disabled');
                                tag.text($addToCart);
                                tag.attr('title', $addToCart);
                                $source = tag.closest('.product-item-info');
                                var width = $source.outerWidth();
                                var height = $source.outerHeight();
                            }
                        }
                    }
                },
                error: function () {
                    $('#mgs-ajax-loading').hide();
                    window.location.href = ajaxCartConfig.redirectCartUrl;
                }
            });
        },
            _initContent: function() {
            var self = this,
                events = {};
            
            this.element.decorate('list', this.options.isRecursive);
            /*events['click ' + this.options.button.close] = function(event) {
                event.stopPropagation();
                $(self.options.targetElement).dropdownDialog("close");
            };*/
            
            events['click ' + this.options.button.checkout] = $.proxy(function() {              
                var cart = customerData.get('cart'),
                    customer = customerData.get('customer');

                if (!customer().firstname && !cart().isGuestCheckoutAllowed) {
                    if (this.options.url.isRedirectRequired) {
                        location.href = this.options.url.loginUrl;
                    } else {
                        authenticationPopup.showModal();
                    }

                    return false;
                }
                location.href = this.options.url.checkout;
            }, this);
            
            events['click ' + this.options.button.remove] =  function(event) {
                event.stopPropagation();
                self._removeItem($(event.currentTarget));
                event.stopImmediatePropagation();
                /*confirm({
                    content: self.options.confirmMessage,
                    actions: {
                        confirm: function () {
                            
                            self._removeItem($(event.currentTarget));
                        },
                        always: function (event) {
                            event.stopImmediatePropagation();
                        }
                    }
                });*/
            };
            events['keyup ' + this.options.item.qty] = function(event) {
                self._showItemButton($(event.target));
            };
            events['click ' + this.options.item.button] = function(event) {
                event.stopPropagation();                
                self._updateItemQty($(event.currentTarget));
            };
            events['focusout ' + this.options.item.qty] = function(event) {
                self._validateQty($(event.currentTarget));
            };
            events['click .edit-icon'] = function (event) {
                event.stopPropagation();                              
                self._showOptions($(event.currentTarget));
            }
            this._on(this.element, events);
            //this._calcHeight();
            //this._isOverflowed();
        }

        });
        return $.mage.sidebar;
    }
});