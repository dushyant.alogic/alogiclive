<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MGS\Amp\Block\Html;

use Magento\Framework\Data\Tree\Node;

/**
 * Html page top menu block
 *
 * @api
 * @since 100.0.2
 */
class Topmenu extends \Magento\Theme\Block\Html\Topmenu {
	/**
	 * Recursively generates top menu html from data that is specified in $menuTree
	 *
	 * @param Node $menuTree
	 * @param string $childrenWrapClass
	 * @param int $limit
	 * @param array $colBrakes
	 * @return string
	 */

	protected function _getHtml(
		Node $menuTree,
		$childrenWrapClass,
		$limit,
		array $colBrakes = []
	) {
		$html = '';

		$children = $menuTree->getChildren();
		$parentLevel = $menuTree->getLevel();
		$childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

		$counter = 1;
		$itemPosition = 1;
		$childrenCount = $children->count();

		$parentPositionClass = $menuTree->getPositionClass();
		$itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

		/** @var \Magento\Framework\Data\Tree\Node $child */
		foreach ($children as $child) {
			if ($childLevel === 0 && $child->getData('is_parent_active') === false) {
				continue;
			}
			$child->setLevel($childLevel);
			$child->setIsFirst($counter == 1);
			$child->setIsLast($counter == $childrenCount);
			$child->setPositionClass($itemPositionClassPrefix . $counter);

			$outermostClassCode = '';
			$outermostClass = $menuTree->getOutermostClass();

			if ($childLevel == 0 && $outermostClass) {
				$outermostClassCode = ' class="' . $outermostClass . '" ';
				$child->setClass($outermostClass);
			}

			if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
				$html .= '</ul></li><li class="column"><ul>';
			}

			if ($this->_addSubMenu(
				$child,
				$childLevel,
				$childrenWrapClass,
				$limit
			)) {
				$html .= '<amp-accordion ' . $this->_getRenderedMenuItemAttributes($child) . '>';
				$html .= '<section><h6>' . $this->escapeHtml(
					$child->getName()
				) . '</h6>' . $this->_addSubMenu(
					$child,
					$childLevel,
					$childrenWrapClass,
					$limit
				) . '</section></amp-accordion>';
			} else {
				$html .= '<a href="' . $child->getUrl() . '?amp=1" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
					$child->getName()
				) . '</span></a>';
			}
			$itemPosition++;
			$counter++;
		}

		if (count($colBrakes) && $limit) {
			$html = '<li class="column"><ul>' . $html . '</ul></li>';
		}

		return $html;
	}
}
